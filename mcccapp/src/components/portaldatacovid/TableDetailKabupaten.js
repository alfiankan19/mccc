import React, { useState, useEffect } from 'react'
import {
	Table,
	TableHeader,
	TableCell,
	TableBody,
	TableRow,
	TableFooter,
	TableContainer,
	Input,
	Button,
	Pagination,
} from '@windmill/react-ui'
import {
	getDataSeriesKabupaten,
	updateSingleCovidData,
} from '../../data/repository'
import moment from 'moment'

function TableDetailKabupaten({
	kodewil,
	namaKabupaten,
	dateRange,
	onUpdatedData,
}) {
	const [pageTable2, setPageTable2] = useState(1)

	const [dataTable2, setDataTable2] = useState([])
	const [totalResults, settotalResults] = useState(0)
	const [date, setDate] = useState()
	const [isEditOpen, setIsEditOpen] = useState(false)
	const [editKey, setEditKey] = useState()

	const resultsPerPage = 10

	function onPageChangeTable2(p) {
		setPageTable2(p)
	}

	const unique = (value, index, self) => {
		return self.indexOf(value) === index
	}

	const getDataCovidDetail = (kabid) => {
		getDataSeriesKabupaten(kabid, dateRange)
			.then((result) => {
				//console.log(result)
				settotalResults(result.data.data.length)
				namaKabupaten(result.data.kabupaten)

				if (result.data.data.length === 0) {
					setDataTable2([])
				} else {
					//console.log(result.data.kabupaten)
					setDataTable2(
						result.data.data.slice(
							(pageTable2 - 1) * resultsPerPage,
							pageTable2 * resultsPerPage
						)
					)
				}
				//console.log(dataTable2)
			})
			.catch((err) => console.log(err))
	}

	const updateSingleColomnDataCovid = (
		target,
		total,
		kabid,
		date,
		position,
		original,
		e
	) => {
		updateSingleCovidData(target, total, kabid, date)
			.then((result) => {
				//console.log(dataTable2, position)
				//console.log(result)
				setEditKey(position)
				setIsEditOpen(!isEditOpen)
				let temp = dataTable2
				temp[position].Terkonfirmasi = total
				setDataTable2(temp)
				e.target.blur()
			})
			.catch((err) => {
				//console.log(err)
				e.target.value = original
				alert('Gagal')
			})
		onUpdatedData()
	}

	const handleEditControl = (e, target, kasus, original, position) => {
		if (e.key === 'Enter') {
			updateSingleColomnDataCovid(
				target,
				e.target.value,
				kasus.kab_id,
				kasus.date_series,
				position,
				original,
				e
			)
		}
	}

	useEffect(() => {
		getDataCovidDetail(kodewil)
		setDate(dateRange)
	}, [pageTable2, dateRange])

	return (
		<>
			<h1 className='text-white'>{}</h1>
			{isEditOpen ? (
				<div className='py-5'>
					<div
						className='p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex'
						role='alert'
					>
						<span className='flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3'>
							Info
						</span>
						<span className='font-semibold mr-2 text-left flex-auto'>
							Ubah data pada kolom lalu tekan tombol enter untuk menyimpan
						</span>
					</div>
				</div>
			) : null}
			<TableContainer className='mb-8'>
				<Table>
					<TableHeader>
						<tr>
							<TableCell>Tanggal</TableCell>
							<TableCell>Editor</TableCell>
							<TableCell>Terkonfirmasi</TableCell>
							<TableCell>Sembuh</TableCell>
							<TableCell>Meninggal</TableCell>
							<TableCell>Actions</TableCell>
						</tr>
					</TableHeader>
					<TableBody>
						{dataTable2.map((kasus, i) => (
							<TableRow key={i}>
								<TableCell>
									<span className='text-sm'>
										{moment(kasus.date_series).format('DD-MM-Y')}
									</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>
										{kasus.Editor.split('/').filter(unique)}
									</span>
								</TableCell>
								<TableCell>
									<Input
										onKeyDown={(e) =>
											handleEditControl(
												e,
												'terkonfirmasi',
												kasus,
												kasus.Terkonfirmasi,
												i
											)
										}
										style={{ backgroundColor: '#00ffff00' }}
										className={`
											${isEditOpen && editKey === i ? 'border-solid border-2 rounded' : ' '}`}
										defaultValue={kasus.Terkonfirmasi}
										disabled={!(isEditOpen && editKey === i)}
									/>
								</TableCell>
								<TableCell>
									<Input
										onKeyDown={(e) =>
											handleEditControl(e, 'sembuh', kasus, kasus.Sembuh, i)
										}
										style={{ backgroundColor: '#00ffff00' }}
										className={`
											${isEditOpen && editKey === i ? 'border-solid border-2 rounded' : ' '}`}
										defaultValue={kasus.Sembuh}
										disabled={!(isEditOpen && editKey === i)}
									/>
								</TableCell>
								<TableCell>
									<Input
										onKeyDown={(e) =>
											handleEditControl(
												e,
												'meninggal',
												kasus,
												kasus.Meninggal,
												i
											)
										}
										style={{ backgroundColor: '#00ffff00' }}
										className={`
											${isEditOpen && editKey === i ? 'border-solid border-2 rounded' : ' '}`}
										defaultValue={kasus.Meninggal}
										disabled={!(isEditOpen && editKey === i)}
									/>
								</TableCell>
								<TableCell>
									<div className='flex items-center space-x-4'>
										<Button
											onClick={() => {
												setEditKey(i)
												setIsEditOpen(!isEditOpen)
											}}
											size='small'
											aria-label='Edot'
										>
											{isEditOpen && editKey === i ? 'Close' : 'Edit'}
										</Button>
									</div>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<TableFooter>
					<Pagination
						totalResults={totalResults}
						resultsPerPage={resultsPerPage}
						onChange={onPageChangeTable2}
						label='Table navigation'
					/>
				</TableFooter>
			</TableContainer>
		</>
	)
}

export default TableDetailKabupaten
