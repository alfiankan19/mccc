import React, { useState, useEffect } from 'react'
import { Select, Input, Label } from '@windmill/react-ui'
import {
	getWilayahKasusProvinsi,
	getWilayahKabupaten,
	getWilayahKecamatan,
	getWilayahDesa,
} from '../../data/repository'
import {
	TableContainer,
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	ModalFooter,
} from '@windmill/react-ui'

function WilayahOption({ onWilayahChanged, kodewilValue }) {
	const [provinsidata, setProvinsidata] = useState([])
	const [kabupatendata, setKabupatendata] = useState([])
	const [kecamatandata, setKecamatandata] = useState([])
	const [desadata, setDesadata] = useState([])
	const [kodewilProv, setKodewilProv] = useState(34)
	const [kodewilKab, setKodewilKab] = useState('00')
	const [kodewilKec, setKodewilKec] = useState('00')
	const [kodewilDes, setKodewilDes] = useState('00')

	const getProvinsiData = (_default = '') => {
		setProvinsidata([])
		setKabupatendata([])
		setKecamatandata([])
		setDesadata([])

		setKodewilProv('00')
		setKodewilKab('00')
		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
				if (_default !== '') {
					setKodewilProv(_default.slice(0, 2))
					getKabupatenData(_default.slice(0, 2), _default)
				}
			})
			.catch((err) => {
				getProvinsiData()
				//console.log(err)
			})
	}
	const getKabupatenData = (provid, _default = '') => {
		setKabupatendata([])
		setKecamatandata([])
		setDesadata([])

		setKodewilKab('00')
		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKabupaten(provid)
			.then((result) => {
				//console.log(result.data)
				setKabupatendata(result.data)
				if (_default !== '') {
					setKodewilKab(_default.slice(0, 4))
					getKecamatanData(_default.slice(0, 4), _default)
				}
			})
			.catch((err) => {
				getKabupatenData(provid)
				//console.log(err)
			})
	}

	const getKecamatanData = (kabid, _default = '') => {
		setKecamatandata([])
		setDesadata([])

		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKecamatan(kabid)
			.then((result) => {
				//console.log(result.data)
				setKecamatandata(result.data)
				if (_default !== '') {
					setKodewilKec(_default.slice(0, 7))
					getDesaData(_default.slice(0, 7), _default)
				}
			})
			.catch((err) => {
				getKecamatanData(kabid)
				//console.log(err)
			})
	}

	const getDesaData = (kecid, _default = '') => {
		setDesadata([])

		if (_default === '') {
			setKodewilDes('00')
			////console.log('set desa 00')
		}

		getWilayahDesa(kecid)
			.then((result) => {
				////console.log(result.data)
				setDesadata(result.data)
				if (_default !== '') {
					setKodewilDes(_default)
				}
			})
			.catch((err) => {
				getDesaData(kecid)
				////console.log(err)
			})
	}

	//effect default value
	useEffect(() => {
		if (kodewilValue !== '') {
			const kodewilArr = kodewilValue
			if (kodewilArr.length >= 9) {
				setKodewilDes(kodewilArr)
				////console.log(kodewilArr, kodewilArr.slice(0, 2))
				getProvinsiData(kodewilArr)
			}
			// setKodewilKab('00')
			// setKodewilKec('00')
			// setKodewilDes('00')
		}
	}, [kodewilValue])

	useEffect(() => {
		getProvinsiData()
	}, [0])

	useEffect(() => {
		//onWilayahChanged(kodewilDes)
	}, [kodewilDes])
	return (
		<>
			{/* <Modal isOpen={isModalOpen} onClose={closeModal}></Modal> */}

			<Label className=''>
				<span>
					Pilih Provinsi {kodewilProv} {kodewilKab} {kodewilKec} {kodewilDes}
				</span>
				<Select
					onChange={(e) => {
						getKabupatenData(e.target.value)
						setKodewilProv(e.target.value)
					}}
					value={kodewilProv}
					className='mt-1 border-solid border-2 rounded'
				>
					<option key={'00'} value={'00'}>
						- Semua -
					</option>
					{provinsidata.map((v) => (
						<option key={v.idwil} value={v.idwil}>
							{v.nama}
						</option>
					))}
				</Select>
			</Label>
			{kabupatendata.length !== 0 ? (
				<Label className='pt-4'>
					<span>Pilih Kabupaten</span>
					<Select
						onChange={(e) => {
							getKecamatanData(e.target.value)
							setKodewilKab(e.target.value)
						}}
						value={kodewilKab}
						className='mt-1 border-solid border-2 rounded'
					>
						<option key={'00'} value={'00'}>
							- Semua -
						</option>
						{kabupatendata.map((v) => (
							<option key={v.kpu_idkab} value={v.kpu_idkab}>
								{v.nama}
							</option>
						))}
					</Select>
				</Label>
			) : null}
			{kecamatandata.length !== 0 ? (
				<Label className='pt-4'>
					<span>Pilih Kecamatan</span>
					<Select
						onChange={(e) => {
							setKodewilKec(e.target.value)
							getDesaData(e.target.value)
						}}
						value={kodewilKec}
						className='mt-1 border-solid border-2 rounded'
					>
						<option key={'00'} value={'00'}>
							- Semua -
						</option>
						{kecamatandata.map((v) => (
							<option key={v.kpu_idkec} value={v.kpu_idkec}>
								{v.nama}
							</option>
						))}
					</Select>
				</Label>
			) : null}
			{desadata.length !== 0 ? (
				<Label className='pt-4'>
					<span>Pilih Desa</span>
					<Select
						onChange={(e) => {
							//setKodewilDes(e.target.value)
							onWilayahChanged(e.target.value)
						}}
						value={kodewilDes}
						className='mt-1 border-solid border-2 rounded'
					>
						<option key={'00'} value={'00'}>
							- Semua -
						</option>
						{desadata.map((v) => (
							<option key={v.id_desa} value={v.id_desa}>
								{v.nama}
							</option>
						))}
					</Select>
				</Label>
			) : null}
		</>
	)
}

export default WilayahOption
