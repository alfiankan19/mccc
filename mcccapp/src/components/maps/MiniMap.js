import React, { useState } from 'react'
import {
	MapContainer,
	TileLayer,
	LayersControl,
	Marker,
	Popup,
	FeatureGroup,
} from 'react-leaflet'
import L from 'leaflet'
import IconEwarong from './makam.svg'
function MiniMap({ lokasiVal, jenisLokasi }) {
	const [isPopUpOpen, setIsPopUpOpen] = useState(false)
	const myIcon = L.icon({
		iconUrl: IconEwarong,
		iconSize: [30, 30],
	})
	return (
		<div className='mt-3'>
			<MapContainer
				center={lokasiVal}
				style={{ height: '500px', borderRadius: '15px' }}
				zoom={13}
				scrollWheelZoom={false}
				whenCreated={(map) => {
					L.marker(lokasiVal, { icon: myIcon })
						.addTo(map)
						.bindPopup(`Lokasi ${jenisLokasi}`)
						.openPopup()
				}}
			>
				<TileLayer
					attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					url='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
					subdomains={['mt0', 'mt1', 'mt2', 'mt3']}
				/>

				{/* <Marker icon={myIcon} position={lokasiVal}>
					<Popup ref={(e) => e.openPopup()}>
						<span>Lokasi {jenisLokasi}</span>
					</Popup>
				</Marker> */}
			</MapContainer>
		</div>
	)
}

export default MiniMap
