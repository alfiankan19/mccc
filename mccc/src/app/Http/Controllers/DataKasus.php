<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;
use App\Models\WilayahKasusModel;
use App\Models\WilayahModel;
use App\Models\User;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DataKabupaten;
use Illuminate\Support\Facades\DB;


class DataKasus extends Controller
{

    public function kasusprovinsi()
    {
        $data = DB::table('wilayah_provinsi')->select(DB::raw('nama,id AS idwil,0 kasus'))->orderBy('nama', 'asc')->get();
        return response()->json($data);
    }

    public function datakabupatenAll()
    {

        $kabupatens = WilayahModel::kabupatenAll();

 
        foreach ($kabupatens as $key => $value){
            // get kabupaten total
            $total = WilayahKasusModel::getTotalDataKabupaten($value->kpu_idkab);
            $kabupatens[$key]->terkonfirmasi = $total["terkonfirmasi"]->total?$total["terkonfirmasi"]->total : 0;
            $kabupatens[$key]->sembuh = $total["sembuh"]->total?$total["sembuh"]->total:0;
            $kabupatens[$key]->meninggal = $total["meninggal"]->total?$total["meninggal"]->total:0;
            $aktif = $kabupatens[$key]->terkonfirmasi - ($kabupatens[$key]->sembuh + $kabupatens[$key]->meninggal);
            $kabupatens[$key]->aktif = $aktif < 0?0:$aktif;
            $kabupatens[$key]->count = $key;
        }
        
        return response()->json($kabupatens);
    }

    public function dataSummary()
    {

        $kabupatens = WilayahModel::kabupatenAll();

        $terkonfirmasi_t = 0;
        $aktif_t = 0;
        $sembuh_t = 0;
        $meninggal_t = 0;
        foreach ($kabupatens as $key => $value){
            // get kabupaten total
            $total = WilayahKasusModel::getTotalDataKabupaten($value->kpu_idkab);

            $terkonfirmasi_raw = $total["terkonfirmasi"]->total?$total["terkonfirmasi"]->total : 0;
            $sembuh_raw = $total["sembuh"]->total?$total["sembuh"]->total:0;
            $meninggal_raw = $total["meninggal"]->total?$total["meninggal"]->total:0;

            $terkonfirmasi_t += $terkonfirmasi_raw;
            $sembuh_t += $sembuh_raw;
            $meninggal_t += $meninggal_raw;
            
            $aktif_t += $terkonfirmasi_raw - ($sembuh_raw + $meninggal_raw) < 0?0:$terkonfirmasi_raw - ($sembuh_raw + $meninggal_raw);
        }
        
        return response()->json(array(
            'terkonfirmasi' => $terkonfirmasi_t,
            'aktif' => $aktif_t,
            'sembuh' => $sembuh_t,
            'meninggal' => $meninggal_t
        ));
    }

    public function datakabupaten($provid, $search = '')
    {

        $kabupatens = WilayahModel::Kabupaten($provid, $search);

 
        foreach ($kabupatens as $key => $value){
            // get kabupaten total
            $total = WilayahKasusModel::getTotalDataKabupaten($value->kpu_idkab);
            $kabupatens[$key]->terkonfirmasi = $total["terkonfirmasi"]->total?$total["terkonfirmasi"]->total : 0;
            $kabupatens[$key]->sembuh = $total["sembuh"]->total?$total["sembuh"]->total:0;
            $kabupatens[$key]->meninggal = $total["meninggal"]->total?$total["meninggal"]->total:0;
        }
        
        return response()->json($kabupatens);
    }

    public function datakabupatenExcell($provid)
    {

        //return $kabupatens;
        //return response()->json($data);
        //generate excell save to folder temp return link download
        $filename = WilayahModel::getProvOne($provid)->nama . "_" . date('d-m-Y') . ".xlsx";
        return Excel::download(new DataKabupaten($provid), $filename);
    }

    public function datakabupatenExcellJson($provid)
    {
        $kabupatens = WilayahModel::Kabupaten($provid);

        $result[] = array(
            "Kabupaten",
            "Terkonfirmasi",
            "Sembuh",
            "Meninggal"
        );
        foreach ($kabupatens as $key => $value){
            // get kabupaten total
            $total = WilayahKasusModel::getTotalDataKabupaten($kabupatens[$key]->kpu_idkab);
            $result[] = array(
                $value->nama,
                $total["terkonfirmasi"]->total,
                $total["sembuh"]->total,
                $total["meninggal"]->total,
            );
        }
        
        
        return $result;
    }



    public function uploadDataSeries(Request $request)
    {
        $tanggal = date('Y-m-d', strtotime($request->date));
        $data = json_decode($request->data);
        $user = Auth::user();
        //update into db
        foreach ($data as $val) {
            try {
                WilayahKasusModel::addDataCovid(
                    array(
                        'kab_id' => $val->Kode,
                        'total' => $val->Terkonfirmasi,
                        'date_series' => $tanggal,
                        'created_by' => $user->id
                    ), 'terkonfirmasi');
            } catch (\Throwable $th) {
                //throw $th;
            }
            try {
                WilayahKasusModel::addDataCovid(
                    array(
                        'kab_id' => $val->Kode,
                        'total' => $val->Sembuh,
                        'date_series' => $tanggal,
                        'created_by' => $user->id
                    ), 'sembuh');
            } catch (\Throwable $th) {
                //throw $th;
            }

            try {
                WilayahKasusModel::addDataCovid(
                    array(
                        'kab_id' => $val->Kode,
                        'total' => $val->Meninggal,
                        'date_series' => $tanggal,
                        'created_by' => $user->id
                    ), 'meninggal');
            } catch (\Throwable $th) {
                //throw $th;
            }


        }

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Series Data",
        ),200);

    }

    public function getDataSeriesKabupaten($kabid, $dateStart, $dateEnd)
    {
        $data  = WilayahKasusModel::getSeriesKabupaten(
            $kabid, 
            date('Y-m-d', strtotime($dateStart)), 
            date('Y-m-d', strtotime($dateEnd))
        );
        $kab = DB::table('wilayah_kabupaten')->where('id', $kabid)->first();
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "kabupaten" => $kab->kabkot,
            "data" => $data,
        ),200);
    }

    public function getLastUpdate(){
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "data" => DB::table('data_log')->orderBy('log_time', 'desc')->first(),
        ),200);
    }

    public function updateSingleData(Request $request)
    {
        try {

            $user = Auth::user();

            $result = WilayahKasusModel::updateSingleSeries(
                $request->target, 
                $request->kabid, 
                date('Y-m-d', strtotime($request->date)), 
                $request->total, 
                $user
            );

            $this->logd(json_encode(array('action' => 'update')));

            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil Query Series Data",
                "data" => $result,
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Gagal Query Series Data",
                "data" => $request->total,
            ),500);        
        }
    }

    public function seriesProvinsiTerkonfirmasi($provid='')
    {
        $data  = WilayahKasusModel::getSeriesProvinsiTerkonfirmasi($provid);
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "data" => $data,
        ),200); 
    }

    public function seriesProvinsiSembuh($provid='')
    {
        $data  = WilayahKasusModel::getSeriesProvinsiSembuh($provid);
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "data" => $data,
        ),200); 
    }

    public function seriesProvinsiMeninggal($provid='')
    {
        $data  = WilayahKasusModel::getSeriesProvinsiMeninggal($provid);
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "data" => $data,
        ),200); 
    }

    public function stackedCovidSeries($provid = '')
    {
        $data  = WilayahKasusModel::getStackedSeries($provid);

        foreach ($data as $key => $value) {
            $data[$key]->aktif = ($value->terkonfirmasi - ($value->sembuh + $value->meninggal)) < 0?0:$value->terkonfirmasi - ($value->sembuh + $value->meninggal);
        }

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Query Series Data",
            "data" => $data,
        ),200); 
    }

}