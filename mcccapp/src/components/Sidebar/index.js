import React from 'react'
import MobileSidebar from './MobileSidebar'
import DesktopSidebar from './DesktopSidebar'
function Sidebar() {
	return (
		<>
			<DesktopSidebar />
			<MobileSidebar />
		</>
	)
}

export default Sidebar
