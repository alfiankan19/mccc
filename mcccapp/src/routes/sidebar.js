/**
 * ⚠ These are used just to render the Sidebar!
 * You can include any link here, local or external.
 *
 * If you're looking to actual Router routes, go to
 * `routes/index.js`
 */
const routes = [
	{
		icon: 'HomeIcon',
		name: 'Dashboard',
		auth: false,
		routes: [
			// submenu
			{
				path: '/app/dashboard',
				name: 'Dashboard',
			},
			{
				path: '/app/siranap',
				name: 'SIRANAP',
			},
		],
	},
	{
		icon: 'FormsIcon',
		name: 'Master Data',
		auth: true,
		routes: [
			// submenu
			{
				path: '/app/rumahsakit',
				name: 'Rumah Sakit',
			},
			{
				path: '/app/jenisranap',
				name: 'Jenis Ranap',
			},
			{
				path: '/app/users',
				name: 'User Account',
			},
		],
	},
	{
		icon: 'FormsIcon',
		name: 'Portal Data',
		auth: true,
		routes: [
			// submenu
			{
				path: '/app/covid19',
				name: 'Covid 19',
			},
			{
				path: '/app/vaksinasi',
				name: 'Vaksinasi',
			},
			{
				path: '/app/siranapdata',
				name: 'Siranap',
			},
			{
				path: '/app/pemakaman',
				name: 'Pemakaman',
			},
		],
	},
	// {
	// 	path: '/app/forms',
	// 	icon: 'FormsIcon',
	// 	name: 'Forms',
	// 	auth: true,
	// },
	// {
	// 	path: '/app/cards',
	// 	icon: 'CardsIcon',
	// 	name: 'Cards',
	// 	auth: true,
	// },
	// {
	// 	path: '/app/charts',
	// 	icon: 'ChartsIcon',
	// 	name: 'Charts',
	// 	auth: true,
	// },
	// {
	// 	path: '/app/buttons',
	// 	icon: 'ButtonsIcon',
	// 	name: 'Buttons',
	// 	auth: true,
	// },
	// {
	// 	path: '/app/modals',
	// 	icon: 'ModalsIcon',
	// 	name: 'Modals',
	// 	auth: true,
	// },
	// {
	// 	path: '/app/tables',
	// 	icon: 'TablesIcon',
	// 	name: 'Tables',
	// 	auth: true,
	// },
	{
		icon: 'PagesIcon',
		name: 'Lainya',
		auth: false,
		routes: [
			// submenu
			{
				path: '/login',
				name: 'Login',
			},
			{
				path: '/app/dashboard',
				name: 'Home Page',
			},
		],
	},
]

export default routes
