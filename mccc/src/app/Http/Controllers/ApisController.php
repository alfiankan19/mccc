<?php

namespace App\Http\Controllers;

use Validator;

use App\Models\User;
use App\Models\Instrument;
use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;

class ApisController extends ApiResponse
{
    public function __construct() 
    {
        $this->middleware('auth:api', ['except' => ['collectInstrumentFormTest','getInstrumentForm','getInstrument']]);
    }

    public function getInstrument()
    {

        
        return response()->json(array(
            "success" => TRUE,
            "message" => "Succes Get All Instrument",
            "data" => Instrument::getAllinstrument()
        ),200);
    }

    public function getInstrumentForm($jenis)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "get Instrumen Form",
            "data" => Instrument::getInstrumentForm($jenis)
        ),200);
    }

    public function collectInstrumentForm(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'respondent_nama' => 'required|string',
            'respondent_lokasi' => 'required|string',
            'respondent_kode_wilayah' => 'required|string',
            'answer' => 'required|string',
        ]);

        if ($validator->fails()) {

            return $this->json_fail($validator->errors(),"Validation Error",422);
        }
      

        //create responden
        try {
            $responden = Instrument::createRespondent(
                auth()->user()->id,
                $request->respondent_nama,
                $request->respondent_lokasi,
                $request->respondent_kode_wilayah,
                $request->answer,
            );
            return response()->json(array(
                "success" => TRUE,
                "message" => "get Instrumen Form",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error",
                "data" => $th
            ),500);
        }

    }

    public function getRespondentByUser($page,$search="")
    {
        $data = Respondent::getAllRespondenByUserId(auth()->user()->id,$page,$search);
        return response()->json(array(
            "success" => TRUE,
            "message" => "succes get respondent created by ".auth()->user()->name,
            "data" => $data
        ),200);
    }
   
}
