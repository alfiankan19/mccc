import React, { useRef, useState } from 'react'
import { Button } from '@windmill/react-ui'
import Loader from 'react-loader-spinner'
import * as XLSX from 'xlsx'
import { Modal, ModalHeader, ModalBody, ModalFooter } from '@windmill/react-ui'
import { uploadDate } from './uploaderSlice'
import { useSelector } from 'react-redux'
import { addDataSeriesCovid } from '../../data/repository'
const rules = {
	covid: ['Kode', 'Kabupaten', 'Terkonfirmasi', 'Sembuh', 'Meninggal'],
}

function Uploader({ tipes }) {
	const inputFile = useRef(null)
	const [isLoading, setIsLoading] = useState(false)
	const [statusMsg, setStatusMsg] = useState('')
	const [isModalOpen, setIsModalOpen] = useState(false)
	const [modalButtonOk, setModalButtonOk] = useState()
	const [isJsonValid, setIsJsonValid] = useState(false)
	const tanggalUpload = useSelector(uploadDate)
	const [dataUpload, setDataUpload] = useState('')

	function openModal(msg, confirm = false) {
		setStatusMsg(msg)
		setIsJsonValid(confirm)
		setIsModalOpen(true)
	}

	function closeModal() {
		setIsModalOpen(false)
		inputFile.current.value = ''
	}

	const uploadData = () => {
		setStatusMsg('Memproses Data ...')
		setIsLoading(true)
		addDataSeriesCovid(dataUpload, tanggalUpload)
			.then((result) => {
				setIsLoading(false)
				openModal('Berhasil Menambahkan Data')
			})
			.catch((err) => {
				setIsLoading(false)
				openModal('Gagal ' + err)
			})
	}

	const onFileChange = (event) => {
		// setStatusMsg('Validasi Data')
		// setIsLoading(true)
		////console.log(event.target.files[0])

		if (event.target.files[0] !== null && event.target.files[0] !== undefined) {
			readFile(event.target.files[0])
		}
	}

	const readFile = (file) => {
		let f = file
		const reader = new FileReader()
		reader.onload = (evt) => {
			const bstr = evt.target.result
			const wb = XLSX.read(bstr, { type: 'binary' })
			const wsname = wb.SheetNames[0]
			const ws = wb.Sheets[wsname]
			const data = XLSX.utils.sheet_to_csv(ws, { header: 1 })
			const result = convertToJson(data)
			console.log(
				JSON.stringify(Object.keys(result[0])),
				JSON.stringify(rules.covid)
			)
			if (
				JSON.stringify(Object.keys(result[0])) === JSON.stringify(rules.covid)
			) {
				setDataUpload(result)
				openModal(
					`Pengecekan File Selesai, File Valid, Apakah anda mengkonfirmasi untuk update data per tanggal ${tanggalUpload}`,
					true
				)
			} else {
				openModal(
					'Format File Excel tidak valid,jangan melakukan perubahan kolomdan nama kolom cukup isinya'
				)
			}
		}
		reader.readAsBinaryString(f)
	}

	const convertToJson = (csv) => {
		let lines = csv.split('\n')
		let result = []
		let headers = lines[0].split(',')
		for (let i = 1; i < lines.length; i++) {
			let obj = {}
			let currentline = lines[i].split(',')
			for (let j = 0; j < headers.length; j++) {
				obj[headers[j]] = currentline[j]
			}
			result.push(obj)
		}
		return result
	}

	return (
		<div className='mt-2'>
			{!isLoading ? (
				<>
					<input
						className='hidden'
						type='file'
						ref={inputFile}
						accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
						onChange={onFileChange}
					/>
					<Button onClick={() => inputFile.current.click()} size='small'>
						Pilih File
					</Button>
				</>
			) : (
				<div className='inline-flex'>
					<Loader
						className='p-5'
						type='Oval'
						visible={true}
						color='#00BFFF'
						height={30}
						width={30}
					/>
					<span className='dark:text-white mt-5'>{statusMsg}</span>
				</div>
			)}
			<Modal isOpen={isModalOpen} onClose={closeModal}>
				<ModalHeader>Information</ModalHeader>
				<ModalBody>{statusMsg}</ModalBody>
				<ModalFooter>
					<div className='hidden sm:block'>
						<Button onClick={closeModal}>Close</Button>
					</div>

					{isJsonValid ? (
						<div className='hidden sm:block'>
							<Button onClick={uploadData}>Konfirmasi</Button>
						</div>
					) : null}

					<div className='block w-full sm:hidden'>
						<Button block size='large' onClick={closeModal}>
							Close
						</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	)
}

export default Uploader
