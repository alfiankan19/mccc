import React, { useContext } from 'react'

import SidebarContent from './SidebarContent'
import { Transition } from '@windmill/react-ui'

import { SidebarContext } from '../../context/SidebarContext'
function DesktopSidebar(props) {
	const { isSidebarOpen } = useContext(SidebarContext)

	return (
		<Transition show={isSidebarOpen}>
			<Transition
				enter='transition ease-in-out duration-150'
				enterFrom='opacity-0 transform -translate-x-20'
				enterTo='opacity-100'
				leave='transition ease-in-out duration-150'
				leaveFrom='opacity-100'
				leaveTo='opacity-0 transform -translate-x-20'
			>
				<aside className='z-30 flex-shrink-0 hidden w-64 overflow-y-auto bg-white dark:bg-gray-800 lg:block'>
					<SidebarContent />
				</aside>
			</Transition>
		</Transition>
	)
}

export default DesktopSidebar
