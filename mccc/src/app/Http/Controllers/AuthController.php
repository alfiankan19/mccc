<?php

namespace App\Http\Controllers;
use Validator;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;


class AuthController extends ApiResponse
{

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {

            return $this->json_fail($validator->errors(),"Validation Error",422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return $this->json_fail(['error' => 'Unauthorized'],"Unauthorized",401);
        }

        return $this->createNewToken($token);
    }


    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'username' => 'required|string|between:2,100|unique:users',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return $this->json_fail($validator->errors(),"Validation Error",422);

        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));


        return $this->json_success(['user' => $user],"User successfully registered",201);

    }


    public function logout() {
        auth()->logout();
        return $this->json_success(array(),"User successfully signed out",200);

    }


    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return $this->json_success(auth()->user(),"Succes Request User Data",200);

    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return $this->json_success([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user()
        ],"Succes LoggedIn",200);
    }

    public function changeFoto(Request $request){
        $user = Auth::user();
        try{
            $tipe = "penerimabantuan";
            $file1 = $request->file('foto');
            $subname1 = md5(time()."_".$file1->getClientOriginalName()).".".$file1->getClientOriginalExtension();
            $file1->move('./upload/fotouser',$subname1);
        } catch (\Throwable $th) {
            $subname1 = "";
        }

        User::where('id', $user->id)->update(['foto' => $subname1]);

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil",
            "data" => $subname1
        ),200);

    }

    public function changeUsername(Request $request){
        $user = Auth::user();
        User::where('id', $user->id)->update(['username' => $request->username]);

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil",
            "data" => ""
        ),200);
    }

    public function changeHP(Request $request){
        $user = Auth::user();
        User::where('id', $user->id)->update(['phone' => $request->phone]);

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil",
            "data" => ""
        ),200);
    }

    public function changeEmail(Request $request){
        $user = Auth::user();
        User::where('id', $user->id)->update(['email' => $request->email]);

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil",
            "data" => ""
        ),200);
    }

    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required',
        ]);

        if($validator->fails()){
            return $this->json_fail($validator->errors(),"Validation Error",422);
        }
  
        
        $user = Auth::user();
  
        if (!Hash::check($request->current_password, $user->password)) {
            return $this->json_fail(array("password" => "Current password does not match!"),"Validation Error : Current password does not match!",422);

        }
  
        $user->password = Hash::make($request->password);
        $user->save();
  
        return $this->json_success(array(),"Password successfully changed! Please Relogin",200);

    }

}
