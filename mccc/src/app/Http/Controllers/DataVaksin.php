<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;
use App\Models\WilayahKasusModel;
use App\Models\WilayahModel;
use App\Models\User;
use App\Models\DataVaksinModel;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DataKabupaten;
use Illuminate\Support\Facades\DB;


class DataVaksin extends Controller
{

    public function datavaksin1(Request $request, $kodewil)
    {
        $page = $request->query('page','1');
        $date_start = $request->query('date_start', date("Y-m-d", strtotime("-1 years")));
        $date_stop = $request->query('date_end', date("Y-m-d"));
        if ($date_start == '') {
            $date_start = date("Y-m-d", strtotime("-1 years"));
        }
        if ($date_stop == '') {
            $date_stop = date("Y-m-d");
        }

        $date_start = date('Y-m-d', strtotime($date_start));
        $date_stop = date('Y-m-d', strtotime($date_stop));

        //try {
            $data = DataVaksinModel::vaksin1($kodewil, $date_start, $date_stop, $page);
            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil Query Series Datad". $date_start,
                'row_total' => DB::table('vaksinasi_1')->whereRaw('kode_wilayah LIKE ?', [$kodewil .'%'])->whereRaw('penyelenggaraan_date BETWEEN ? AND ?', [$date_start, $date_stop])->count(),
                'row_resul_total' => count($data),
                'row_data' => $data
            ),200); 
        // } catch (\Throwable $th) {
        //     return response()->json(array(
        //         "success" => FALSE,
        //         "message" => "Gagal Query Series Data",
        //         "data" => $th,
        //     ),200); 
        // }
    }

    public function datavaksin1All(Request $request)
    {
        $page = $request->query('page','1');
        $date_start = $request->query('date_start', date("Y-m-d", strtotime("-1 years")));
        $date_stop = $request->query('date_end', date("Y-m-d"));
        if ($date_start == '') {
            $date_start = date("Y-m-d", strtotime("-1 years"));
        }
        if ($date_stop == '') {
            $date_stop = date("Y-m-d", strtotime("+1 years"));
        }


        $date_start = date('Y-m-d', strtotime($date_start));
        $date_stop = date('Y-m-d', strtotime($date_stop));

        //try {
            $data = DataVaksinModel::vaksin1('', $date_start, $date_stop, $page);
            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil Query Series Data All",
                'row_total' => DB::table('vaksinasi_1')->whereRaw('penyelenggaraan_date BETWEEN ? AND ?', [$date_start, $date_stop])->count(),
                'row_resul_total' => count($data),
                'row_data' => $data
            ),200); 
        // } catch (\Throwable $th) {
        //     return response()->json(array(
        //         "success" => FALSE,
        //         "message" => "Gagal Query Series Data",
        //         "data" => $th,
        //     ),200); 
        // }
    }

    public function addData(Request $request)
    {
        $user = Auth::user();
        //try {
            DataVaksinModel::addNew($request, $user);
            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil Menambah Data",
            ),200); 
        // } catch (\Throwable $th) {
        //     return response()->json(array(
        //         "success" => FALSE,
        //         "message" => "Gagal Menambah Data",
        //     ),200); 
        // }
    }

    public function editData(Request $request)
    {
        $user = Auth::user();
        //try {
            DataVaksinModel::editData($request, $user);
            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil Edit Data",
            ),200); 
        // } catch (\Throwable $th) {
        //     return response()->json(array(
        //         "success" => FALSE,
        //         "message" => "Gagal Menambah Data",
        //     ),200); 
        // }
    }

    public function deleteData(Request $request)
    {
        
        DB::delete('delete from vaksinasi_1 where id = ?', [$request->id]);

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Edit Data",
        ),200); 

    }

    public function akumulasiData($tahap)
    {
        // $data = DB::select("SELECT COUNT(id) AS Total FROM vaksinasi_1 WHERE tahap = ?", [$tahap])

        // return response()->json(array(
        //     "success" => TRUE,
        //     "message" => "Berhasil Edit Data",
        //     "data" => $
        // ),200); 

    }

    public function seriesPertumbuhanVaksinasi()
    {
        $data = DB::select("SELECT t.day AS date, @running_total:=@running_total + t.vaksinasi_count AS value FROM ( SELECT date(penyelenggaraan_date) as day, sum(total) as vaksinasi_count FROM vaksinasi_1 GROUP BY day ) t JOIN (SELECT @running_total:=0) r ORDER BY t.day", [1]);
        
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil mendapatkan data",
            "data" => $data,
        ),200);
    }
    
}