import React, { Suspense, useEffect, lazy, useState } from 'react'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	Link,
	useRouteMatch,
} from 'react-router-dom'

import Swal from 'sweetalert2'

import {
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	Input,
	Label,
	Textarea,
	ModalFooter,
} from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import {
	addRS,
	addVaksinRecord,
	deleteRS,
	deleteVaksinRecord,
	editRS,
	editVaksinRecord,
	getLastUpdated,
} from '../data/repository'
import WilayahOption from '../components/wilayahoption/WilayahOption'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'
import PinLokasi from '../components/pinlokasi/PinLokasi'

const TableRS = lazy(() => import('../components/portaldatars/TableRS'))

const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

function PortalDataRS() {
	const [ismodalOpen, setIsmodalOpen] = useState(false)
	const [isEditEvent, setIsEditEvent] = useState(false)
	const [editId, setEditId] = useState(0)
	const [lokasi, setLokasi] = useState([-2.548926, 118.0148634])
	const [namaRS, setNamaRS] = useState('')
	const [alamatRS, setAlamatRS] = useState('')
	const [refreshtable, setRefreshtable] = useState(0)
	const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false)
	const [kodewil, setKodewil] = useState('')

	const addNewData = () => {
		if (namaRS === '' || alamatRS === '' || kodewil == '') {
			failedAlert('Lengkapilah Form !!')
		} else {
			addRS(namaRS, alamatRS, lokasi[0], lokasi[1], kodewil)
				.then((res) => {
					successAlert('Berhasil Menambah Data')
					setIsmodalOpen(false)
					console.log(res)
					setRefreshtable(refreshtable + 1)
				})
				.catch((err) => {
					failedAlert('Gagal Menambahkan Data')
					setIsmodalOpen(false)
					console.log(err)
				})
		}
	}
	const editData = () => {
		if (namaRS === '' || alamatRS === '') {
			failedAlert('Lengkapilah Form !!')
		} else {
			console.log('EDIT WIL ', kodewil)
			editRS(editId, namaRS, alamatRS, lokasi[0], lokasi[1], kodewil)
				.then((res) => {
					successAlert('Berhasil Edit Data')
					setIsmodalOpen(false)
					console.log(res)
					setRefreshtable(refreshtable + 1)
				})
				.catch((err) => {
					failedAlert('Gagal Edit Data')
					setIsmodalOpen(false)
					console.log(err)
				})
		}
	}
	const successAlert = (msg) => {
		Swal.fire({
			title: 'Berhasil!',
			text: msg,
			icon: 'success',
			confirmButtonText: 'Oke',
		})
	}

	const failedAlert = (msg) => {
		Swal.fire({
			title: 'Error!',
			text: msg,
			icon: 'error',
			confirmButtonText: 'Oke',
		})
	}
	const clearForm = () => {
		setNamaRS('')
		setAlamatRS('')
		setLokasi([-2.548926, 118.0148634])
	}
	const openAddNewModal = () => {
		clearForm()
		setIsEditEvent(false)
		setIsmodalOpen(true)
	}

	const buildEditForm = (data) => {
		setIsEditEvent(true)
		////console.log(data)
		setKodewil(data.kode_wilayah)
		setEditId(data.id)
		setNamaRS(data.nama)
		setAlamatRS(data.alamat)
		setLokasi([data.Lng, data.Lat])
		setIsmodalOpen(true)
	}

	const deleteData = () => {
		deleteRS(editId)
			.then(() => {
				setRefreshtable(refreshtable + 1)
				successAlert('Berhasil Menghapus Data')
				isDeleteModalOpen(false)
			})
			.catch((err) => {
				failedAlert('Gagal menghapus data')
				isDeleteModalOpen(false)
			})
	}

	return (
		<div className='px-10 py-5'>
			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				tag={Link}
				to='/app/covidupdate'
			>
				<div className='flex items-center'>
					<span>
						Master Data Rumah Sakit
						{/* {kodewil} {penyelenggara} TGL : {date}{' '}
						{tahapVaksin} {alamat} {totalTervaksin} {jenisVaksin} {lokasi} */}
					</span>
				</div>
				<Button
					onClick={openAddNewModal}
					className='text-white font-bold'
					size='small'
				>
					TAMBAH DATA
				</Button>
			</a>
			<Suspense fallback={<ThemedSuspense />}>
				<Suspense fallback={<ThemedSuspense />}>
					<TableRS
						refreshTable={refreshtable}
						onEditClicked={(e) => {
							buildEditForm(e)
						}}
						onDeleteClicked={(e) => {
							setEditId(e.id)
							setIsDeleteModalOpen(true)
						}}
					/>
				</Suspense>
			</Suspense>
			<Modal isOpen={ismodalOpen} onClose={() => setIsmodalOpen(false)}>
				<ModalBody className='overflow-y-auto h-screen py-5 px-3 scrollbar-hide'>
					<ModalHeader>{isEditEvent ? 'Edit' : 'Tambah'} Data</ModalHeader>
					<WilayahOption
						kodewilValue={kodewil}
						onWilayahChanged={(wil) => setKodewil(wil)}
					/>
					<Label className='pt-4'>
						<span>Nama Rumah Sakit</span>
						<Input
							defaultValue={namaRS}
							onChange={(e) => setNamaRS(e.target.value)}
							placeholder='Ex. Rs. ...'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Alamat</span>
						<Textarea
							defaultValue={alamatRS}
							onChange={(e) => setAlamatRS(e.target.value)}
							placeholder='Ex. Jl. Kyai Mojo ...'
							rows='3'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Pin Lokasi</span>
						<PinLokasi
							lokasiVal={lokasi}
							onLocationPinned={(e) => setLokasi(e)}
						/>
					</Label>
					<div className='mt-5 hidden sm:block float-right'>
						<Button
							onClick={() => {
								isEditEvent ? editData() : addNewData()
							}}
						>
							Simpan
						</Button>
					</div>
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>
			<Modal
				isOpen={isDeleteModalOpen}
				onClose={() => setIsDeleteModalOpen(false)}
			>
				<ModalHeader>Delete Data</ModalHeader>
				<ModalBody>Apakah anda yakin untuk menghapus ? </ModalBody>
				<ModalFooter>
					<div className='mt-5 hidden sm:block float-right'>
						<Button onClick={deleteData}>Hapus</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	)
}

export default PortalDataRS
