import React, { useState, useMemo, useEffect } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import faker from 'faker'
import Swal from 'sweetalert2'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'

import {
	Button,
	Input,
	Modal,
	Select,
	ModalBody,
	ModalFooter,
	ModalHeader,
	Label,
	Textarea,
} from '@windmill/react-ui'
import {
	addKamarRS,
	getAllJenisKamar,
	getAllRS,
	getInfoRS,
	getJenisKamarByRS,
	getKamarByRS,
	updateKamarRS,
} from '../../data/repository'
import { useParams, Link } from 'react-router-dom'
import moment from 'moment'

const TextField = styled.input`
	height: 32px;
	width: 200px;
	border-radius: 3px;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	border: 1px solid #e5e5e5;
	padding: 0 32px 0 16px;

	&:hover {
		cursor: pointer;
	}
`

const ClearButton = styled(Button)`
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	height: 34px;
	width: 32px;
	text-align: center;
	display: flex;
	align-items: center;
	justify-content: center;
`
const FilterComponent = ({ filterText, onFilter, onClear }) => (
	<>
		<TextField
			id='search'
			type='text'
			placeholder='Cari Kamar'
			aria-label='Search Input'
			value={filterText}
			onChange={onFilter}
		/>
		<ClearButton type='button' onClick={onClear}>
			X
		</ClearButton>
	</>
)

const columns = [
	{
		name: 'Kamar',
		selector: (row) => row.kamar,
		wrap: true,
		sortable: true,
	},
	{
		name: 'Total',
		selector: (row) => row.total,
		sortable: true,
	},
	{
		name: 'Terpakai',
		selector: (row) => row.terpakai,
		sortable: true,
	},
	{
		name: 'Antrian',
		selector: (row) => row.antrian,
		sortable: true,
	},
	{
		name: 'Tanggal Data Terakhir',
		selector: (row) => row.tanggal,
		sortable: true,
		wrap: true,
	},
	{
		name: 'Terakhir di update oleh',
		selector: (row) => row.last_update_by,
		sortable: true,
	},
	{
		name: 'Opsi',
		selector: (row) => row.opsi,
		wrap: true,
	},
	{
		name: 'Riwayat',
		selector: (row) => row.riwayat,
		wrap: true,
	},
]
const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}
function TableKamar({ refreshTable = 0 }) {
	const [filterText, setFilterText] = useState('')
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false)
	const [tableData, setTableData] = useState([])
	const [ismodalOpen, setIsmodalOpen] = useState(false)
	const [isAddmodalOpen, setIsAddmodalOpen] = useState(false)
	const [jenisKamar, setJeniskamar] = useState([])
	const [_jenisKamar, _setJeniskamar] = useState(0)

	const [_namaJenisKamar, _setNamaJenisKamar] = useState(0)
	const [_totalKamar, _setTotalKamar] = useState(0)
	const [_totalTerpakai, _setTotalTerpakai] = useState(0)
	const [_totalAntrian, _setTotalAntrian] = useState(0)
	const [_tanggal, setTanggal] = useState(
		moment().format('YYYY-MM-DD') + ' ' + moment().format('HH:mm:ss')
	)

	const [rsInfoData, setRsInfoData] = useState({
		id: 0,
		nama: '-',
		alamat: '-',
		Lat: 109.01419881731,
		Lng: 0.88231164485945,
	})
	let { rsid } = useParams()
	const getData = () => {
		getKamarByRS(rsid)
			.then((res) => {
				let raw = res.data.data
				console.log(raw)
				let datas = []
				raw.map((val) => {
					datas.push({
						kamar: val.jenis,
						total: val.total,
						terpakai: val.terpakai,
						antrian: val.antrian,
						last_update_by: val.updated_by,
						tanggal: moment(val.tanggal).format('DD-MM-YYYY HH:mm:ss'),
						opsi: (
							<>
								<Link>
									<button
										onClick={() => buildupdateForm(val)}
										className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-1 px-2 rounded'
									>
										Update
									</button>
								</Link>
							</>
						),
						riwayat: (
							<Link
								to={`/app/siranapdata/riwayat/${rsid}/${val.jenis_kamar_id}`}
							>
								<button className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-1 px-2 rounded'>
									Riwayat
								</button>
							</Link>
						),
					})
				})
				console.log(datas)
				setTableData(datas)
				console.log('tabel data')
				console.log(tableData)
			})
			.catch((err) => {
				console.log(err)
				setTableData([])
			})
	}

	const buildupdateForm = (data) => {
		setTanggal(
			moment().format('YYYY-MM-DD') + ' ' + moment().format('HH:mm:ss')
		)
		_setJeniskamar(data.jenis_kamar_id)
		_setNamaJenisKamar(data.jenis)
		_setTotalKamar(data.total)
		_setTotalTerpakai(data.terpakai)
		_setTotalAntrian(data.antrian)
		setIsmodalOpen(true)
	}

	const filteredItems = tableData.filter((item) => {
		return (
			item.kamar && item.kamar.toLowerCase().includes(filterText.toLowerCase())
		)
	})

	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle)
				setFilterText('')
			}
		}

		return (
			<FilterComponent
				onFilter={(e) => setFilterText(e.target.value)}
				onClear={handleClear}
				filterText={filterText}
			/>
		)
	}, [filterText, resetPaginationToggle])

	const getRsInfo = () => {
		getInfoRS(rsid)
			.then((res) => {
				console.table(res.data.data)
				setRsInfoData(res.data.data)
			})
			.catch((err) => {
				console.log(err)
			})
	}

	const getJenisKamar = () => {
		getJenisKamarByRS(rsid)
			.then((res) => {
				setJeniskamar(res.data.data)
			})
			.catch((err) => {
				console.log(err)
			})
	}
	const successAlert = (msg) => {
		Swal.fire({
			title: 'Berhasil!',
			text: msg,
			icon: 'success',
			confirmButtonText: 'Oke',
		})
	}

	const failedAlert = (msg) => {
		Swal.fire({
			title: 'Error!',
			text: msg,
			icon: 'error',
			confirmButtonText: 'Oke',
		})
	}
	const addNewKamarRS = () => {
		addKamarRS(rsid, _jenisKamar)
			.then((res) => {
				successAlert('Berhasil menambah Kamar')
				getData()
				setIsAddmodalOpen(false)
			})
			.catch((err) => {
				failedAlert('Gagal menambah kamar')
				setIsAddmodalOpen(false)
			})
	}

	useEffect(() => {
		getJenisKamar()
	}, [0])

	useEffect(() => {
		getData()
		getRsInfo()
	}, [0, refreshTable])

	const updateKamar = () => {
		console.table({
			rsid: rsid,
			jenis_kamar: _jenisKamar,
			total: _totalKamar,
			terpakai: _totalTerpakai,
			antrian: _totalAntrian,
			tanggal: _tanggal,
		})
		updateKamarRS(
			rsid,
			_jenisKamar,
			_totalKamar,
			_totalTerpakai,
			_totalAntrian,
			_tanggal
		)
			.then((res) => {
				console.log(res.data)
				getData()
				setIsmodalOpen(false)
				successAlert('Berhasil Update Data')
			})
			.catch((err) => {
				console.log(err)
				failedAlert('Gagal update data')
			})
	}

	return (
		<>
			<a className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'>
				<div className='flex items-center'>
					<span>DATA KAMAR {rsInfoData.nama}</span>
				</div>
				<Button
					onClick={() => {
						getJenisKamar()
						setIsAddmodalOpen(true)
					}}
					className='text-white font-bold float-right'
					size='small'
				>
					Tambah Kamar{' '}
					<span dangerouslySetInnerHTML={{ __html: '&plus;' }}></span>
				</Button>
			</a>
			<DataTable
				columns={columns}
				data={filteredItems}
				pagination
				theme='dark'
				paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
				subHeader
				title='Data Terbaru / Terakhir'
				subHeaderComponent={subHeaderComponentMemo}
				persistTableHead
			/>
			<Modal isOpen={ismodalOpen} onClose={() => setIsmodalOpen(false)}>
				<ModalBody>
					<ModalHeader>Update Data Kamar {_namaJenisKamar}</ModalHeader>
					<Label className=''>
						<span>Tanggal Data</span>
						<Flatpickr
							options={{ dateFormat: 'd-m-Y' }}
							onChange={([date]) => {
								setTanggal(
									moment(date).format('YYYY-MM-DD') +
										' ' +
										moment().format('HH:mm:ss')
								)
							}}
							render={({ defaultValue, value, ...props }, ref) => {
								return (
									<CustomInput
										onChange={(e) => {
											setTanggal(
												moment(e.target.value).format('YYYY-MM-DD') +
													' ' +
													moment().format('HH:mm:ss')
											)
										}}
										defaultValue={defaultValue}
										inputRef={ref}
									/>
								)
							}}
						/>
					</Label>
					<Label className='pt-4'>
						<span>Jumlah Kamar</span>
						<Input
							defaultValue={_totalKamar}
							onChange={(e) => {
								_setTotalKamar(e.target.value)
							}}
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Jumlah Kamar Terpakai</span>
						<Input
							defaultValue={_totalTerpakai}
							onChange={(e) => {
								_setTotalTerpakai(e.target.value)
							}}
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Jumlah Antrian Kamar</span>
						<Input
							defaultValue={_totalAntrian}
							onChange={(e) => {
								_setTotalAntrian(e.target.value)
							}}
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<div className='mt-5 hidden sm:block float-right'>
						<Button onClick={updateKamar}>Simpan</Button>
					</div>
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>
			<Modal isOpen={isAddmodalOpen} onClose={() => setIsAddmodalOpen(false)}>
				<ModalBody>
					<ModalHeader>Tambah Kamar</ModalHeader>
					<Label className='pt-4'>
						<span>Pilih Jenis Kamar</span>
						<Select
							onChange={(e) => {
								_setJeniskamar(e.target.value)
							}}
							className='mt-1 border-solid border-2 rounded'
						>
							{jenisKamar.map((v) => (
								<option key={v.id} value={v.id}>
									{v.jenis}
								</option>
							))}
						</Select>
					</Label>
					<div className='mt-5 hidden sm:block float-right'>
						<Button onClick={addNewKamarRS}>Simpan</Button>
					</div>
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>
		</>
	)
}

export default TableKamar
