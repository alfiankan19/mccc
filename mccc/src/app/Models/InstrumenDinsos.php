<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class InstrumenDinsos
{
    public static function updateRecord($data, $uid)
    {

        $editData = array(
            'kodewil' => $data->kodewil,  
            'nama' => $data->nama,  
            'jenisKelamin' => $data->jenisKelamin,  
            'pendidikanTerakhir' => $data->pendidikanTerakhir,  
            'jabatan' => $data->jabatan,  
            'lamaMenjabat' => $data->lamaMenjabat,  
            'nomorHP' => $data->nomorHP,  
            'kontribusi11' => $data->kontribusi11,  
            'kontribusi12' => $data->kontribusi12,  
            'kontribusi13' => $data->kontribusi13,  
            'kontribusi14' => $data->kontribusi14,  
            'kontribusi2' => $data->kontribusi2,  
            'kontribusi3' => $data->kontribusi3,  
            'kontribusi4' => $data->kontribusi4,  
            'supervisi1' => $data->supervisi1,  
            'supervisi31' => $data->supervisi31,  
            'supervisi32' => $data->supervisi32,  
            'supervisi33' => $data->supervisi33,  
            'supervisi34' => $data->supervisi34,  
            'alamat' => $data->alamat,
        );


        $responden = DB::table('instrumen_dinsos')
        ->where('uid', $uid)
        ->update($editData);
        return $responden;
    }

    public static function newRecord($uid)
    {
        $responden = DB::table('instrumen_dinsos')->insertGetId([
            'uid' => $uid
        ]);
        return $responden;
    }

    public static function ifExist($uid)
    {
        return DB::table('instrumen_dinsos')
        ->where('uid',$uid)
        ->get()
        ->first();
    }


    public static function getRecord($uid)
    {
        return DB::table('instrumen_dinsos')
        ->where('uid',$uid)
        ->get()
        ->first();
    }
}