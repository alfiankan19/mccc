import React, { Suspense, useEffect, lazy, useState } from 'react'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	Link,
	useRouteMatch,
} from 'react-router-dom'

import PageTitle from '../components/Typography/PageTitle'
import { Button } from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import { getLastUpdated } from '../data/repository'

const TabelKasusKabupaten = lazy(() =>
	import('../pages/subpages/PortalDataCovid/TabelKasusKabupaten')
)
const TabelDetailKasusKabupaten = lazy(() =>
	import('../pages/subpages/PortalDataCovid/TabelDetailKasusKabupaten')
)
function PortalDataCovid() {
	let { path, url } = useRouteMatch()
	const [lastUpdated, setLastUpdated] = useState(0)

	const getLastUpdatedDate = () => {
		getLastUpdated().then((result) => {
			////console.log(result)
			setLastUpdated(
				moment(result.data.data.log_time)
					.add(7, 'hour')
					.format('DD-MM-YY hh:mm')
			)
		})
	}

	useEffect(() => {
		getLastUpdatedDate()
	}, [0])

	return (
		<div className='px-10 py-5'>
			<PageTitle>Portal Data Covid</PageTitle>

			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				tag={Link}
				to='/app/covidupdate'
			>
				<div className='flex items-center'>
					<span>Terakhir data di update : {lastUpdated}</span>
				</div>
				<Button
					tag={Link}
					to='/app/covidupdate'
					className='text-white font-bold'
					size='small'
				>
					UPDATE DATA{' '}
					<span dangerouslySetInnerHTML={{ __html: '&RightArrow;' }}></span>
				</Button>
			</a>
			<Suspense fallback={<ThemedSuspense />}>
				<Switch>
					<Route exact path={path}>
						<Suspense fallback={<ThemedSuspense />}>
							<TabelKasusKabupaten />
						</Suspense>
					</Route>
					<Route path={`${path}/detail/:kodewil`}>
						<Suspense fallback={<ThemedSuspense />}>
							<TabelDetailKasusKabupaten onUpdatedData={getLastUpdatedDate} />
						</Suspense>
					</Route>
				</Switch>
			</Suspense>
		</div>
	)
}

export default PortalDataCovid
