<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;
use Illuminate\Support\Facades\DB;

class WilayahController extends Controller
{

    public function kabupaten($provid)
    {

        $kabupatens = DB::table('wilayah_kabupaten')->where('provinsi_id', $provid)->select(DB::raw('id AS kpu_idkab, nama'))->orderBy('nama', 'asc')->get();

        //
        return response()->json($kabupatens);
    }

    public function kecamatan($idkab)
    {

        $kecamatans = DB::table('wilayah_kecamatan')->where('kabupaten_id', $idkab)->select(DB::raw('id AS kpu_idkec, nama'))->orderBy('nama', 'asc')->get();

        //
        return response()->json($kecamatans);
    }


    public function desa($idkec)
    {

        $desa = DB::table('wilayah_desa')->where('kecamatan_id',$idkec)->select(DB::raw('id AS id_desa, nama'))->orderBy('nama', 'asc')->get();

        return response()->json($desa);
    }


    public function wilayahResolver($kodewil)
    {
        # code...
    }
}