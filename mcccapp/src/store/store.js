import { configureStore } from '@reduxjs/toolkit'
import mapReducer from '../components/maps/mapSlice'
import uploaderReducer from '../components/uploader/uploaderSlice'
import covidReducer from '../components/portaldatacovid/covidSlice'

export default configureStore({
	reducer: {
		map: mapReducer,
		uploader: uploaderReducer,
		covid: covidReducer,
	},
})
