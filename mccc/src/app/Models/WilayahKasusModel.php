<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class WilayahKasusModel
{
   

    public static function getTotalDataKabupaten($kabid)
    {
        $terkonfirmasi = DB::table('kasus_terkonfirmasi')->where('kab_id', $kabid)->select(DB::raw('SUM(total) AS total'))->first();
        $sembuh = DB::table('kasus_sembuh')->where('kab_id', $kabid)->select(DB::raw('SUM(total) AS total'))->first();
        $meninggal = DB::table('kasus_meninggal')->where('kab_id', $kabid)->select(DB::raw('SUM(total) AS total'))->first();
        return array(
            "terkonfirmasi" => $terkonfirmasi,
            "sembuh" => $sembuh,
            "meninggal" => $meninggal,
        );
    }

    public static function addDataCovid($data, $tipe)
    {
        switch ($tipe) {
            case 'terkonfirmasi':
                $exist = DB::table('kasus_terkonfirmasi')->where('kab_id', $data["kab_id"])->where('date_series',$data["date_series"])->first();
                if($exist){
                    return DB::table('kasus_terkonfirmasi')->where('id', $exist->id)->update($data);
                }else{
                    return DB::table('kasus_terkonfirmasi')->insert($data);
                }
                
                break;
            case 'sembuh':
                $exist = DB::table('kasus_sembuh')->where('kab_id', $data["kab_id"])->where('date_series',$data["date_series"])->first();
                if($exist){
                    return DB::table('kasus_sembuh')->where('id', $exist->id)->update($data);
                }else{
                    return DB::table('kasus_sembuh')->insert($data);
                }
                break;
            case 'meninggal':
                $exist = DB::table('kasus_meninggal')->where('kab_id', $data["kab_id"])->where('date_series',$data["date_series"])->first();
                if($exist){
                    return DB::table('kasus_meninggal')->where('id', $exist->id)->update($data);
                }else{
                    return DB::table('kasus_meninggal')->insert($data);
                }
                break;
            
            default:
                return false;
                break;
        }
        
    }

    public static function getSeriesKabupaten($kabid, $dateStart, $dateEnd)
    {

        return DB::select(
            "SELECT 
            a.kab_id, 
            a.date_series, 
            a.total AS Terkonfirmasi, 
            b.total AS Sembuh, 
            c.total AS Meninggal, 
            d.nama,
            CONCAT((SELECT username FROM users WHERE id=a.created_by),'/',(SELECT username FROM users WHERE id=b.created_by),'/',(SELECT username FROM users WHERE id=c.created_by)) AS Editor 
            FROM kasus_terkonfirmasi a 
            JOIN kasus_sembuh b ON a.kab_id = b.kab_id AND a.date_series = b.date_series 
            JOIN kasus_meninggal c ON c.kab_id = a.kab_id AND c.date_series = a.date_series 
            JOIN wilayah_kabupaten d ON id = a.kab_id
            WHERE a.kab_id = ? AND a.date_series BETWEEN ? AND ? ORDER BY a.date_series DESC", 
            [$kabid,$dateStart, $dateEnd]);
    }

    public static function updateSingleSeries($target, $kabid, $date, $total, $user)
    {
        switch ($target) {
            case 'terkonfirmasi':
                return DB::table('kasus_terkonfirmasi')->sharedLock()->where('kab_id', $kabid)->where('date_series', $date)->update(['total' => $total, 'created_by' => $user->id]);
                break;
            case 'sembuh':
                return DB::table('kasus_sembuh')->sharedLock()->where('kab_id', $kabid)->where('date_series', $date)->update(['total' => $total, 'created_by' => $user->id]);
                break;
            case 'meninggal':
                return DB::table('kasus_meninggal')->sharedLock()->where('kab_id', $kabid)->where('date_series', $date)->update(['total' => $total, 'created_by' => $user->id]);
                break;
            default:
                break;
        }
    }

    public static function getSeriesProvinsiTerkonfirmasi($provid = '')
    {
        if($provid == '') {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_terkonfirmasi GROUP BY date_series");
        } else {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_terkonfirmasi WHERE SUBSTRING(kab_id,1,2) = ? GROUP BY date_series", [$provid]);
        }
        
    }

    public static function getSeriesProvinsiSembuh($provid = '')
    {
        if($provid == '') {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_sembuh GROUP BY date_series");
        } else {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_sembuh WHERE SUBSTRING(kab_id,1,2) = ? GROUP BY date_series", [$provid]);
        }

        
        
    }

    public static function getSeriesProvinsiMeninggal($provid = '')
    {
        if($provid == '') {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_meninggal GROUP BY date_series");
        } else {
            return DB::select(
                "SELECT date_series AS date,kab_id AS name,SUM(total) AS value FROM kasus_meninggal WHERE SUBSTRING(kab_id,1,2) = ? GROUP BY date_series", [$provid]);
        }
        
    }

    public static function getStackedSeries($provid = '')
    {
        // {wilayah: 'Solo',
        // terkonfirmasi: 2.5,
        // aktif: 2.5,
        // sembuh: 2.1,
        // meninggal: 0.3}
        if($provid == '') {
            return DB::select(
                "SELECT
                (SELECT nama FROM wilayah_provinsi WHERE id = SUBSTRING(a.kab_id,1,2) LIMIT 1) AS wilayah,
                SUM(a.total) AS terkonfirmasi,
                SUM(b.total) AS sembuh,
                SUM(c.total) AS meninggal

                FROM kasus_terkonfirmasi a
                JOIN kasus_sembuh b ON a.date_series = b.date_series AND a.kab_id = b.kab_id
                JOIN kasus_meninggal c on a.date_series = c.date_series AND a.kab_id = c.kab_id
                GROUP BY SUBSTRING(a.kab_id,1,2)");
        } else {
            return DB::select(
                "SELECT
                (SELECT nama FROM wilayah_kabupaten WHERE id = SUBSTRING(a.kab_id,1,4) LIMIT 1) AS wilayah,
                SUM(a.total) AS terkonfirmasi,
                SUM(b.total) AS sembuh,
                SUM(c.total) AS meninggal
                FROM kasus_terkonfirmasi a
                JOIN kasus_sembuh b ON a.date_series = b.date_series AND a.kab_id = b.kab_id
                JOIN kasus_meninggal c on a.date_series = c.date_series AND a.kab_id = c.kab_id
                WHERE SUBSTRING(a.kab_id,1,2) = ?
                GROUP BY a.kab_id", [$provid]);
        }
        
    }

}
