<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;
use App\Models\WilayahKasusModel;
use App\Models\WilayahModel;
use App\Models\User;
use App\Models\DataVaksinModel;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DataKabupaten;
use Illuminate\Support\Facades\DB;


class Siranap extends Controller
{
    public function infoRS($id)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Get Data",
            "data" => DB::table('rumah_sakit')->select('id', 'nama', 'alamat', DB::raw("ST_X(Lokasi) AS Lat,ST_Y(Lokasi) AS Lng"))->where('id', $id)->first(),
        ),200); 
    }
    public function deleteRS(Request $request)
    {
        DB::table('rumah_sakit')->where('id', $request->id)->delete();
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menghapus Data",
        ),200); 
    }
    public function addRS(Request $request)
    {
        DB::table('rumah_sakit')->insert(array(
            'nama' => $request->nama,
            'kode_wilayah' => $request->kodewil,
            'alamat' => $request->alamat,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT($request->lng $request->lat)',1)")
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
        ),200); 
    }

    public function getRS()
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Get Data",
            "data" => DB::table('rumah_sakit')->select('id', 'nama', 'kode_wilayah', 'alamat', DB::raw("ST_X(Lokasi) AS Lat,ST_Y(Lokasi) AS Lng"))->get(),
        ),200); 
    }

    public function editRS(Request $request)
    {
        DB::table('rumah_sakit')->where('id', $request->id)->update(array(
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'kode_wilayah' => $request->kode_wilayah,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT($request->lng $request->lat)',1)")
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
        ),200); 
    }

    public function getJenisKamar(Request $request)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
            "data" => DB::table('jenis_kamar_rs')->get(),
        ),200); 
    }

    public function getJenisKamarByRS($rsid)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
            "data" => DB::select("SELECT * FROM jenis_kamar_rs WHERE id NOT IN (SELECT jenis_kamar_id FROM kamar_rs WHERE rs_id = ?)", [$rsid]),
        ),200); 
    }

    public function addJenisKamar(Request $request)
    {
        DB::table('jenis_kamar_rs')->insert(array(
            'jenis' => $request->jenis,
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
        ),200); 
    }
    public function editJenisKamar(Request $request)
    {
        DB::table('jenis_kamar_rs')->where('id', $request->id)->update(array(
            'jenis' => $request->jenis,
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
        ),200); 
    }

    function getKamarByRSID($rsid)
    {
        $data = DB::select("SELECT * FROM kamar_rs WHERE rs_id = ? GROUP BY jenis_kamar_id", [$rsid]);

        foreach ($data as $val) {
            $lk = DB::table('kamar_rs')->where('rs_id', $rsid)->where('jenis_kamar_id', $val->jenis_kamar_id)->orderBy('date_time', 'desc')->first();
            $val->updated_by = DB::table('users')->where('id', $val->update_by)->first()->username;
            $val->jenis = DB::table('jenis_kamar_rs')->where('id', $val->jenis_kamar_id)->first()->jenis;
            $val->rs_id = $lk->rs_id;
            $val->jenis_kamar_id = $lk->jenis_kamar_id;
            $val->total = $lk->total;
            $val->terpakai = $lk->terpakai;
            $val->antrian = $lk->antrian;
            $val->tanggal = $lk->date_time;
        }
        return $data;
    }

    public function getKamarByRS($rsid)
    {
        $data = $this->getKamarByRSID($rsid);

        // foreach ($data as $val) {
        //     $lk = DB::table('kamar_rs')->where('rs_id', $rsid)->where('jenis_kamar_id', $val->jenis_kamar_id)->first();
        //     $val->updated_by = DB::table('users')->where('id', $val->update_by)->first()->username;
        //     $val->jenis = DB::table('jenis_kamar_rs')->where('id', $val->jenis_kamar_id)->first()->jenis;
        //     $val->rs_id = $lk->rs_id;
        //     $val->jenis_kamar_id = $lk->jenis_kamar_id;
        //     $val->total = $lk->total;
        //     $val->terpakai = $lk->terpakai;
        //     $val->antrian = $lk->antrian;
        //     $val->tanggal = $lk->date_time;
        // }

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
            "data" => $data,
        ),200); 
    }

    public function addKamarRS(Request $request)
    {
        $user = Auth::user();
        $data = DB::table('kamar_rs')->insert(array(
            'rs_id' => $request->rs_id,
            'jenis_kamar_id' => $request->jenis_kamar,
            'total' => 0,
            'terpakai' => 0,
            'antrian' => 0,
            'update_by' => $user->id,
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Menambah Data",
            "data" => $data,
        ),200); 
    }

    public function updateKamarRS(Request $request)
    {
        $user = Auth::user();
        $data = DB::table('kamar_rs')->insert(array(
            'rs_id' => $request->rs_id,
            'jenis_kamar_id' => $request->jenis_kamar,
            'total' => $request->total,
            'terpakai' => $request->terpakai,
            'antrian' => $request->antrian,
            'date_time' => $request->tanggal,
            'update_by' => $user->id,
        ));
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Edit Data",
            "data" => $data,
        ),200); 
    }

    public function infoKamarRS($rsid, $jnskmr)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Get Data",
            "data" => DB::select("SELECT a.rs_id, c.nama, b.jenis, MAX(a.date_time) AS date_time FROM kamar_rs a JOIN jenis_kamar_rs b ON a.jenis_kamar_id = b.id JOIN rumah_sakit c ON c.id = a.rs_id WHERE a.rs_id = ? AND a.jenis_kamar_id = ?", [$rsid, $jnskmr]),
        ),200); 
    }
    public function riwayatKamarRS($rsid, $jnskmr)
    {
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Get Data",
            "data" => DB::select("SELECT a.*, b.*, c.username AS updater_username FROM kamar_rs a JOIN jenis_kamar_rs b ON a.jenis_kamar_id = b.id JOIN users c ON c.id = a.update_by WHERE a.rs_id = ? AND a.jenis_kamar_id = ? ORDER BY a.date_time DESC", [$rsid, $jnskmr]),
        ),200); 
    }

    public function getRSbyKodewil($kodewil)
    {
        $rs = DB::select("SELECT 
        id,
        nama,
        (SELECT nama FROM wilayah_provinsi WHERE id = SUBSTRING(kode_wilayah,1,2) LIMIT 1) AS provinsi,
        (SELECT nama FROM wilayah_kabupaten WHERE id = SUBSTRING(kode_wilayah,1,4) LIMIT 1) AS kabupaten,
        (SELECT nama FROM wilayah_kecamatan WHERE id = SUBSTRING(kode_wilayah,1,7) LIMIT 1) AS kecamatan,
        (SELECT nama FROM wilayah_desa WHERE id = kode_wilayah LIMIT 1) AS desa,
        alamat
        FROM rumah_sakit WHERE kode_wilayah LIKE ?", [$kodewil.'%']);

        foreach($rs as $val){
            //get kamar
            
            $kamar = $this->getKamarByRSID($val->id);
            $val->kamar = $kamar;
        }

        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil Get Data",
            "data" => $rs,
        ),200); 
    }
}