import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import Loader from 'react-loader-spinner'
import { Label, Input, Button } from '@windmill/react-ui'
import { login } from '../data/repository'

function Login() {
	const history = useHistory()
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const goLogin = () => {
		setIsLoading(true)
		login(email, password)
			.then((response) => {
				////console.log(response.data.data.access_token)
				if (response.data.data.access_token != null) {
					localStorage.setItem('JWT', response.data.data.access_token)
					history.push('/app/dashboard')
				}
			})
			.catch((err) => {
				////console.log(err)
				setIsLoading(false)
			})
	}

	useEffect(() => {
		if (localStorage.getItem('JWT')) {
			history.push('/app/dashboard')
		}
	}, [0])

	return (
		<div className='flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900'>
			<div className='flex-1 h-full max-w-xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800'>
				<main className='flex items-center justify-center p-6 sm:p-12'>
					<div className='w-full'>
						<h1 className='mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200'>
							Login
						</h1>
						<Label>
							<span>Email</span>
							<Input
								onChange={(e) => setEmail(e.target.value)}
								className='mt-1'
								type='email'
								placeholder='email@email.com'
							/>
						</Label>

						<Label className='mt-4'>
							<span>Password</span>
							<Input
								className='mt-1'
								onChange={(e) => setPassword(e.target.value)}
								type='password'
								placeholder='***************'
							/>
						</Label>
						<center>
							<Loader
								className='p-5'
								type='Oval'
								visible={isLoading}
								color='#00BFFF'
								height={30}
								width={30}
							/>
						</center>
						{!isLoading ? (
							<Button className='mt-4' block onClick={goLogin}>
								Log in
							</Button>
						) : null}

						<hr className='my-8' />

						<p className='mt-4'>
							<Link
								className='text-sm font-medium text-purple-600 dark:text-purple-400 hover:underline'
								to='/forgot-password'
							>
								Lupa Password?
							</Link>
						</p>
						<p className='mt-1'>
							<Link
								className='text-sm font-medium text-purple-600 dark:text-purple-400 hover:underline'
								to='/'
							>
								Daftar
							</Link>
						</p>
					</div>
				</main>
			</div>
		</div>
	)
}

export default Login
