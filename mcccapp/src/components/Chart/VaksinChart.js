import React, { useEffect, useRef, useState } from 'react'

import * as am4core from '@amcharts/amcharts4/core'
import * as am4charts from '@amcharts/amcharts4/charts'
import chart_dark from '@amcharts/amcharts4/themes/dark'
import chart_light from '@amcharts/amcharts4/themes/animated'
import PageTitle from '../../components/Typography/PageTitle'
import Loader from 'react-loader-spinner'
import { getVaksinChartPerkembangan } from '../../data/repository'

// eslint-disable-next-line react/prop-types
function VaksinChart({ idchart }) {
	const chartRef = useRef(null)
	const [isLoading, setIsLoading] = useState(false)
	const [chartData, setChartData] = useState([])

	const getChartData = (provid) => {
		setIsLoading(true)
		getVaksinChartPerkembangan()
			.then((result) => {
				//console.log('refresh', provid)
				setChartData(result.data.data)
				setIsLoading(false)
			})
			.catch((err) => {
				console.log(err)
				setIsLoading(false)
			})
	}

	const createSeries = (chart, field, name) => {
		var series = chart.series.push(new am4charts.LineSeries())
		series.dataFields.valueY = field
		series.dataFields.dateX = 'date'
		series.name = name
		series.tooltipText = '{dateX}: [b]{valueY}[/]'
		series.strokeWidth = 2

		series.smoothing = 'monotoneX'

		var bullet = series.bullets.push(new am4charts.CircleBullet())
		bullet.circle.stroke = am4core.color('#fff')
		bullet.circle.strokeWidth = 1

		return series
	}

	useEffect(() => {
		if (!chartRef.current) {
			if (localStorage.getItem('theme') === 'dark') {
				am4core.useTheme(chart_dark)
			} else {
				am4core.useTheme(chart_light)
			}

			let x = am4core.create(idchart, am4charts.XYChart)

			x.paddingRight = 20

			// Create axes
			var dateAxis = x.xAxes.push(new am4charts.DateAxis())

			// dateAxis.baseInterval = {
			// 	timeUnit: 'second',
			// 	count: 1,
			// }

			dateAxis.renderer.grid.template.location = 0
			//dateAxis.renderer.minGridDistance = 50

			var valueAxis = x.yAxes.push(new am4charts.ValueAxis())

			createSeries(x, 'value', 'Perkembangan')

			x.legend = new am4charts.Legend()
			x.cursor = new am4charts.XYCursor()

			let series = x.series.push(new am4charts.LineSeries())
			series.dataFields.dateX = 'date'
			series.dataFields.valueY = 'value'
			series.tooltipText = '{valueY.value}'
			series.fillOpacity = 0.5

			x.cursor = new am4charts.XYCursor()
			let scrollbarX = new am4charts.XYChartScrollbar()
			scrollbarX.series.push(series)
			x.scrollbarX = scrollbarX

			chartRef.current = x
		}

		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		if (chartRef.current) {
			chartRef.current.data = chartData
		}
	}, [chartData])

	useEffect(() => {
		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		getChartData('')
	}, [0])

	return (
		<div className='pt-12'>
			<div className='grid grid-col-2 grid-flow-col gap-1'>
				<div>
					<PageTitle>Pertumbuhan Vaksinasi</PageTitle>
				</div>
				<div>
					<div className='relative inline-block float-right text-gray-700 pt-5'></div>
				</div>
			</div>

			{isLoading ? (
				<center>
					<div className='pt-5'>
						<Loader type='Oval' color='#00BFFF' height={30} width={30} />
					</div>
				</center>
			) : null}
			<div
				id={idchart}
				style={{ width: '100%', height: '500px', borderRadius: 15 }}
			></div>
		</div>
	)
}

export default VaksinChart
