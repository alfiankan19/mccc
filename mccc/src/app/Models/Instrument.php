<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Instrument
{
    

    public static function getAllinstrument()
    {
        return DB::table('jenis_instrumen')->get();
    }

    

    public static function getInstrumentForm($jenis)
    {
        $question =  DB::table('instrumen_question')
            ->join('jenis_instrumen', 'jenis_instrumen.id', '=', 'instrumen_question.jenis_instrumen')
            ->join('jenis_form', 'jenis_form.id', '=', 'instrumen_question.jenis_form')
            ->select(
                'instrumen_question.id AS quest_id', 
                'jenis_instrumen.id AS instrumen_id', 
                'jenis_instrumen.Judul AS judul_instrument', 
                'jenis_instrumen.Sub AS subjudul_instrument', 
                'jenis_form.jenis AS jenis_form', 
                'instrumen_question.inherit AS inherit',
                'instrumen_question.inherit_when_answer AS inherit_when_answer'
            )
            ->where('jenis_instrumen.id',$jenis)
            ->get();

            foreach ($question as $key => $value) {
                $value->answer = DB::table('instrumen_answer')->where('question_id',$value->quest_id)->get();
            }

            return $question;
        

    }

    public static function createRespondent($created_by,$nama,$lokasi,$kodewil,$answer)
    {
        $point = explode(",",$lokasi);
        $responden = DB::table('responden')->insertGetId([
            'created_by' => $created_by,
            'nama' => $nama,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT(".$point[1]." ".$point[0].")')"),
            'kode_wilayah' => $kodewil
        ]);

        

        //bulk insert record

        // [
        //     {
        //         "question":"29",
        //         "answer":null,
        //         "answer_text":null,
        //         "answer_bool":"yes"
        //     }
        // ]
        foreach (json_decode($answer) as $key => $value) {
            DB::table('instrumen_record')->insertGetId([
                'responden' => $responden,
                'question' => $value->question,
                'answer' => $value->answer,
                'answer_text' => $value->answer_text,
                'answer_bool' => $value->answer_bool
            ]);


        }

        return $responden;
        
        
    }



}
