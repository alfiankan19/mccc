<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class PenerimaBantuan
{

    

    public static function getOne($id)
    {
        return DB::table('penerima_bantuan')
        ->select('id', 'uid', 'created_at', 'foto1', 'foto2', 'updated_at','bantuanTambahan', 'bahanBakarMemasak', 'dusun', 'fasilitasMck', 'jenisAtap', 'jenisDindingRumah', 'jenisKelamin', 'jenisLantaiRumah', 'jumlahAnggotaKeluarga', 'kodewil', 'lebarRumah', 'nama', 'panjangRumah', 'pekerjaan', 'pembagianRuangan', 'pendidikan', 'peneranganRumah', 'penganggaran1', 'penganggaran2', 'penganggaran3', 'penganggaran4', 'penganggaran51', 'penganggaran52', 'penganggaran53', 'penganggaran54', 'pengeluaranBulanan', 'perilaku1', 'perilaku2', 'perilaku3', 'perilaku4', 'perilaku5', 'perilaku6', 'perilaku7', 'perilaku8', 'rtrw', 'sirkulasiUdara', 'sosial1', 'sosial2', 'sosial3', 'sosial41', 'sosial42', 'sosial43', 'statusRumah', 'sumberAir', 'umur', DB::raw('ST_X(Lokasi) AS lat'), DB::raw('ST_Y(Lokasi) AS lng'))
        ->where('id',$id)
        ->get()
        ->first();
    }

    public static function deleteOne($id)
    {
        return DB::table('penerima_bantuan')->where('id', $id)->delete();
    }

    public static function getAll($user_id,$page=0,$search="")
    {
        if ($search == ""){
            $page = $page * 10;
            return DB::table('penerima_bantuan')
            ->select('id', 'uid', 'created_at', 'updated_at', 'foto1', 'foto2', 'bahanBakarMemasak','bantuanTambahan', 'dusun', 'fasilitasMck', 'jenisAtap', 'jenisDindingRumah', 'jenisKelamin', 'jenisLantaiRumah', 'jumlahAnggotaKeluarga', 'kodewil', 'lebarRumah', 'nama', 'panjangRumah', 'pekerjaan', 'pembagianRuangan', 'pendidikan', 'peneranganRumah', 'penganggaran1', 'penganggaran2', 'penganggaran3', 'penganggaran4', 'penganggaran51', 'penganggaran52', 'penganggaran53', 'penganggaran54', 'pengeluaranBulanan', 'perilaku1', 'perilaku2', 'perilaku3', 'perilaku4', 'perilaku5', 'perilaku6', 'perilaku7', 'perilaku8', 'rtrw', 'sirkulasiUdara', 'sosial1', 'sosial2', 'sosial3', 'sosial41', 'sosial42', 'sosial43', 'statusRumah', 'sumberAir', 'umur', DB::raw('ST_X(Lokasi) AS lat'), DB::raw('ST_Y(Lokasi) AS lng'))
            ->where('uid',$user_id)
            ->offset($page)
            ->limit(10)
            ->orderBy('id', 'desc')
            ->get();
        } else {
            $page = $page * 10;
            return DB::table('penerima_bantuan')
            ->select('id', 'uid', 'created_at', 'updated_at', 'bahanBakarMemasak','bantuanTambahan', 'dusun', 'fasilitasMck', 'jenisAtap', 'jenisDindingRumah', 'jenisKelamin', 'jenisLantaiRumah', 'jumlahAnggotaKeluarga', 'kodewil', 'lebarRumah', 'nama', 'panjangRumah', 'pekerjaan', 'pembagianRuangan', 'pendidikan', 'peneranganRumah', 'penganggaran1', 'penganggaran2', 'penganggaran3', 'penganggaran4', 'penganggaran51', 'penganggaran52', 'penganggaran53', 'penganggaran54', 'pengeluaranBulanan', 'perilaku1', 'perilaku2', 'perilaku3', 'perilaku4', 'perilaku5', 'perilaku6', 'perilaku7', 'perilaku8', 'rtrw', 'sirkulasiUdara', 'sosial1', 'sosial2', 'sosial3', 'sosial41', 'sosial42', 'sosial43', 'statusRumah', 'sumberAir', 'umur', DB::raw('ST_X(Lokasi) AS lat'), DB::raw('ST_Y(Lokasi) AS lng'))
            ->where('uid',$user_id)
            ->where('nama', 'like', '%'.$search.'%')
            ->offset($page)
            ->limit(10)
            ->orderBy('id', 'desc')
            ->get();
        }
    }
    public static function newSurveyPenerimaBantuan($data, $uid, $foto1, $foto2)
    {
        $point = explode(",",$data->Lokasi);
        $responden = DB::table('penerima_bantuan')->insertGetId([
            'uid' => $uid,
            'bahanBakarMemasak'=> $data->bahanBakarMemasak,
            'dusun'=> $data->dusun,
            'fasilitasMck'=> $data->fasilitasMck,
            'jenisAtap'=> $data->jenisAtap,
            'jenisDindingRumah'=> $data->jenisDindingRumah,
            'jenisKelamin'=> $data->jenisKelamin,
            'jenisLantaiRumah'=> $data->jenisLantaiRumah,
            'jumlahAnggotaKeluarga'=> $data->jumlahAnggotaKeluarga,
            'kodewil'=> $data->kodewil,
            'lebarRumah'=> $data->lebarRumah,
            'nama'=> $data->nama,
            'panjangRumah'=> $data->panjangRumah,
            'pekerjaan'=> $data->pekerjaan,
            'pembagianRuangan'=> $data->pembagianRuangan,
            'pendidikan'=> $data->pendidikan,
            'peneranganRumah'=> $data->peneranganRumah,
            'penganggaran1'=> $data->penganggaran1,
            'penganggaran2'=> $data->penganggaran2,
            'penganggaran3'=> $data->penganggaran3,
            'penganggaran4'=> $data->penganggaran4,
            'penganggaran51'=> $data->penganggaran51,
            'penganggaran52'=> $data->penganggaran52,
            'penganggaran53'=> $data->penganggaran53,
            'penganggaran54'=> $data->penganggaran54,
            'pengeluaranBulanan'=> $data->pengeluaranBulanan,
            'bantuanTambahan' => $data->bantuanTambahan,
            'perilaku1'=> $data->perilaku1,
            'perilaku2'=> $data->perilaku2,
            'perilaku3'=> $data->perilaku3,
            'perilaku4'=> $data->perilaku4,
            'perilaku5'=> $data->perilaku5,
            'perilaku6'=> $data->perilaku6,
            'perilaku7'=> $data->perilaku7,
            'perilaku8'=> $data->perilaku8,
            'rtrw'=> $data->rtrw,
            'sirkulasiUdara'=> $data->sirkulasiUdara,
            'sosial1'=> $data->sosial1,
            'sosial2'=> $data->sosial2,
            'sosial3'=> $data->sosial3,
            'sosial41'=> $data->sosial41,
            'sosial42'=> $data->sosial42,
            'sosial43'=> $data->sosial43,
            'statusRumah'=> $data->statusRumah,
            'sumberAir'=> $data->sumberAir,
            'umur'=> $data->umur,        
            'foto1' => $foto1,
            'foto2' => $foto2,    
            'Lokasi' => DB::raw("ST_GeomFromText('POINT(".$point[1]." ".$point[0].")')"),
        ]);

        return $responden;
    }




    public static function editSurveyPenerimaBantuan($data, $id, $foto1="", $foto2="")
    {
        $point = explode(",",$data->Lokasi);
        $editData = array(
            'bahanBakarMemasak'=> $data->bahanBakarMemasak,
            'dusun'=> $data->dusun,
            'fasilitasMck'=> $data->fasilitasMck,
            'jenisAtap'=> $data->jenisAtap,
            'jenisDindingRumah'=> $data->jenisDindingRumah,
            'jenisKelamin'=> $data->jenisKelamin,
            'jenisLantaiRumah'=> $data->jenisLantaiRumah,
            'jumlahAnggotaKeluarga'=> $data->jumlahAnggotaKeluarga,
            'kodewil'=> $data->kodewil,
            'lebarRumah'=> $data->lebarRumah,
            'nama'=> $data->nama,
            'panjangRumah'=> $data->panjangRumah,
            'pekerjaan'=> $data->pekerjaan,
            'pembagianRuangan'=> $data->pembagianRuangan,
            'pendidikan'=> $data->pendidikan,
            'peneranganRumah'=> $data->peneranganRumah,
            'penganggaran1'=> $data->penganggaran1,
            'penganggaran2'=> $data->penganggaran2,
            'penganggaran3'=> $data->penganggaran3,
            'penganggaran4'=> $data->penganggaran4,
            'penganggaran51'=> $data->penganggaran51,
            'penganggaran52'=> $data->penganggaran52,
            'penganggaran53'=> $data->penganggaran53,
            'penganggaran54'=> $data->penganggaran54,
            'pengeluaranBulanan'=> $data->pengeluaranBulanan,
            'perilaku1'=> $data->perilaku1,
            'perilaku2'=> $data->perilaku2,
            'perilaku3'=> $data->perilaku3,
            'perilaku4'=> $data->perilaku4,
            'perilaku5'=> $data->perilaku5,
            'perilaku6'=> $data->perilaku6,
            'perilaku7'=> $data->perilaku7,
            'perilaku8'=> $data->perilaku8,
            'rtrw'=> $data->rtrw,
            'sirkulasiUdara'=> $data->sirkulasiUdara,
            'sosial1'=> $data->sosial1,
            'sosial2'=> $data->sosial2,
            'sosial3'=> $data->sosial3,
            'sosial41'=> $data->sosial41,
            'sosial42'=> $data->sosial42,
            'sosial43'=> $data->sosial43,
            'statusRumah'=> $data->statusRumah,
            'sumberAir'=> $data->sumberAir,
            'umur'=> $data->umur,            
            'Lokasi' => DB::raw("ST_GeomFromText('POINT(".$point[1]." ".$point[0].")')"),
        );
        if($foto1 == "" && $foto2 != "") {
            $editData = $editData + array('foto2' => $foto2);
        }else if($foto2 == "" && $foto1 != ""){
            $editData = $editData + array('foto1' => $foto1);
        }else if($foto2 == "" && $foto1 == ""){

        }else{
            $editData = $editData + array('foto1' => $foto1);
            $editData = $editData + array('foto2' => $foto2);
        }


        $responden = DB::table('penerima_bantuan')
            ->where('id', $id)
            ->update($editData);
        return $responden;

    }
}