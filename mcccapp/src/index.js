import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import './assets/css/tailwind.output.css'
import App from './App'
import { SidebarProvider } from './context/SidebarContext'
import ThemedSuspense from './components/ThemedSuspense'
import { Windmill } from '@windmill/react-ui'
// eslint-disable-next-line no-unused-vars
import * as serviceWorker from './serviceWorker'

import store from './store/store'
import { Provider as ReduxProvider } from 'react-redux'

// if (process.env.NODE_ENV !== 'production') {
//   const axe = require('react-axe')
//   axe(React, ReactDOM, 1000)
// }c

ReactDOM.render(
	<SidebarProvider>
		<Suspense fallback={<ThemedSuspense />}>
			<Windmill usePreferences>
				<ReduxProvider store={store}>
					<App />
				</ReduxProvider>
			</Windmill>
		</Suspense>
	</SidebarProvider>,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.register()
