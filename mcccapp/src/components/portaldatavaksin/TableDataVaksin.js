import React, { useState, useEffect } from 'react'
import {
	Table,
	TableHeader,
	TableCell,
	TableBody,
	TableRow,
	TableFooter,
	TableContainer,
	Button,
	Pagination,
} from '@windmill/react-ui'
import { getDataVaksin } from '../../data/repository'
import { EditIcon } from '../../icons'
import { Link } from 'react-router-dom'
import moment from 'moment'

function TableDataVaksin({
	kodewil = '',
	searchKey = '',
	dateStarted = '',
	dateEnded = '',
	onEditClicked,
	onDeleteClicked,
	refreshTable,
}) {
	const [pageTable2, setPageTable2] = useState(1)

	const [dataTable2, setDataTable2] = useState([])
	const [totalData, settotalData] = useState(0)

	const [resultsPerPage, setResultPerPage] = useState(10)

	function onPageChangeTable2(p) {
		setPageTable2(p)
	}

	const getDataVaksinCovid = (
		kodewil = '',
		date_start = '',
		date_end = '',
		page = ''
	) => {
		//console.log('KODEWIL ', kodewil)
		getDataVaksin(kodewil, date_start, date_end, page)
			.then((result) => {
				//console.log(result)
				settotalData(result.data.row_total)
				setResultPerPage(10)
				setDataTable2(result.data.row_data)
			})
			.catch((err) => console.log(err))

		//console.log(dataTable2)
	}

	useEffect(() => {
		getDataVaksinCovid(kodewil, dateStarted, dateEnded, pageTable2)
	}, [pageTable2, refreshTable, kodewil, dateStarted, dateEnded])

	return (
		<>
			{/* <p className='text-white'>
				Page ke = {pageTable2} per page = {resultsPerPage} total match data ={' '}
				{totalData}
			</p> */}
			<TableContainer className='mb-8'>
				<Table>
					<TableHeader>
						<tr>
							<TableCell>Wilayah</TableCell>
							<TableCell>Penyelanggara</TableCell>
							<TableCell>Tanggal</TableCell>
							<TableCell>Alamat</TableCell>
							<TableCell>Tahap</TableCell>
							<TableCell>Jenis Vaksin</TableCell>
							<TableCell>Total</TableCell>
							<TableCell>Editor</TableCell>
							<TableCell>Actions</TableCell>
						</tr>
					</TableHeader>
					<TableBody>
						{dataTable2.map((kasus, i) => (
							<TableRow key={i}>
								<TableCell>
									<span className='text-sm'>
										{kasus.provinsi}, {kasus.kabkot}, {kasus.kecamatan}
									</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.penyelenggara}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>
										{moment(kasus.penyelenggaraan_date).format('DD-MM-YYYY')}
									</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.alamat}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.tahap}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.jenis_vaksin}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.total}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.editor}</span>
								</TableCell>
								<TableCell>
									<div className='flex items-center space-x-4'>
										<button
											onClick={() => onEditClicked(kasus)}
											className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-1 px-2 rounded'
										>
											Edit
										</button>
										<button
											onClick={() => onDeleteClicked(kasus)}
											className='bg-red-700 hover:bg-red-900 text-white text-sm py-1 px-2 rounded'
										>
											Delete
										</button>
									</div>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<TableFooter>
					<Pagination
						totalResults={totalData}
						resultsPerPage={resultsPerPage}
						onChange={onPageChangeTable2}
						label='Table navigation'
					/>
				</TableFooter>
			</TableContainer>
		</>
	)
}

export default TableDataVaksin
