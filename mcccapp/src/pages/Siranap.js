import React, { Suspense, lazy, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

import {
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	Input,
	Label,
	ModalFooter,
} from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import {
	addJenisKamar,
	getWilayahKabupaten,
	getWilayahKasusProvinsi,
} from '../data/repository'
import 'flatpickr/dist/themes/dark.css'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	useRouteMatch,
} from 'react-router-dom'
const DaftarRS = lazy(() => import('../components/siranap/DaftarRS'))
const DaftarKamar = lazy(() => import('../components/siranap/DaftarKamar'))

function Siranap() {
	const [ismodalOpen, setIsmodalOpen] = useState(false)
	const [isEditEvent, setIsEditEvent] = useState(false)
	const [editId, setEditId] = useState(0)
	const [jenisKamar, setJenisKamar] = useState('')
	const [refreshtable, setRefreshtable] = useState(0)
	let { path, url } = useRouteMatch()
	return (
		<Suspense fallback={<ThemedSuspense />}>
			<Switch>
				<Route exact path={`${path}`}>
					<Suspense fallback={<ThemedSuspense />}>
						<DaftarRS />
					</Suspense>
				</Route>
				<Route exact path={`${path}/:defaultKodewil`}>
					<Suspense fallback={<ThemedSuspense />}>
						<DaftarRS />
					</Suspense>
				</Route>
				<Route path={`${path}/kamar/:rsid`}>
					<Suspense fallback={<ThemedSuspense />}>
						<DaftarKamar />
					</Suspense>
				</Route>
			</Switch>
		</Suspense>
	)
}

export default Siranap
