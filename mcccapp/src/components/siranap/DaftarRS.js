import React, { useState, useMemo, useEffect } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import faker from 'faker'
import Loader from 'react-loader-spinner'
import SelectSearch, { fuzzySearch } from 'react-select-search'
import './select.css'
import { Button, Input, Card, CardBody, Label } from '@windmill/react-ui'
import {
	getAllRS,
	getFrontSiranapKamar,
	getWilayahKabupaten,
	getWilayahKasusProvinsi,
} from '../../data/repository'
import { useRouteMatch, Link, useParams, useHistory } from 'react-router-dom'

function DaftarRS() {
	const [dataRs, setDataRS] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [filterText, setFilterText] = useState('')
	const [kodewil, setKodewil] = useState(null)
	const [provinsiData, setProvinsiData] = useState([])
	const [kabupatenData, setKabupatenData] = useState([])
	let { path, url } = useRouteMatch()
	const history = useHistory()
	let { defaultKodewil } = useParams()
	const getRSData = () => {
		setIsLoading(true)
		setTimeout(() => {
			getFrontSiranapKamar(kodewil)
				.then((res) => {
					console.log(res.data.data)
					setDataRS(res.data.data)
					setIsLoading(false)
				})
				.catch((err) => {
					setDataRS([])
					console.log(err)
					setIsLoading(false)
				})
		}, 1000)
	}
	const getProvinsiData = () => {
		setProvinsiData([])

		getWilayahKasusProvinsi()
			.then((result) => {
				console.log(result)
				let datas = []
				result.data.map((val) => {
					datas.push({ name: val.nama, value: val.idwil })
				})
				setProvinsiData(datas)
			})
			.catch((err) => {
				getProvinsiData()
			})
	}

	const getKabupatenData = (provid) => {
		setKodewil(null)
		setKabupatenData([])
		setKodewil(null)
		getWilayahKabupaten(provid)
			.then((result) => {
				console.log(result.data)
				let datas = []
				result.data.map((val) => {
					datas.push({ name: val.nama, value: val.kpu_idkab })
				})
				setKabupatenData(datas)
				//setKabupatendata(result.data)
			})
			.catch((err) => {
				getKabupatenData(provid)
				//console.log(err)
			})
	}

	useEffect(() => {
		getProvinsiData()
		console.log('URLNYA', url)
	}, [0])

	const searchRS = dataRs.filter((item) => {
		return (
			item.kamar && item.nama.toLowerCase().includes(filterText.toLowerCase())
		)
	})

	useEffect(() => {
		if (defaultKodewil !== '') {
			setKodewil(defaultKodewil)
		}

		getRSData()
	}, [0, kodewil, defaultKodewil])
	return (
		<div className='md:px-10 md:px-10 py-5'>
			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				tag={Link}
				to='/app/covidupdate'
			>
				<div className='float-left hidden md:block xl:block'>
					<span className='text-xl'>SIRANAP</span>
				</div>
				<div className='float-right'>
					<Label className='text-white'>
						<span className='mb-2 text-white'>Provinsi</span>
						<SelectSearch
							options={provinsiData}
							search
							onChange={(e) => {
								console.log('PROV HIT')
								getKabupatenData(e)
							}}
							filterOptions={fuzzySearch}
							placeholder='Pilih Provinsi'
						/>
					</Label>
					<Label className='text-white pt-2'>
						<span className='mb-2 text-white'>Kabupaten</span>
						<SelectSearch
							options={kabupatenData}
							search
							onChange={(e) => {
								console.log('KAB HIT', url)
								history.replace(`/app/siranap/${e}`)
								setKodewil(e)
							}}
							value={kodewil}
							filterOptions={fuzzySearch}
							placeholder='Pilih Kabupaten'
						/>
					</Label>
				</div>
			</a>
			<div>
				<center className={`${isLoading ? 'block' : 'hidden'}`}>
					<Loader type='Oval' color='#00BFFF' height={30} width={30} />
				</center>
			</div>
			<div>
				<center
					className={`${
						searchRS.length <= 0 || isLoading ? 'block' : 'hidden'
					} pt-5`}
				>
					<p className='dark:text-gray-500 text-gray-500 text-sm'>
						{isLoading
							? 'Memuat Data'
							: 'Tidak ada data, pilih Provinsi, kabupaten pada bar atas untuk mendapatkan data'}
					</p>
				</center>
			</div>
			<Label
				className={` ${searchRS.length <= 0 ? 'hidden' : ''} ${
					isLoading ? 'hidden' : 'block'
				} max-w-max p-5`}
			>
				<span>Cari Rumah Sakit</span>
				<Input
					placeholder='.....'
					onChange={(e) => setFilterText(e.target.value)}
					className='mt-1 border-solid border-2 rounded'
				/>
			</Label>
			<div className='grid grid-rows-2 xl:px-5'>
				<div className={`${isLoading ? 'hidden' : 'block'}`}>
					{searchRS.map((val) => (
						<Card className='shadow-2xl shadow-lg mb-5'>
							<CardBody>
								<div class='grid grid-cols-1 xl:grid-cols-2 p-2'>
									<div>
										<h2 className='dark:text-white text-lg'>{val.nama}</h2>
										<p className='dark:text-white text-sm  pt-2'>
											{val.provinsi}, {val.kabupaten}, {val.kecamatan},{' '}
											{val.desa}, {val.alamat}
										</p>
										<p className='dark:text-white text-sm  pt-2'>
											Terakhir Update :{' '}
											<span className='bg-purple-600 rounded'>
												<span className='p-2'>
													{val.kamar.length > 0 ? val.kamar[0].tanggal : null}
												</span>
											</span>
										</p>
									</div>
									<div className='float-right xl:text-right'>
										<ul className='xl:float-right text-sm dark:text-white pt-2'>
											{val.kamar.length > 0
												? val.kamar.map((val) => <li>{val.jenis}</li>)
												: null}
										</ul>
									</div>
								</div>
							</CardBody>
							<div className='p-5 float-right'>
								<Button tag={Link} to={`/app/siranap/kamar/${val.id}`}>
									Bed Detail
								</Button>
							</div>
						</Card>
					))}
				</div>
			</div>
		</div>
	)
}

export default DaftarRS
