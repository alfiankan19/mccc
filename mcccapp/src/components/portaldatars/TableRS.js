import React, { useState, useMemo, useEffect } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import faker from 'faker'
import { Button, Input } from '@windmill/react-ui'
import { getAllRS } from '../../data/repository'

const TextField = styled.input`
	height: 32px;
	width: 200px;
	border-radius: 3px;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	border: 1px solid #e5e5e5;
	padding: 0 32px 0 16px;

	&:hover {
		cursor: pointer;
	}
`

const ClearButton = styled(Button)`
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	height: 34px;
	width: 32px;
	text-align: center;
	display: flex;
	align-items: center;
	justify-content: center;
`
const FilterComponent = ({ filterText, onFilter, onClear }) => (
	<>
		<TextField
			id='search'
			type='text'
			placeholder='Cari RS'
			aria-label='Search Input'
			value={filterText}
			onChange={onFilter}
		/>
		<ClearButton type='button' onClick={onClear}>
			X
		</ClearButton>
	</>
)

const columns = [
	{
		name: 'Nama RS',
		selector: (row) => row.name,
		sortable: true,
	},
	{
		name: 'Alamat',
		selector: (row) => row.alamat,
		sortable: true,
	},
	{
		name: 'Opsi',
		selector: (row) => row.opsi,
		sortable: true,
	},
]

function TableRS({ onEditClicked, onDeleteClicked, refreshTable = 0 }) {
	const [filterText, setFilterText] = useState('')
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false)
	const [tableData, setTableData] = useState([])

	const getData = () => {
		getAllRS()
			.then((res) => {
				let raw = res.data.data
				console.log(raw)
				let datas = []
				raw.map((val) => {
					datas.push({
						id: val.id,
						name: val.nama,
						alamat: val.alamat,
						opsi: (
							<>
								<button
									onClick={() => {
										onEditClicked(val)
									}}
									className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-1 px-2 rounded'
								>
									Edit
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<button
									onClick={() => {
										onDeleteClicked(val)
									}}
									className='bg-red-700 hover:bg-red-900 text-white text-sm py-1 px-2 rounded'
								>
									Delete
								</button>
							</>
						),
					})
				})
				console.log(datas)
				setTableData(datas)
				console.log('tabel data')
				console.log(tableData)
			})
			.catch((err) => {
				console.log(err)
				setTableData([])
			})
	}

	const filteredItems = tableData.filter((item) => {
		return (
			item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
		)
	})

	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle)
				setFilterText('')
			}
		}

		return (
			<FilterComponent
				onFilter={(e) => setFilterText(e.target.value)}
				onClear={handleClear}
				filterText={filterText}
			/>
		)
	}, [filterText, resetPaginationToggle])

	useEffect(() => {
		getData()
	}, [0, refreshTable])

	return (
		<DataTable
			columns={columns}
			data={filteredItems}
			pagination
			theme='dark'
			paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
			subHeader
			subHeaderComponent={subHeaderComponentMemo}
			persistTableHead
		/>
	)
}

export default TableRS
