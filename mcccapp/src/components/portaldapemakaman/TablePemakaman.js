import React, { useState, useMemo, useEffect, useContext } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
	faEye,
	faPaperclip,
	faPhotoVideo,
} from '@fortawesome/free-solid-svg-icons'
import {
	Button,
	Input,
	Modal,
	ModalBody,
	ModalHeader,
	ModalFooter,
	WindmillContext,
} from '@windmill/react-ui'
import { getPemakamanData } from '../../data/repository'
import MiniMap from '../maps/MiniMap'

const TextField = styled.input`
	height: 32px;
	width: 200px;
	border-radius: 3px;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	border: 1px solid #e5e5e5;
	padding: 0 32px 0 16px;

	&:hover {
		cursor: pointer;
	}
`

const ClearButton = styled(Button)`
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	height: 34px;
	width: 32px;
	text-align: center;
	display: flex;
	align-items: center;
	justify-content: center;
`
const FilterComponent = ({ filterText, onFilter, onClear }) => (
	<>
		<TextField
			id='search'
			type='text'
			placeholder='Cari Nama Penerima'
			className='text-sm'
			aria-label='Search Input'
			value={filterText}
			onChange={onFilter}
		/>
		<ClearButton type='button' onClick={onClear}>
			X
		</ClearButton>
	</>
)

function TablePemakaman({ onEditClicked, onDeleteClicked, refreshTable = 0 }) {
	const { mode, toggleMode } = useContext(WindmillContext)
	const columns = [
		{
			name: 'Wilayah',
			selector: (row) =>
				`${row.prov ? row.prov : ''}, ${row.kab ? row.kab : ''}, ${
					row.kec ? row.kec : ''
				}, ${row.des ? row.des : ''}`,
			sortable: true,
			wrap: true,
		},
		{
			name: 'Nama Penerima',
			selector: (row) => row.nama,
			sortable: true,
			wrap: true,
		},
		{
			name: 'Alamat',
			selector: (row) => row.alamat,
			sortable: true,
			wrap: true,
		},
		{
			name: 'Detail Survey',
			selector: (row) => (
				<button
					onClick={() => {
						setDetailContent(row)
						setIsDetailmodalOpen(true)
					}}
					className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-2 px-2 rounded'
				>
					<FontAwesomeIcon color='white' size='lg' icon={faPaperclip} /> Lokasi
				</button>
			),
			sortable: true,
			wrap: true,
		},
		{
			name: 'Tanggal Data',
			selector: (row) => row.updated_at,
			sortable: true,
			wrap: true,
		},
		{
			name: 'Opsi',
			selector: (row) => (
				<>
					<button
						onClick={() => {
							onEditClicked(row)
						}}
						className='bg-purple-700 hover:bg-purple-800 text-white text-sm py-1 px-2 rounded'
					>
						Edit
					</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button
						onClick={() => {
							onDeleteClicked(row)
						}}
						className='bg-red-700 hover:bg-red-900 text-white text-sm py-1 px-2 rounded'
					>
						Delete
					</button>
				</>
			),
			sortable: true,
			wrap: true,
		},
	]
	const [filterText, setFilterText] = useState('')
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false)
	const [data, setData] = useState([])
	const [loading, setLoading] = useState(false)
	const [totalRows, setTotalRows] = useState(0)
	const [perPage, setPerPage] = useState(10)
	const [isDetailmodalOpen, setIsDetailmodalOpen] = useState(false)
	const [isFotomodalOpen, setIsFotomodalOpen] = useState(false)
	const [detailContent, setDetailContent] = useState()
	const [fotoContent, setFotoContent] = useState()

	const fetchData = async (page) => {
		setLoading(true)

		getPemakamanData(page, perPage, filterText)
			.then((result) => {
				console.log(result.data.data)
				setData(result.data.data.data)
				setTotalRows(result.data.data.total)
				setLoading(false)
			})
			.catch((err) => console.log(err))
	}

	const handlePageChange = (page) => {
		fetchData(page)
	}

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true)

		getPemakamanData(page, newPerPage, filterText)
			.then((result) => {
				console.log(result.data.data)
				setData(result.data.data.data)
				setPerPage(newPerPage)
				setLoading(false)
			})
			.catch((err) => console.log(err))
	}

	useEffect(() => {
		fetchData(1) // fetch page 1 of users
	}, [filterText, refreshTable])

	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle)
				setFilterText('')
			}
		}

		return (
			<FilterComponent
				onFilter={(e) => setFilterText(e.target.value)}
				onClear={handleClear}
				filterText={filterText}
			/>
		)
	}, [filterText, resetPaginationToggle])
	function filterNotEmpty(content) {
		return content !== ''
	}
	return (
		<>
			<div className='shadow-md rounded-md'>
				<DataTable
					columns={columns}
					data={data}
					progressPending={loading}
					pagination
					theme={mode}
					paginationServer
					paginationTotalRows={totalRows}
					onChangeRowsPerPage={handlePerRowsChange}
					onChangePage={handlePageChange}
					subHeader
					subHeaderComponent={subHeaderComponentMemo}
				/>
			</div>
			<Modal
				isOpen={isDetailmodalOpen}
				onClose={() => setIsDetailmodalOpen(false)}
			>
				<ModalBody className='overflow-y-auto max-h-xl py-5 px-3 scrollbar-hide'>
					<ModalHeader>Detail Lokasi</ModalHeader>
					{detailContent ? (
						<>
							<MiniMap
								lokasiVal={[detailContent.lat, detailContent.lng]}
								jenisLokasi='MAKAM/Meninggal'
							/>
						</>
					) : (
						<center>No data</center>
					)}
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>
		</>
	)
}

export default TablePemakaman
