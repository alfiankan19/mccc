import { lazy } from 'react'

// use lazy for better code splitting, a.k.a. load faster
const PortalDataVaksin = lazy(() => import('../pages/PortalDataVaksin'))

const Dashboard = lazy(() => import('../pages/Dashboard'))
const Forms = lazy(() => import('../pages/Forms'))
const Cards = lazy(() => import('../pages/Cards'))
const Charts = lazy(() => import('../pages/Charts'))
const Buttons = lazy(() => import('../pages/Buttons'))
const Modals = lazy(() => import('../pages/Modals'))
const Tables = lazy(() => import('../pages/Tables'))
const Page404 = lazy(() => import('../pages/404'))
const Blank = lazy(() => import('../pages/Blank'))
const Siranap = lazy(() => import('../pages/Siranap'))
const PortalDataRS = lazy(() => import('../pages/PortalDataRS'))
const PortalDataCovid = lazy(() => import('../pages/PortalDataCovid'))
const PortalDataCovidUploader = lazy(() =>
	import('../pages/PortalDataCovidUploader')
)
const PortalDataJenisRanap = lazy(() => import('../pages/PortalDataJenisRanap'))
const PortalDataSiranap = lazy(() => import('../pages/PortalDataSiranap'))
const PortalDataPemakaman = lazy(() => import('../pages/PortalDataPemakaman'))

/**PortalDataSiranap
 * ⚠ These are internal routes!
 * They will be rendered inside the app, using the default `containers/Layout`.
 * If you want to add a route to, let's say, a landing page, you should add
 * it to the `App`'s router, exactly like `Login`, `CreateAccount` and other pages
 * are routed.
 *
 * If you're looking for the links rendered in the SidebarContent, go to
 * `routes/sidebar.js`
 */
const routes = [
	{
		path: '/dashboard', // the url
		component: Dashboard, // view rendered
		auth: false, //is need auth
	},
	{
		path: '/covid19',
		component: PortalDataCovid,
		auth: true,
	},
	{
		path: '/covidupdate',
		component: PortalDataCovidUploader,
		auth: true,
	},
	{
		path: '/vaksinasi',
		component: PortalDataVaksin,
		auth: true,
	},
	{
		path: '/forms',
		component: Forms,
		auth: true,
	},
	{
		path: '/cards',
		component: Cards,
		auth: false,
	},
	{
		path: '/charts',
		component: Charts,
		auth: false,
	},
	{
		path: '/buttons',
		component: Buttons,
		auth: true,
	},
	{
		path: '/modals',
		component: Modals,
		auth: false,
	},
	{
		path: '/tables',
		component: Tables,
		auth: false,
	},
	{
		path: '/404',
		component: Page404,
		auth: false,
	},
	{
		path: '/blank',
		component: Blank,
		auth: false,
	},
	{
		path: '/rumahsakit',
		component: PortalDataRS,
		auth: true,
	},
	{
		path: '/siranapdata',
		component: PortalDataSiranap,
		auth: true,
	},
	{
		path: '/jenisranap',
		component: PortalDataJenisRanap,
		auth: true,
	},
	{
		path: '/siranap',
		component: Siranap,
		auth: false,
	},
	{
		path: '/pemakaman',
		component: PortalDataPemakaman,
		auth: false,
	},
]

export default routes
