import { createSlice } from '@reduxjs/toolkit'

export const slice = createSlice({
	name: 'map',
	initialState: {
		valueLoadingBatasDesa: false,
		valueMainMap: null,
	},
	reducers: {
		showLoadingBatasDesa: (state) => {
			state.valueLoadingBatasDesa = true
		},
		hideLoadingBatasDesa: (state) => {
			state.valueLoadingBatasDesa = false
		},
		setMainMap: (state, action) => {
			state.valueMainMap = action.payload
		},
	},
})

export const { showLoadingBatasDesa, hideLoadingBatasDesa, setMainMap } =
	slice.actions

export const loadingBatasDesa = (state) => state.map.valueLoadingBatasDesa
export const mainMap = (state) => state.map.valueMainMap

export default slice.reducer
