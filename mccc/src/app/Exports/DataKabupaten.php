<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models\WilayahKasusModel;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Models\WilayahModel;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DataKabupaten implements FromArray, ShouldAutoSize, WithStyles
{

    function __construct($provid)
    {
        $this->provid = $provid;
    }

    public function array(): array
    {
        $kabupatens = WilayahModel::Kabupaten($this->provid);

        $result[] = array(
            "Kode",
            "Kabupaten",
            "Terkonfirmasi",
            "Sembuh",
            "Meninggal"
        );
        foreach ($kabupatens as $key => $value){
            // get kabupaten total
            $total = WilayahKasusModel::getTotalDataKabupaten($kabupatens[$key]->kpu_idkab);
            $result[] = array(
                $value->kpu_idkab,
                $value->nama,
                $total["terkonfirmasi"]->total,
                $total["sembuh"]->total,
                $total["meninggal"]->total,
            );
        }
        
        
        return $result;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet
        ->getStyle('A1:D1')
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('afff94');
        $sheet->getStyle('A1:D1')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

        ];
    }
}
