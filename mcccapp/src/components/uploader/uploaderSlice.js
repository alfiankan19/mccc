import { createSlice } from '@reduxjs/toolkit'
import moment from 'moment'

export const slice = createSlice({
	name: 'uploader',
	initialState: {
		valueUploadDate: moment().format('D-MM-Y'),
	},
	reducers: {
		setDate: (state, action) => {
			state.valueUploadDate = action.payload
		},
	},
})

export const { setDate } = slice.actions

export const uploadDate = (state) => state.uploader.valueUploadDate

export default slice.reducer
