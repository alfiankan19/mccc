import React, { useState, useMemo, useEffect } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import faker from 'faker'
import { Button, Input, Card, CardBody } from '@windmill/react-ui'
import { getInfoRS, getKamarByRS } from '../../data/repository'
import Loader from 'react-loader-spinner'
import { Link, useParams, useRouteMatch } from 'react-router-dom'
import InfoCard from '../Cards/InfoCard'

function DaftarKamar() {
	const [isLoading, setIsLoading] = useState(false)
	const [dataKamar, setDataKamar] = useState([])
	const [rsInfoData, setRsInfoData] = useState([])
	let { rsid } = useParams()
	const getDataKamar = () => {
		setIsLoading(true)
		getKamarByRS(rsid)
			.then((res) => {
				setDataKamar(res.data.data)
				setIsLoading(false)
			})
			.catch((err) => {
				console.log(err)
				setDataKamar([])
				setIsLoading(false)
			})
	}
	const getRsInfo = () => {
		getInfoRS(rsid)
			.then((res) => {
				console.table(res.data.data)
				setRsInfoData(res.data.data)
			})
			.catch((err) => {
				console.log(err)
			})
	}
	useEffect(() => {
		getRsInfo()
		getDataKamar()
	}, [0])
	return (
		<div className='xl:px-10 xl:px-10 pt-10'>
			<div className='dark:text-white p-5'>
				<h1 className='text-2xl'>{rsInfoData.nama}</h1>
				<p className='text-sm pt-2'>{rsInfoData.alamat}</p>
				<div className='py-2'>
					<Button
						onClick={() => {
							window.location.href = `http://www.google.com/maps/place/${rsInfoData.Lat},${rsInfoData.Lng}`
						}}
					>
						Navigasi Peta
					</Button>
				</div>
			</div>
			<div>
				<center className={`${isLoading ? 'block' : 'hidden'} pt-10`}>
					<Loader type='Oval' color='#00BFFF' height={30} width={30} />
				</center>
			</div>
			<div className='grid grid-rows-2 xl:px-5'>
				<div>
					{dataKamar.map((val) => (
						<Card className='shadow-2xl shadow-lg mb-5'>
							<CardBody>
								<div class=''>
									<div>
										<h2 className='dark:text-white text-lg'>{val.jenis}</h2>
										<p className='dark:text-white text-sm  pt-2'>
											Terakhir Update :{' '}
											<span className='rounded'>
												<span className='p-2'>{val.date_time}</span>
											</span>
										</p>
									</div>
									<div>
										<div className='grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-3 pt-3'>
											<Card colored className='bg-blue-400'>
												<CardBody className='flex items-center'>
													<div>
														<p className='mb-2 text-sm font-medium text-gray-800'>
															Total kamar
														</p>
														<p className='text-lg font-semibold text-gray-800'>
															{val.total}
														</p>
													</div>
												</CardBody>
											</Card>
											<Card colored className='bg-green-400'>
												<CardBody className='flex items-center'>
													<div>
														<p className='mb-2 text-sm font-medium text-gray-800'>
															Kamar Terpakai
														</p>
														<p className='text-lg font-semibold text-gray-800'>
															{val.terpakai}
														</p>
													</div>
												</CardBody>
											</Card>
											<Card colored className='bg-yellow-400'>
												<CardBody className='flex items-center'>
													<div>
														<p className='mb-2 text-sm font-medium text-gray-800'>
															Total Antrian
														</p>
														<p className='text-lg font-semibold text-gray-800'>
															{val.antrian}
														</p>
													</div>
												</CardBody>
											</Card>
										</div>
									</div>
								</div>
							</CardBody>
						</Card>
					))}
				</div>
			</div>
		</div>
	)
}

export default DaftarKamar
