import React, { useEffect, useState } from 'react'
import {
	getKabupatenLayerCluster,
	getWilayahKasusProvinsi,
} from '../../data/repository'
import L from 'leaflet'
import { Marker, Popup, Tooltip } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'

import './cluster.css'
function CovidMapCluster() {
	const [dataKab, setDataKab] = useState([])
	const [dataProv, setDataProv] = useState([])

	const sumAll = (total, num) => {
		return total - num
	}

	const getColor = (total) => {
		let colorize = 'bg-green-400'
		if (total >= 1 && total <= 1000) {
			colorize = 'bg-green-400'
		} else if (total >= 1001 && total <= 5000) {
			colorize = 'bg-yellow-200'
		} else if (total >= 5001 && total <= 15000) {
			colorize = 'bg-orange-500'
		} else if (total > 15000) {
			colorize = 'bg-red-700'
		}
		return colorize
	}

	const kNumber = (number) => {
		if (number / 1000 < 10) {
			return number
		} else {
			let cv = (number / 1000).toFixed(0)
			return cv + 'K'
		}
	}

	const createClusterCustomIcon = function (cluster, nama) {
		let clss = cluster.getAllChildMarkers()
		let total = 0
		for (let i = 0; i < clss.length; i++) {
			total += Number(clss[i].options.data)
		}

		let info = `${nama}`

		return L.divIcon({
			html: `<div class="${getColor(
				total
			)} leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive tooltip"><span class="tooltip text-xs">${kNumber(
				total
			)}<span class="tooltiptext">${info}</span></span></div>`,
			className: 'marker-cluster',
			iconSize: L.point(40, 40, true),
		})
	}

	const getDataKab = () => {
		//console.log('GETTING CLUSTER')
		getKabupatenLayerCluster()
			.then((result) => {
				console.log('SUCCESS CLUSTER')
				console.log(result)
				setDataKab(result.data)
			})
			.catch((err) => console.log(err))
	}

	const getDataProv = () => {
		getWilayahKasusProvinsi()
			.then((result) => {
				console.log(result.data)
				setDataProv(result.data)
				getDataKab()
			})
			.catch((err) => {
				//console.log(err)
			})
	}

	useEffect(() => {
		getDataProv()
	}, [0])

	return (
		<>
			{dataProv.map((vp) => (
				<MarkerClusterGroup
					iconCreateFunction={(cls) => createClusterCustomIcon(cls, vp.nama)}
				>
					{dataKab.map((v) =>
						v.kpu_idprov === Number(vp.idwil) ? (
							<Marker
								icon={L.divIcon({
									html: `<div class="${getColor(
										v.aktif
									)} leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive"><span>${
										v.aktif
									}</span></div>`,
									className: 'marker-cluster bg-black',
									iconSize: L.point(40, 40, true),
								})}
								data={v.aktif}
								position={[v.lat, v.lng]}
							>
								<Tooltip>
									<table className='table-auto'>
										<tbody>
											<tr>
												<td>Kab/Kota</td>
												<td> : {v.nama}</td>
											</tr>
											<tr>
												<td>Terkonfirmasi</td>
												<td> : {v.terkonfirmasi}</td>
											</tr>
											<tr>
												<td>Aktif</td>
												<td> : {v.aktif}</td>
											</tr>
											<tr>
												<td>Sembuh</td>
												<td> : {v.sembuh}</td>
											</tr>
											<tr>
												<td>Meninggal</td>
												<td> : {v.meninggal}</td>
											</tr>
										</tbody>
									</table>
								</Tooltip>
								{/* <Popup>
									<table className='table-auto'>
										<tbody>
											<tr>
												<td>Kab/Kota</td>
												<td> : {v.nama}</td>
											</tr>
											<tr>
												<td>Terkonfirmasi</td>
												<td> : {v.terkonfirmasi}</td>
											</tr>
											<tr>
												<td>Aktif</td>
												<td> : {v.aktif}</td>
											</tr>
											<tr>
												<td>Sembuh</td>
												<td> : {v.sembuh}</td>
											</tr>
											<tr>
												<td>Meninggal</td>
												<td> : {v.meninggal}</td>
											</tr>
										</tbody>
									</table>
								</Popup> */}
							</Marker>
						) : null
					)}
				</MarkerClusterGroup>
			))}
		</>
	)
}

export default CovidMapCluster
