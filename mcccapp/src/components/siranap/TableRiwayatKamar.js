import React, { useState, useMemo, useEffect } from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import faker from 'faker'
import Swal from 'sweetalert2'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'

import {
	Button,
	Input,
	Modal,
	Select,
	ModalBody,
	ModalFooter,
	ModalHeader,
	Label,
	Textarea,
} from '@windmill/react-ui'
import {
	addKamarRS,
	getAllJenisKamar,
	getAllRS,
	getInfoKamarRS,
	getInfoRS,
	getJenisKamarByRS,
	getKamarByRS,
	getRiwayatKamarRS,
	updateKamarRS,
} from '../../data/repository'
import { useParams } from 'react-router-dom'
import moment from 'moment'

const TextField = styled.input`
	height: 32px;
	width: 200px;
	border-radius: 3px;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	border: 1px solid #e5e5e5;
	padding: 0 32px 0 16px;

	&:hover {
		cursor: pointer;
	}
`

const ClearButton = styled(Button)`
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	height: 34px;
	width: 32px;
	text-align: center;
	display: flex;
	align-items: center;
	justify-content: center;
`
const FilterComponent = ({ filterText, onFilter, onClear }) => (
	<>
		<TextField
			id='search'
			type='text'
			placeholder='Cari Kamar'
			aria-label='Search Input'
			value={filterText}
			onChange={onFilter}
		/>
		<ClearButton type='button' onClick={onClear}>
			X
		</ClearButton>
	</>
)

const columns = [
	{
		name: 'Kamar',
		selector: (row) => row.kamar,
		wrap: true,
		sortable: true,
	},
	{
		name: 'Total',
		selector: (row) => row.total,
		sortable: true,
	},
	{
		name: 'Terpakai',
		selector: (row) => row.terpakai,
		sortable: true,
	},
	{
		name: 'Antrian',
		selector: (row) => row.antrian,
		sortable: true,
	},
	{
		name: 'Tanggal Data',
		selector: (row) => row.tanggal,
		sortable: true,
	},
	{
		name: 'Update BY',
		selector: (row) => row.updateby,
		sortable: true,
	},
]
const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}
function TableRiwayatKamar({ refreshTable = 0 }) {
	const [filterText, setFilterText] = useState('')
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false)
	const [tableData, setTableData] = useState([])
	const [kamarRsInfo, setKamarRSInfo] = useState([{ jenis: '', nama: '' }])
	let { rsid, jnskmr } = useParams()
	const getData = () => {
		getRiwayatKamarRS(rsid, jnskmr)
			.then((res) => {
				let raw = res.data.data
				console.log(raw)
				let datas = []
				raw.map((val) => {
					datas.push({
						kamar: val.jenis,
						total: val.total,
						terpakai: val.terpakai,
						antrian: val.antrian,
						tanggal: moment(val.date_time).format('DD-MM-YYYY HH:mm:ss'),
						updateby: val.updater_username,
					})
				})
				console.log(datas)
				setTableData(datas)
				console.log('tabel data')
				console.log(tableData)
			})
			.catch((err) => {
				console.log(err)
				setTableData([])
			})
	}

	const filteredItems = tableData.filter((item) => {
		return (
			item.kamar && item.kamar.toLowerCase().includes(filterText.toLowerCase())
		)
	})

	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle)
				setFilterText('')
			}
		}

		return (
			<FilterComponent
				onFilter={(e) => setFilterText(e.target.value)}
				onClear={handleClear}
				filterText={filterText}
			/>
		)
	}, [filterText, resetPaginationToggle])

	const getKamarRsInfo = () => {
		getInfoKamarRS(rsid, jnskmr)
			.then((res) => {
				console.table(res.data.data)
				setKamarRSInfo(res.data.data)
			})
			.catch((err) => {
				console.log(err)
			})
	}

	useEffect(() => {
		getKamarRsInfo()
		getData()
	}, [0, refreshTable])

	return (
		<>
			<a className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'>
				<div className='flex items-center'>
					<span>
						DATA KAMAR {kamarRsInfo[0].jenis} {kamarRsInfo[0].nama}
					</span>
				</div>
			</a>
			<DataTable
				columns={columns}
				data={filteredItems}
				pagination
				theme='dark'
				paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
				subHeader
				title='Data Terbaru / Terakhir'
				persistTableHead
			/>
		</>
	)
}

export default TableRiwayatKamar
