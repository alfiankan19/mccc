import React, { useState, useEffect } from 'react'
import TableKabupaten from '../../../components/portaldatacovid/TableKabupaten'
import SectionTitle from '../../../components/Typography/SectionTitle'
import { Select, Input, Label } from '@windmill/react-ui'
import { getWilayahKasusProvinsi } from '../../../data/repository'

function TabelKasusKabupaten() {
	const [provinsidata, setProvinsidata] = useState([])
	const [whereProv, setWhereProv] = useState(11)
	const [searchKey, setSearchKey] = useState('')
	const getProvinsiData = () => {
		setProvinsidata([])

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
			})
			.catch((err) => {
				getProvinsiData()
				////console.log(err)
			})
	}

	useEffect(() => {
		getProvinsiData()
	}, [0])
	return (
		<>
			<SectionTitle>Data Terakhir</SectionTitle>
			<div class='grid grid-cols-2'>
				<div>
					<div className='py-3'>
						<Label className=' max-w-max'>
							<span>Pilih Provinsi</span>
							<Select
								onChange={(e) => {
									setWhereProv(e.target.value)
								}}
								className='mt-1 border-solid border-2 rounded'
							>
								{provinsidata.map((v) => (
									<option key={v.idwil} value={v.idwil}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					</div>
				</div>
				<div>
					<div className='py-3 float-right'>
						<Label className=' max-w-max'>
							<span>Cari Kabupaten</span>
							<Input
								placeholder='ketik lalu enter'
								onKeyDown={(e) =>
									e.key === 'Enter' ? setSearchKey(e.target.value) : null
								}
								className='mt-1 border-solid border-2 rounded'
							/>
						</Label>
					</div>
				</div>
			</div>
			<TableKabupaten whereProv={whereProv} searchKey={searchKey} />
		</>
	)
}

export default TabelKasusKabupaten
