import React, { lazy, Suspense, useEffect, useState } from 'react'

import InfoCard from '../components/Cards/InfoCard'
import ThemedSuspense from '../components/ThemedSuspense'

import PageTitle from '../components/Typography/PageTitle'
import { PeopleIcon } from '../icons'
import RoundIcon from '../components/RoundIcon'
import { getSummary } from '../data/repository'
import Loader from 'react-loader-spinner'
import CovidMapCluster from '../components/maps/CovidMapCluster'
import VaksinMapCluster from '../components/maps/VaksinMapCluster'
import RSMapCluster from '../components/maps/RSMapCluster'
import MakamMapCluster from '../components/maps/MakamMapCluster'

const MainMap = lazy(() => import('../components/maps/MainMap'))
const KasusChart = lazy(() => import('../components/Chart/KasusChart'))
const VaksinChart = lazy(() => import('../components/Chart/VaksinChart'))
const SembuhChart = lazy(() => import('../components/Chart/SembuhChart'))
const MeninggalChart = lazy(() => import('../components/Chart/MeninggalChart'))
const StackedChart = lazy(() => import('../components/Chart/StackedChart'))
function Dashboard(props) {
	const [summaryCard, setSummaryCard] = useState({
		terkonfirmasi: 0,
		aktif: 0,
		sembuh: 0,
		meninggal: 0,
	})
	const [isLoading, setIsLoading] = useState(false)
	const getSummaryDasboard = () => {
		setIsLoading(true)
		getSummary()
			.then((result) => {
				setSummaryCard(result.data)
				////console.log('aktif = ' + result.data.aktif)
				setIsLoading(false)
			})
			.catch((err) => {
				////console.log(err)
				setIsLoading(false)
			})
	}

	useEffect(() => {
		getSummaryDasboard()
	}, [0])

	return (
		<div className='px-3'>
			<PageTitle>Peta Sebaran</PageTitle>

			<div className='grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4'>
				<InfoCard
					shadow={true}
					title='Terkonfirmasi'
					value={isLoading ? 'Calculating...' : summaryCard.terkonfirmasi}
				>
					<RoundIcon
						icon={PeopleIcon}
						iconColorClass='text-orange-500 dark:text-orange-100'
						bgColorClass='bg-orange-100 dark:bg-orange-500'
						className='mr-4'
					/>
				</InfoCard>

				<InfoCard
					shadow={true}
					title='Aktif/Positif'
					value={isLoading ? 'Calculating...' : summaryCard.aktif}
				>
					<RoundIcon
						icon={PeopleIcon}
						iconColorClass='text-green-500 dark:text-green-100'
						bgColorClass='bg-green-100 dark:bg-green-500'
						className='mr-4'
					/>
				</InfoCard>

				<InfoCard
					shadow={true}
					title='Sembuh'
					value={isLoading ? 'Calculating...' : summaryCard.sembuh}
				>
					<RoundIcon
						icon={PeopleIcon}
						iconColorClass='text-blue-500 dark:text-blue-100'
						bgColorClass='bg-blue-100 dark:bg-blue-500'
						className='mr-4'
					/>
				</InfoCard>

				<InfoCard
					shadow={true}
					title='Meninggal'
					value={isLoading ? 'Calculating...' : summaryCard.meninggal}
				>
					<RoundIcon
						icon={PeopleIcon}
						iconColorClass='text-teal-500 dark:text-teal-100'
						bgColorClass='bg-teal-100 dark:bg-teal-500'
						className='mr-4'
					/>
				</InfoCard>
			</div>

			<Suspense fallback={<ThemedSuspense />}>
				<MainMap
					otherLayer={[
						{ name: 'Covid Layer', show: false, mapLayer: <CovidMapCluster /> },
						{
							name: 'Vaksinasi',
							show: true,
							mapLayer: <VaksinMapCluster />,
						},
						{ name: 'Rumah Sakit', show: true, mapLayer: <RSMapCluster /> },
						{
							name: 'Meninggal/Makam',
							show: true,
							mapLayer: <MakamMapCluster />,
						},
					]}
				/>
			</Suspense>

			<Suspense fallback={<ThemedSuspense />}>
				<KasusChart idchart='chart1' />
			</Suspense>
			<Suspense fallback={<ThemedSuspense />}>
				<SembuhChart idchart='chart2' />
			</Suspense>
			<Suspense fallback={<ThemedSuspense />}>
				<MeninggalChart idchart='chart3' />
			</Suspense>
			<Suspense fallback={<ThemedSuspense />}>
				<StackedChart idchart='chart4' />
			</Suspense>

			<Suspense fallback={<ThemedSuspense />}>
				<VaksinChart idchart='chart5' />
			</Suspense>
		</div>
	)
}

export default Dashboard
