<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Respondent
{
    

    public static function getAllRespondenByUserId($user_id,$page=0,$search="")
    {
        if ($search == ""){
            $page = $page * 10;
            return DB::table('responden')
            ->select('id','created_by','nama', DB::raw('ST_X(Lokasi) AS lat'), DB::raw('ST_Y(Lokasi) AS lng'), 'kode_wilayah','created_at','updated_at')
            ->where('created_by',$user_id)
            ->offset($page)
            ->limit(10)
            ->get();
        } else {
            $page = $page * 10;
            return DB::table('responden')
            ->select('id','created_by','nama', DB::raw('ST_X(Lokasi) AS lat'), DB::raw('ST_Y(Lokasi) AS lng'), 'kode_wilayah','created_at','updated_at')
            ->where('created_by',$user_id)
            ->where('nama', 'like', '%'.$search.'%')
            ->offset($page)
            ->limit(10)
            ->get();
        }
    }

  



}
