import { base_url } from './service'
import axios from 'axios'

// refresh map get layer batasdesa
const getBatasDesaLayer = async (map, maptype) => {
	let cropparam = '' //3x3372
	let bounds = map.getBounds()
	let zoom = map.getZoom()
	let config = {
		method: 'GET',
		url:
			base_url +
			`geojson/layer?dimension=${bounds._southWest.lng}x${bounds._southWest.lat}x${bounds._northEast.lng}x${bounds._northEast.lat}&zoom=${zoom}&type${maptype}&crop=${cropparam}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getLayerProvinsi = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'geojson/provinsi',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getWilayahKasusProvinsi = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'wilayahkasus/provinsi',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget lkabupaten list
const getWilayahKabupaten = async (provid) => {
	let config = {
		method: 'GET',
		url: base_url + `wilayah/kabupaten/${provid}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget kecamatan list
const getWilayahKecamatan = async (kabid) => {
	let config = {
		method: 'GET',
		url: base_url + `wilayah/kecamatan/${kabid}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget desa list
const getWilayahDesa = async (kecid) => {
	let config = {
		method: 'GET',
		url: base_url + `wilayah/desa/${kecid}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getWilayahKasusKabupaten = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'geojson/provinsi',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget summary
const getSummary = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'covid/summary/json',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getKabupatenLayerCluster = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'covid/kabupaten/json',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getLastUpdated = async () => {
	let config = {
		method: 'GET',
		url: base_url + 'log/lastupdate',
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// login
const login = async (email, password) => {
	let config = {
		method: 'POST',
		url: base_url + 'user/login',
		data: {
			email: email,
			password: password,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget chart json provinsi terkonfirmasi
const getChartJsonTerkonfirmasi = async (provid) => {
	let config

	if (provid === '') {
		config = {
			method: 'GET',
			url: base_url + 'covid/terkonfirmasi/provinsi/series/json',
		}
	} else {
		config = {
			method: 'GET',
			url: base_url + `covid/terkonfirmasi/series/${provid}/json`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget chart json provinsi terkonfirmasi
const getChartJsonSembuh = async (provid) => {
	let config

	if (provid === '') {
		config = {
			method: 'GET',
			url: base_url + 'covid/sembuh/provinsi/series/json',
		}
	} else {
		config = {
			method: 'GET',
			url: base_url + `covid/sembuh/series/${provid}/json`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget chart json provinsi terkonfirmasi
const getChartJsonMeninggal = async (provid) => {
	let config

	if (provid === '') {
		config = {
			method: 'GET',
			url: base_url + 'covid/meninggal/provinsi/series/json',
		}
	} else {
		config = {
			method: 'GET',
			url: base_url + `covid/meninggal/series/${provid}/json`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget chart json provinsi terkonfirmasi
const getChartJsonStacked = async (provid) => {
	let config

	if (provid === '') {
		config = {
			method: 'GET',
			url: base_url + 'covid/stacked/provinsi/series/json',
		}
	} else {
		config = {
			method: 'GET',
			url: base_url + `covid/stacked/series/${provid}/json`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getDataCovidKabupaten = async (provid, search = '') => {
	let config
	if (search === '') {
		config = {
			method: 'GET',
			url: base_url + `covid/kabupaten/${provid}/json`,
		}
	} else {
		config = {
			method: 'GET',
			url: base_url + `covid/kabupaten/${provid}/${search}/json`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const addDataSeriesCovid = async (data, date) => {
	let config = {
		method: 'POST',
		url: base_url + 'series/covid',
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			date: date,
			data: JSON.stringify(data),
		},
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getDataSeriesKabupaten = async (kabid, date) => {
	let dateRange = date.split(' to ')
	let config = {
		method: 'GET',
		url:
			base_url +
			`covid/series/kabupaten/${kabid}/${dateRange[0]}/${dateRange[1]}/json`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const updateSingleCovidData = async (target, total, kabid, date) => {
	let config = {
		method: 'POST',
		url: base_url + `series/covid/updatesingle`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			target: target,
			kabid: kabid,
			date: date,
			total: total,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const getDataVaksin = async (kodewil, date_start, date_end, page) => {
	let config

	if (kodewil === '') {
		config = {
			method: 'GET',
			url:
				base_url +
				`data/vaksin/json?page=${page}&date_start=${date_start}&date_end=${date_end}`,
		}
	} else {
		config = {
			method: 'GET',
			url:
				base_url +
				`data/vaksin/${kodewil}/json?page=${page}&date_start=${date_start}&date_end=${date_end}`,
		}
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const addVaksinRecord = async (
	kodewil,
	penyelenggara,
	penyelenggaraan_date,
	alamat,
	total,
	tahap,
	jenis_vaksin,
	lat,
	lng
) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/vaksin`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			kodewil: kodewil,
			penyelenggara: penyelenggara,
			penyelenggaraan_date: penyelenggaraan_date,
			alamat: alamat,
			total: total,
			tahap: tahap,
			jenis_vaksin: jenis_vaksin,
			lng: lng,
			lat: lat,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const editVaksinRecord = async (
	id,
	kodewil,
	penyelenggara,
	penyelenggaraan_date,
	alamat,
	total,
	tahap,
	jenis_vaksin,
	lat,
	lng
) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/vaksin/edit`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
			kodewil: kodewil,
			penyelenggara: penyelenggara,
			penyelenggaraan_date: penyelenggaraan_date,
			alamat: alamat,
			total: total,
			tahap: tahap,
			jenis_vaksin: jenis_vaksin,
			lng: lng,
			lat: lat,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const deleteVaksinRecord = async (id) => {
	let config = {
		method: 'POST',
		url: base_url + `data/vaksin/delete`,
		data: {
			id: id,
		},
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getGeojsonVaksinasi = async (id) => {
	let config = {
		method: 'GET',
		url: base_url + `geojson/vaksinasi`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getGeojsonRS = async (id) => {
	let config = {
		method: 'GET',
		url: base_url + `geojson/rs`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getGeojsonPemakaman = async (id) => {
	let config = {
		method: 'GET',
		url: base_url + `geojson/pemakaman`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getVaksinChartPerkembangan = async () => {
	let config = {
		method: 'GET',
		url: base_url + `data/vaksin/series`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const addRS = async (nama, alamat, lat, lng, kodewil) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/rs/add`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			nama: nama,
			alamat: alamat,
			lng: lng,
			lat: lat,
			kodewil: kodewil,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getAllRS = async () => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/rs`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getAllJenisKamar = async () => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/jeniskamar`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getJenisKamarByRS = async (rsid) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/jeniskamar/${rsid}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const editRS = async (id, nama, alamat, lat, lng, kodewil) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/rs/edit`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
			nama: nama,
			alamat: alamat,
			kode_wilayah: kodewil,
			lng: lng,
			lat: lat,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const addJenisKamar = async (jenis) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/jeniskamar/add`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			jenis: jenis,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const editJenisKamar = async (id, jenis) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/jeniskamar/edit`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
			jenis: jenis,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const deleteRS = async (id) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/rs/delete`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getInfoRS = async (id) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/rs/info/${id}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getKamarByRS = async (id) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/kamar/${id}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const addKamarRS = async (rs_id, jenis_kamar) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/rs/addkamar`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			rs_id: rs_id,
			jenis_kamar: jenis_kamar,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

// rget layer provinsi download
const updateKamarRS = async (
	rs_id,
	jenis_kamar,
	total,
	terpakai,
	antrian,
	tanggal
) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/siranap/rs/updatekamar`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			rs_id: rs_id,
			jenis_kamar: jenis_kamar,
			total: total,
			terpakai: terpakai,
			antrian: antrian,
			tanggal: tanggal,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getInfoKamarRS = async (rsid, jnskmr) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/kamar/info/${rsid}/${jnskmr}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getRiwayatKamarRS = async (rsid, jnskmr) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/kamar/riwayat/${rsid}/${jnskmr}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const getFrontSiranapKamar = async (kodewil) => {
	let config = {
		method: 'GET',
		url: base_url + `data/siranap/rsfront/${kodewil}`,
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const addPemakamanData = async (nama, note, alamat, kodewil, lat, lng) => {
	let config = {
		method: 'POST',
		url: base_url + 'data/pemakaman',
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			nama: nama,
			note: note,
			alamat: alamat,
			kodewil: kodewil,
			lat: lat,
			lng: lng,
		},
	}

	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}
const editPemakaman = async (id, nama, note, alamat, kodewil, lat, lng) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/pemakaman/edit`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
			nama: nama,
			note: note,
			alamat: alamat,
			kodewil: kodewil,
			lat: lat,
			lng: lng,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}
const getPemakamanData = async (page, per_page, search) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'GET',
		url:
			base_url +
			`data/pemakaman/table?page=${page}&per_page=${per_page}&search=${search}`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

const deletePemakaman = async (id) => {
	////console.log('KIRIM LOKASI ', lat, lng)
	let config = {
		method: 'POST',
		url: base_url + `data/pemakaman/delete`,
		headers: {
			Authorization: `Bearer ${localStorage.getItem('JWT')}`,
			'Content-Type': 'application/json',
		},
		data: {
			id: id,
		},
	}
	return new Promise((resolve, reject) => {
		axios(config)
			.then(function (response) {
				resolve(response)
			})
			.catch(function (error) {
				reject(error)
			})
	})
}

export {
	editPemakaman,
	deletePemakaman,
	getPemakamanData,
	addPemakamanData,
	getInfoKamarRS,
	getRiwayatKamarRS,
	getFrontSiranapKamar,
	updateKamarRS,
	getInfoRS,
	addKamarRS,
	getKamarByRS,
	deleteRS,
	getGeojsonRS,
	editJenisKamar,
	getBatasDesaLayer,
	getLayerProvinsi,
	getWilayahKasusKabupaten,
	getWilayahKasusProvinsi,
	login,
	addJenisKamar,
	getDataCovidKabupaten,
	addDataSeriesCovid,
	getDataSeriesKabupaten,
	updateSingleCovidData,
	getLastUpdated,
	getJenisKamarByRS,
	getKabupatenLayerCluster,
	getSummary,
	getChartJsonTerkonfirmasi,
	getChartJsonSembuh,
	getChartJsonMeninggal,
	getChartJsonStacked,
	getWilayahKabupaten,
	getWilayahKecamatan,
	getWilayahDesa,
	getDataVaksin,
	addVaksinRecord,
	editVaksinRecord,
	deleteVaksinRecord,
	getGeojsonVaksinasi,
	getVaksinChartPerkembangan,
	addRS,
	getAllRS,
	getGeojsonPemakaman,
	editRS,
	getAllJenisKamar,
}
