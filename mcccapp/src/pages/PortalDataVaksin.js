import React, { Suspense, useEffect, lazy, useState } from 'react'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	Link,
	useRouteMatch,
} from 'react-router-dom'

import Swal from 'sweetalert2'

import {
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	Input,
	Label,
	Textarea,
	ModalFooter,
} from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import {
	addVaksinRecord,
	deleteVaksinRecord,
	editVaksinRecord,
	getLastUpdated,
} from '../data/repository'
import WilayahOption from '../components/wilayahoption/WilayahOption'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'
import PinLokasi from '../components/pinlokasi/PinLokasi'

const TableVaksinHead = lazy(() =>
	import('./subpages/PortalDataVaksin/TableVaksinHead')
)

const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

function PortalDataVaksin() {
	let { path, url } = useRouteMatch()
	const [ismodalOpen, setIsModalOpen] = useState(false)
	const [kodewil, setKodewil] = useState('')

	const [penyelenggara, setPenyelenggara] = useState('')
	const [date, setDate] = useState('')
	const [tahapVaksin, setTahapVaksin] = useState('')
	const [alamat, setAlamat] = useState('')
	const [totalTervaksin, setTotalTervaksin] = useState('')
	const [jenisVaksin, setJenisVaksin] = useState('')
	const [lokasi, setLokasi] = useState([-2.548926, 118.0148634])
	const [editLokasi, setEditLokasi] = useState([-2.548926, 118.0148634])

	const [tableRefresh, setTableRefresh] = useState(0)

	const [isEditEvent, setIsEditEvent] = useState(false)
	const [editId, setEditId] = useState(0)

	const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false)

	const reloadTable = () => {
		setTableRefresh(tableRefresh + 1)
	}

	const successAlert = (msg) => {
		Swal.fire({
			title: 'Berhasil!',
			text: msg,
			icon: 'success',
			confirmButtonText: 'Oke',
		})
	}

	const failedAlert = (msg) => {
		Swal.fire({
			title: 'Error!',
			text: msg,
			icon: 'error',
			confirmButtonText: 'Oke',
		})
	}

	const clearForm = () => {
		setKodewil('')
		setPenyelenggara('')
		setDate('')
		setTahapVaksin('')
		setAlamat('')
		setTotalTervaksin('')
		setJenisVaksin('')
		setLokasi([-2.548926, 118.0148634])
	}

	const addNewData = () => {
		//validate
		if (
			kodewil === '' ||
			penyelenggara === '' ||
			date === '' ||
			alamat === '' ||
			totalTervaksin === '' ||
			tahapVaksin === '' ||
			jenisVaksin === '' ||
			lokasi === []
		) {
			alert('Harap Lengkapi Form')
		} else {
			addVaksinRecord(
				kodewil,
				penyelenggara,
				date,
				alamat,
				totalTervaksin,
				tahapVaksin,
				jenisVaksin,
				lokasi[0],
				lokasi[1]
			)
				.then((result) => {
					////console.log(result)
					setIsModalOpen(false)
					reloadTable()
					successAlert('Berhasil Menambah Data')
				})
				.catch((err) => {
					////console.log(err)
					setIsModalOpen(false)
					reloadTable()
					failedAlert('Gagal Menambah Data')
				})
			clearForm()
		}
	}

	const editData = () => {
		//validate
		if (
			kodewil === '' ||
			penyelenggara === '' ||
			date === '' ||
			alamat === '' ||
			totalTervaksin === '' ||
			tahapVaksin === '' ||
			jenisVaksin === '' ||
			lokasi === []
		) {
			alert('Harap Lengkapi Form')
		} else {
			editVaksinRecord(
				editId,
				kodewil,
				penyelenggara,
				date,
				alamat,
				totalTervaksin,
				tahapVaksin,
				jenisVaksin,
				lokasi[0],
				lokasi[1]
			)
				.then((result) => {
					////console.log(result)
					setIsModalOpen(false)
					reloadTable()
					successAlert('Berhasil Edit Data')
				})
				.catch((err) => {
					////console.log(err)
					setIsModalOpen(false)
					reloadTable()
					failedAlert('Gagal Edit Data')
				})
			clearForm()
		}
	}

	const openAddNewModal = () => {
		clearForm()
		setIsEditEvent(false)
		setIsModalOpen(true)
	}

	const buildEditForm = (data) => {
		setIsEditEvent(true)
		////console.log(data)
		setEditId(data.id)
		setKodewil(data.kode_wilayah)
		setPenyelenggara(data.penyelenggara)
		setDate(data.penyelenggaraan_date)
		setTahapVaksin(data.tahap)
		setAlamat(data.alamat)
		setTotalTervaksin(data.total)
		setJenisVaksin(data.jenis_vaksin)
		setLokasi([data.Lng, data.Lat])
		setIsModalOpen(true)
	}

	const deleteData = () => {
		deleteVaksinRecord(editId)
			.then((result) => {
				setIsDeleteModalOpen(false)
				reloadTable()
				successAlert('Berhasil Menghapus Data')
			})
			.catch((err) => {
				setIsDeleteModalOpen(false)
				reloadTable()
				failedAlert('Gagal Menghapus Data')
			})
	}

	return (
		<div className='px-10 py-5'>
			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				tag={Link}
				to='/app/covidupdate'
			>
				<div className='flex items-center'>
					<span>
						Portal Data Vaksin
						{/* {kodewil} {penyelenggara} TGL : {date}{' '}
						{tahapVaksin} {alamat} {totalTervaksin} {jenisVaksin} {lokasi} */}
					</span>
				</div>
				<Button
					onClick={openAddNewModal}
					className='text-white font-bold'
					size='small'
				>
					TAMBAH DATA
				</Button>
			</a>
			<Suspense fallback={<ThemedSuspense />}>
				<Suspense fallback={<ThemedSuspense />}>
					<TableVaksinHead
						refreshTable={tableRefresh}
						onEditClicked={(v) => {
							buildEditForm(v)
						}}
						onDeleteClicked={(v) => {
							setEditId(v.id)
							setIsDeleteModalOpen(true)
						}}
					/>
				</Suspense>
			</Suspense>
			<Modal isOpen={ismodalOpen} onClose={() => setIsModalOpen(false)}>
				<ModalBody className='overflow-y-auto h-screen py-5 px-3 scrollbar-hide'>
					<ModalHeader>
						{isEditEvent ? 'Edit' : 'Tambah'} Data {kodewil}
					</ModalHeader>
					<WilayahOption
						kodewilValue={kodewil}
						onWilayahChanged={(wil) => setKodewil(wil)}
					/>
					<Label className='pt-4'>
						<span>Penyelenggara Vaksinasi</span>
						<Input
							defaultValue={penyelenggara}
							onChange={(e) => setPenyelenggara(e.target.value)}
							placeholder='Ex. Univ, PCM ...'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className=' pt-4'>
						<span>Tanggal Pelaksanaan</span>
						<Flatpickr
							value={moment(date).format('DD-MM-YYYY')}
							options={{ dateFormat: 'd-m-Y' }}
							onChange={([date]) => {
								setDate(moment(date).format('DD-MM-YYYY'))
							}}
							render={({ defaultValue, value, ...props }, ref) => {
								return (
									<CustomInput
										onChange={(e) => {
											setDate(e)
										}}
										defaultValue={defaultValue}
										inputRef={ref}
									/>
								)
							}}
						/>
					</Label>
					<Label className='pt-4'>
						<span>Tahap Vaksinasi</span>
						<Input
							defaultValue={tahapVaksin}
							placeholder='Ex. 1 ...'
							onChange={(e) => setTahapVaksin(e.target.value)}
							type='number'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Alamat</span>
						<Textarea
							defaultValue={alamat}
							onChange={(e) => setAlamat(e.target.value)}
							placeholder='Ex. Jl. Kyai Mojo ...'
							rows='3'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Total Tervaksin</span>
						<Input
							defaultValue={totalTervaksin}
							onChange={(e) => setTotalTervaksin(e.target.value)}
							placeholder='Ex. 1 ...'
							type='number'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Jenis Vaksin</span>
						<Input
							defaultValue={jenisVaksin}
							onChange={(e) => setJenisVaksin(e.target.value)}
							placeholder='Ex. Sinovac'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<Label className='pt-4'>
						<span>Pin Lokasi</span>
						<PinLokasi
							lokasiVal={lokasi}
							onLocationPinned={(e) => setLokasi(e)}
						/>
					</Label>
					<div className='mt-5 hidden sm:block float-right'>
						<Button
							onClick={() => {
								isEditEvent ? editData() : addNewData()
							}}
						>
							Simpan
						</Button>
					</div>
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>

			<Modal
				isOpen={isDeleteModalOpen}
				onClose={() => setIsDeleteModalOpen(false)}
			>
				<ModalHeader>Delete Data</ModalHeader>
				<ModalBody>Apakah anda yakin untuk menghapus ? </ModalBody>
				<ModalFooter>
					<div className='mt-5 hidden sm:block float-right'>
						<Button onClick={deleteData}>Hapus</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	)
}

export default PortalDataVaksin
