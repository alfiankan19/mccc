import React, { useEffect, useState, useRef } from 'react'
import { getGeojsonPemakaman } from '../../data/repository'
import L from 'leaflet'
import { Marker, Popup, Tooltip, GeoJSON, useMap } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import { Button } from '@windmill/react-ui'
import './cluster.css'
import { Link, useRouteMatch } from 'react-router-dom'
import MakamIcon from './makam.svg'
function MakamMapCluster() {
	const [layerVaksin, setLayerVaksin] = useState([])
	const layerRef = useRef(null)
	const parentMap = useMap()
	const { url } = useRouteMatch()
	const getLayer = () => {
		getGeojsonPemakaman()
			.then((result) => {
				console.log(result.data.features[1].geometry.coordinates)
				setLayerVaksin(result.data.features)
			})
			.catch((err) => {
				console.log(err)
			})
	}

	useEffect(() => {
		getLayer()
	}, [0])

	function pointToLayer(feature, latlng) {
		let marker = L.circleMarker(latlng, {
			radius: 8,
			weight: 1,
			fillColor: 'orange',
			color: '#000',
			opacity: 1,
			fillOpacity: 0.8,
		})
		var markers = L.markerClusterGroup()
		markers.addLayer(marker)
		parentMap.addLayer(markers)
	}

	return (
		<MarkerClusterGroup chunkedLoading={true} maxClusterRadius={90}>
			{layerVaksin.map((val) => (
				<Marker
					position={[val.geometry.coordinates[0], val.geometry.coordinates[1]]}
					icon={L.icon({
						iconUrl: MakamIcon,
						iconSize: [30, 30],
					})}
				>
					<Popup>
						<table className='table-auto'>
							<tbody>
								<tr>
									<td className='font-bold pt-2'>Nama &nbsp;&nbsp;</td>
									<td className='pt-2'>{val.properties.nama}</td>
								</tr>
								<tr>
									<td className='font-bold pt-2'>Keterangan &nbsp;&nbsp;</td>
									<td className='pt-2'>{val.properties.note}</td>
								</tr>
								<tr>
									<td className='font-bold pt-2'>Alamat&nbsp;&nbsp;</td>
									<td className='pt-2'>{val.properties.alamat}</td>
								</tr>
							</tbody>
						</table>
					</Popup>
				</Marker>
			))}
		</MarkerClusterGroup>
	)
}

export default MakamMapCluster
