import React, { Suspense, useEffect, lazy, useState } from 'react'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	Link,
	useRouteMatch,
} from 'react-router-dom'

import Swal from 'sweetalert2'

import {
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	Input,
	Label,
	Textarea,
	ModalFooter,
} from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import {
	addJenisKamar,
	addRS,
	addVaksinRecord,
	deleteVaksinRecord,
	editJenisKamar,
	editRS,
	editVaksinRecord,
	getLastUpdated,
} from '../data/repository'
import WilayahOption from '../components/wilayahoption/WilayahOption'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'
import PinLokasi from '../components/pinlokasi/PinLokasi'

const TableJenisRanap = lazy(() =>
	import('../components/jenisranap/TableJenisRanap')
)

const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

function PortalDataJenisRanap() {
	const [ismodalOpen, setIsmodalOpen] = useState(false)
	const [isEditEvent, setIsEditEvent] = useState(false)
	const [editId, setEditId] = useState(0)
	const [jenisKamar, setJenisKamar] = useState('')
	const [refreshtable, setRefreshtable] = useState(0)

	const addNewData = () => {
		if (jenisKamar === '') {
			failedAlert('Lengkapilah Form !!')
		} else {
			addJenisKamar(jenisKamar)
				.then((res) => {
					successAlert('Berhasil Menambah Data')
					setIsmodalOpen(false)
					console.log(res)
					setRefreshtable(refreshtable + 1)
				})
				.catch((err) => {
					failedAlert('Gagal Menambahkan Data')
					setIsmodalOpen(false)
					console.log(err)
				})
		}
	}
	const editData = () => {
		if (jenisKamar === '') {
			failedAlert('Lengkapilah Form !!')
		} else {
			editJenisKamar(editId, jenisKamar)
				.then((res) => {
					successAlert('Berhasil Edit Data')
					setIsmodalOpen(false)
					console.log(res)
					setRefreshtable(refreshtable + 1)
				})
				.catch((err) => {
					failedAlert('Gagal Edit Data')
					setIsmodalOpen(false)
					console.log(err)
				})
		}
	}
	const successAlert = (msg) => {
		Swal.fire({
			title: 'Berhasil!',
			text: msg,
			icon: 'success',
			confirmButtonText: 'Oke',
		})
	}

	const failedAlert = (msg) => {
		Swal.fire({
			title: 'Error!',
			text: msg,
			icon: 'error',
			confirmButtonText: 'Oke',
		})
	}
	const clearForm = () => {
		setJenisKamar('')
	}
	const openAddNewModal = () => {
		clearForm()
		setIsEditEvent(false)
		setIsmodalOpen(true)
	}

	const buildEditForm = (data) => {
		setIsEditEvent(true)
		////console.log(data)
		setEditId(data.id)
		setJenisKamar(data.jenis)
		setIsmodalOpen(true)
	}

	return (
		<div className='px-10 py-5'>
			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				tag={Link}
				to='/app/covidupdate'
			>
				<div className='flex items-center'>
					<span>
						Master Data Jenis Ranap
						{/* {kodewil} {penyelenggara} TGL : {date}{' '}
						{tahapVaksin} {alamat} {totalTervaksin} {jenisVaksin} {lokasi} */}
					</span>
				</div>
				<Button
					onClick={openAddNewModal}
					className='text-white font-bold'
					size='small'
				>
					TAMBAH DATA
				</Button>
			</a>
			<Suspense fallback={<ThemedSuspense />}>
				<Suspense fallback={<ThemedSuspense />}>
					<TableJenisRanap
						refreshTable={refreshtable}
						onEditClicked={(e) => {
							buildEditForm(e)
						}}
					/>
				</Suspense>
			</Suspense>
			<Modal isOpen={ismodalOpen} onClose={() => setIsmodalOpen(false)}>
				<ModalBody>
					<ModalHeader>{isEditEvent ? 'Edit' : 'Tambah'} Data</ModalHeader>
					<Label className='pt-4'>
						<span>Jenis Kamar Ranap</span>
						<Input
							defaultValue={jenisKamar}
							onChange={(e) => setJenisKamar(e.target.value)}
							placeholder='Ex. ICU ...'
							className='mt-1 border-solid border-2 rounded'
						/>
					</Label>
					<div className='mt-5 hidden sm:block float-right'>
						<Button
							onClick={() => {
								isEditEvent ? editData() : addNewData()
							}}
						>
							Simpan
						</Button>
					</div>
				</ModalBody>
				<ModalFooter></ModalFooter>
			</Modal>
		</div>
	)
}

export default PortalDataJenisRanap
