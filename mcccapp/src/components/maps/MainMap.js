import React, { useState } from 'react'
import {
	MapContainer,
	TileLayer,
	LayersControl,
	FeatureGroup,
} from 'react-leaflet'

import BatasDesa from './BatasDesa'
import Loading from './Loading'
import LokasiPanel from './LokasiPanel'
import ToolBox from './ToolBox'
// import { Button } from '@windmill/react-ui'
import { useSelector } from 'react-redux'
import { loadingBatasDesa } from './mapSlice'
import CovidMapCluster from './CovidMapCluster'

function MainMap({ otherLayer }) {
	// eslint-disable-next-line no-unused-vars
	const [peta, setPeta] = useState(null)

	const isLoadingBatasDesa = useSelector(loadingBatasDesa)

	return (
		<div>
			{/* <Button onClick={() => dispatch(showLoading())} >Tambahi</Button> <h3 className="text-white">{loading?"oke":"no"}</h3> <Button onClick={() => dispatch(hideLoading())}>Kurangi</Button> */}
			<MapContainer
				center={[-2.548926, 118.0148634]}
				style={{ height: '600px', borderRadius: '15px' }}
				zoom={5}
				scrollWheelZoom={true}
				whenCreated={(map) => {
					setPeta(map)
				}}
			>
				{/* <Marker position={[51.505, -0.09]}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker> */}

				<LayersControl position='bottomright'>
					<LayersControl.BaseLayer name='Google Street'>
						<TileLayer
							attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							url='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
							subdomains={['mt0', 'mt1', 'mt2', 'mt3']}
						/>
					</LayersControl.BaseLayer>
					<LayersControl.BaseLayer name='OpenStreetMap'>
						<TileLayer
							attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
						/>
					</LayersControl.BaseLayer>

					<LayersControl.BaseLayer checked name='Dark Stadia'>
						<TileLayer
							attribution='&copy; <a href="maptiler.com">maptiler.com</a> contributors'
							url='https://api.maptiler.com/maps/ch-swisstopo-lbm-dark/256/{z}/{x}/{y}.png?key=aGxk663lHtG4BclbDezb'
						/>
					</LayersControl.BaseLayer>
					<LayersControl.Overlay name='Batas Desa'>
						<FeatureGroup>
							<BatasDesa />
						</FeatureGroup>
					</LayersControl.Overlay>
					{otherLayer.map((val) =>
						val.show ? (
							<LayersControl.Overlay checked name={val.name}>
								<FeatureGroup>{val.mapLayer}</FeatureGroup>
							</LayersControl.Overlay>
						) : (
							<LayersControl.Overlay name={val.name}>
								<FeatureGroup>{val.mapLayer}</FeatureGroup>
							</LayersControl.Overlay>
						)
					)}
				</LayersControl>
				<Loading isLoading={isLoadingBatasDesa} position='bottomright' />
				<LokasiPanel position='bottomleft' />
				<ToolBox position='topright' />
			</MapContainer>
		</div>
	)
}

export default MainMap
