import React, { useState, useEffect } from 'react'
import { Select, Input, Label } from '@windmill/react-ui'
import {
	getWilayahKasusProvinsi,
	getWilayahKabupaten,
	getWilayahKecamatan,
	getWilayahDesa,
} from '../../data/repository'
import {
	TableContainer,
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	ModalFooter,
} from '@windmill/react-ui'
import Flatpickr from 'react-flatpickr'
import moment from 'moment'
import 'flatpickr/dist/themes/dark.css'

const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

function DataVaksinFilter({ onFilterSaved, isModalOpen, onCloseModal }) {
	const [provinsidata, setProvinsidata] = useState([])
	const [kabupatendata, setKabupatendata] = useState([])
	const [kecamatandata, setKecamatandata] = useState([])
	const [desadata, setDesadata] = useState([])
	const [kodewilProv, setKodewilProv] = useState(34)
	const [kodewilKab, setKodewilKab] = useState('00')
	const [kodewilKec, setKodewilKec] = useState('00')
	const [kodewilDes, setKodewilDes] = useState('00')
	const [dateRange, setDateRange] = useState(
		moment().subtract(1, 'month').format('DD-MM-Y').toString() +
			' to ' +
			moment().format('DD-MM-Y').toString()
	)

	const getProvinsiData = () => {
		setProvinsidata([])
		setKabupatendata([])
		setKecamatandata([])
		setDesadata([])

		setKodewilProv('00')
		setKodewilKab('00')
		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
			})
			.catch((err) => {
				getProvinsiData()
				//console.log(err)
			})
	}
	const getKabupatenData = (provid) => {
		setKabupatendata([])
		setKecamatandata([])
		setDesadata([])

		setKodewilKab('00')
		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKabupaten(provid)
			.then((result) => {
				//console.log(result.data)
				setKabupatendata(result.data)
			})
			.catch((err) => {
				getKabupatenData(provid)
				//console.log(err)
			})
	}

	const getKecamatanData = (kabid) => {
		setKecamatandata([])
		setDesadata([])

		setKodewilKec('00')
		setKodewilDes('00')

		getWilayahKecamatan(kabid)
			.then((result) => {
				//console.log(result.data)
				setKecamatandata(result.data)
			})
			.catch((err) => {
				getKecamatanData(kabid)
				//console.log(err)
			})
	}

	const getDesaData = (kecid) => {
		setDesadata([])

		setKodewilDes('00')

		getWilayahDesa(kecid)
			.then((result) => {
				//console.log(result.data)
				setDesadata(result.data)
			})
			.catch((err) => {
				getDesaData(kecid)
				//console.log(err)
			})
	}

	useEffect(() => {
		getProvinsiData()
	}, [0])
	return (
		<>
			{/* <Modal isOpen={isModalOpen} onClose={closeModal}></Modal> */}
			<Modal isOpen={isModalOpen} onClose={() => onCloseModal()}>
				<ModalHeader>Filter Data {dateRange}</ModalHeader>
				<ModalBody>
					<Label className=''>
						<span>
							Pilih Provinsi {kodewilProv} {kodewilKab} {kodewilKec}{' '}
							{kodewilDes}
						</span>
						<Select
							onChange={(e) => {
								getKabupatenData(e.target.value)
								setKodewilProv(e.target.value)
							}}
							defaultValue={kodewilProv}
							className='mt-1 border-solid border-2 rounded'
						>
							<option key={'00'} value={'00'}>
								- Semua -
							</option>
							{provinsidata.map((v) => (
								<option key={v.idwil} value={v.idwil}>
									{v.nama}
								</option>
							))}
						</Select>
					</Label>
					{kabupatendata.length !== 0 ? (
						<Label className='pt-4'>
							<span>Pilih Kabupaten</span>
							<Select
								onChange={(e) => {
									getKecamatanData(e.target.value)
									setKodewilKab(e.target.value)
								}}
								defaultValue={kodewilKab}
								className='mt-1 border-solid border-2 rounded'
							>
								<option key={'00'} value={'00'}>
									- Semua -
								</option>
								{kabupatendata.map((v) => (
									<option key={v.kpu_idkab} value={v.kpu_idkab}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					) : null}
					{kecamatandata.length !== 0 ? (
						<Label className='pt-4'>
							<span>Pilih Kecamatan</span>
							<Select
								onChange={(e) => {
									setKodewilKec(e.target.value)
									getDesaData(e.target.value)
								}}
								defaultValue={kodewilKec}
								className='mt-1 border-solid border-2 rounded'
							>
								<option key={'00'} value={'00'}>
									- Semua -
								</option>
								{kecamatandata.map((v) => (
									<option key={v.kpu_idkec} value={v.kpu_idkec}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					) : null}
					{desadata.length !== 0 ? (
						<Label className='pt-4'>
							<span>Pilih Desa</span>
							<Select
								onChange={(e) => {
									setKodewilDes(e.target.value)
								}}
								defaultValue={kodewilDes}
								className='mt-1 border-solid border-2 rounded'
							>
								<option key={'00'} value={'00'}>
									- Semua -
								</option>
								{desadata.map((v) => (
									<option key={v.id_desa} value={v.id_desa}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					) : null}
					<Label className='pt-4'>
						<span>Tanggal Data</span>
						<Flatpickr
							options={{
								defaultDate: [
									moment().subtract(1, 'month').format('DD-MM-Y'),
									moment().format('DD-MM-Y'),
								],
								dateFormat: 'd-m-Y',
								mode: 'range',
							}}
							onChange={([date, dateb]) => {
								console.log(
									moment(date).format('DD-MM-Y') +
										' to ' +
										moment(dateb).format('DD-MM-Y')
								)
								setDateRange(
									moment(date).format('DD-MM-Y') +
										' to ' +
										moment(dateb).format('DD-MM-Y')
								)
							}}
							render={({ defaultValue, value, ...props }, ref) => {
								return (
									<CustomInput
										onChange={(e) => {
											setDateRange(e.target.value)
										}}
										defaultValue={defaultValue}
										inputRef={ref}
									/>
								)
							}}
						/>
					</Label>
				</ModalBody>
				<ModalFooter>
					<div className='hidden sm:block'>
						<Button
							onClick={() => {
								onFilterSaved([
									kodewilProv,
									kodewilKab,
									kodewilKec,
									kodewilDes,
									dateRange,
								])
								onCloseModal()
							}}
						>
							Terapkan
						</Button>
					</div>

					{/* <div className='block w-full sm:hidden'>
						<Button block size='large'>
							Close
						</Button>
					</div> */}
				</ModalFooter>
			</Modal>
		</>
	)
}

export default DataVaksinFilter
