

API Docs (POSTMAN):
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/7847165-b2b2980e-7bbf-4c27-ab28-5ce785ee3656?action=collection%2Ffork&collection-url=entityId%3D7847165-b2b2980e-7bbf-4c27-ab28-5ce785ee3656%26entityType%3Dcollection%26workspaceId%3D00780acb-4968-4dfb-8d78-8fba62e62661)

Folder :
- Laravel App : mccc
- React Builded : app
- React Dev : mcccapp


## Open Source Attribution :

<div>Icons made by <a href="https://www.flaticon.com/authors/good-ware" title="Good Ware">Good Ware</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>


## Message to MCCC
Assalamualaikum 
berikut saya sampaikan progress webapp gis mccc

Versi saat ini V1.2.0

Version History :

- V1.0.0
    - Web Base App
    - Dashboard Kasus Covid19 bisa diakses public untuk Melihat persebaran dan perkembangan data
    - Portal Data Covid untuk input dan manajemen data kasus covid19

- V1.1.0
    - Portal Data Vaksinasi untuk input dan manajemen data  vaksinasi

- V1.2.0
    - Portal Data Siranap untuk input data siranap
    - Halaman Siranap untuk public mencari data rumah sakit dan detail kamar
    - Update Grafik Vaksin pada Dashboard



## WebApp Sudah bisa digunakan untuk input data yaitu data kasus, vaksinasi, dan Siranap,

## data siranap bisa mulai di input, Video cara input dapat dilihat di : https://www.icloud.com/iclouddrive/0TR1ikGYhhEJiKYTlG_jrdIXA#tutorial-input-data-siranap 

## Video cara input data vaksinasi : https://www.icloud.com/iclouddrive/0iLIxsukPtzrEgexa7F8AvQiA#Screen_Recording_2021-09-08_at_09.30

## Video Cara menggunakan dashboard : https://www.icloud.com/iclouddrive/001fhkeHJeG8oVIbHV7_XUQLQ#tutorial-penggunaan-dashboard

Akses Utama : https://mccc-e4eee.web.app/app/dashboard

Akses Admin : https://mccc-e4eee.web.app/login





## CHANGELOG

- V1.0.0
    - Web Base App
    - Dashboard Kasus Covid19 bisa diakses public untuk Melihat persebaran dan perkembangan data
    - Portal Data Covid untuk input dan manajemen data kasus covid19

- V1.1.0
    - Portal Data Vaksinasi untuk input dan manajemen data  vaksinasi

- V1.2.0
    - Portal Data Siranap untuk input data siranap
    - Halaman Siranap untuk public mencari data rumah sakit dan detail kamar
    - Update Grafik Vaksin pada Dashboard