import React from 'react'
import Loader from 'react-loader-spinner'

const POSITION_CLASSES = {
	bottomleft: 'leaflet-bottom leaflet-left',
	bottomright: 'leaflet-bottom leaflet-right',
	topleft: 'leaflet-top leaflet-left',
	topright: 'leaflet-top leaflet-right',
}
function Loading({ position, zoom, isLoading }) {
	const positionClass =
		(position && POSITION_CLASSES[position]) || POSITION_CLASSES.topright
	return (
		<div
			className={positionClass}
			style={isLoading ? { display: 'block' } : { display: 'none' }}
		>
			<div className='leaflet-control leaflet-bar bg-black'>
				<div className='grid grid-cols-3 gap-4 p-2'>
					<div>
						<Loader type='Oval' color='#00BFFF' height={30} width={30} />
					</div>
					<div>
						<span className='text-white font-bold	'>LOADING</span>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Loading
