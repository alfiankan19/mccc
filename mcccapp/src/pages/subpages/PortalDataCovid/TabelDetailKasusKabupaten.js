import React, { useState, useEffect } from 'react'
import TableDetailKabupaten from '../../../components/portaldatacovid/TableDetailKabupaten'
import SectionTitle from '../../../components/Typography/SectionTitle'
import { Select, Input, Label } from '@windmill/react-ui'
import { useParams } from 'react-router-dom'
import moment from 'moment'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'

const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

const TabelDetailKasusKabupaten = ({ onUpdatedData }) => {
	let { kodewil } = useParams()
	const [dateRange, setDateRange] = useState(
		moment().subtract(1, 'month').format('DD-MM-Y').toString() +
			' to ' +
			moment().format('DD-MM-Y').toString()
	)
	const [kabupateninfo, setKabupatenInfo] = useState(String(kodewil))

	return (
		<>
			<SectionTitle>
				Record Data {kabupateninfo} ({kodewil})
			</SectionTitle>
			<div class='grid grid-cols-2'>
				<div>
					<div className='py-3'>
						<Label className=' max-w-xs'>
							<span>Range Tanggal</span>
							<Flatpickr
								options={{
									defaultDate: [
										moment().subtract(1, 'month').format('DD-MM-Y'),
										moment().format('DD-MM-Y'),
									],
									dateFormat: 'd-m-Y',
									mode: 'range',
								}}
								onChange={([date, dateb]) => {
									////console.log(date.getDate())
									setDateRange(
										moment(date).format('DD-MM-Y') +
											' to ' +
											moment(dateb).format('DD-MM-Y')
									)
								}}
								render={({ defaultValue, value, ...props }, ref) => {
									return (
										<CustomInput
											onChange={(e) => {
												setDateRange(e.target.value)
											}}
											defaultValue={defaultValue}
											inputRef={ref}
										/>
									)
								}}
							/>
						</Label>
					</div>
				</div>
				<div></div>
			</div>
			<TableDetailKabupaten
				onUpdatedData={onUpdatedData}
				dateRange={dateRange}
				kodewil={kodewil}
				namaKabupaten={(e) => setKabupatenInfo(e)}
			/>
		</>
	)
}

export default TabelDetailKasusKabupaten
