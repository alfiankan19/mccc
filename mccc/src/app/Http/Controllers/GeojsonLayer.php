<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;
use Illuminate\Support\Facades\DB;

class GeojsonLayer extends Controller
{

  public function coba()
  {
      return "OJELAH";
  }

    public function getlayerprov()
    {

        //GET PROV
        $val = "'30'";
        $lid = 4;
        $layer = DB::table('data_prov')
             ->select(DB::raw("data_prov.kpu_idprov id, concat('Prop. ',data_prov.provinsi) nama, $val val,ST_AsGeoJson(data_prov.SHAPE) geom,kpu_idprov idwil"))
             ->get();

        $json_prov = array();
        $i=0;
        foreach ($layer as $row)
        {
            $attributes = array("NAME"=>$row->nama, "value"=>$row->val, "lid"=>$lid, "idwil" => $row->idwil); // 
            $data = array(
                "type"=>"Feature", 
                "id"=>$row->id, 
                "properties"=> $attributes, 
                "geometry"=> json_decode($row->geom)
            );        
            $json_prov[$i]=$data;
            $i++;
        }  

    
        return response()->json(array(
                            "type"=>"FeatureCollection",
                            "features"=>$json_prov
        ));
    }

    public function getBatasDesa(Request $request)
    {
        $dimension = $request->query('dimension', 'Helen');
        $zoom = $request->query('zoom', 'Helen');
        $type = $request->query('type', '1');
        $crop = $request->query('crop', '0x0');

        ini_set('memory_limit', '-1');
        if($type==1)
        {
            $val = "'30'";
            $valdes = "'30'";
        } else {
            $val = "'30'";
            $valdes = "'30'";
        }


        $dimension = explode("x",$dimension);

        $crops = explode("x",$crop);
        $andwheredesa="";
        $andwherekec="";
        $andwherekab="";
        $andwhereprov="";


        switch($crops[0]){
            case 2:
                $andwheredesa=" and kpu_idkec='".$crops[1]."'";
                $andwherekec=" and a.kpu_idkec='".$crops[1]."'";
                $andwherekab=" and a.OGR_FID='0'";
                $andwhereprov=" and a.OGR_FID='0'";
                break;

            case 3:
                $andwheredesa=" and kpu_idkab='".$crops[1]."'";
                $andwherekec=" and a.kpu_idkab='".$crops[1]."'";
                $andwherekab=" and a.kpu_idkab='".$crops[1]."'";
                $andwhereprov=" and a.OGR_FID='0'";
                break;
            case 4:
                $andwheredesa=" and kpu_idprov='".$crops[1]."'";
                $andwherekec=" and a.kpu_idprov='".$crops[1]."'";
                $andwherekab=" and a.kpu_idprov='".$crops[1]."'";
                $andwhereprov=" and a.kpu_idprov='".$crops[1]."'";
                break;
        }

        if($zoom > 12){
            //$tingkat = "desa";
            $g1="ST_Envelope(ST_GeomFromText(concat('LineString(',".$dimension[0].",' ',".$dimension[1].",',',".$dimension[2].",' ',".$dimension[3].",')'),2))";
            // $sql="SELECT id id,concat('Desa ',desa) nama,$valdes val,ST_AsWKB(SHAPE) geom, kode_desa idwil
            //       FROM desa
            //       WHERE intersects($g1, SHAPE)=1 $andwheredesa ";
            // $lid=1;

            $tingkat = "desa";
            $layer = DB::table('desa')
             ->select(DB::raw("id id, concat('Desa ',desa) nama, $valdes val, kode_desa idwil, ST_AsGeoJson(desa.SHAPE) geom,kode_desa idwil"))
             ->whereRaw('intersects('.$g1.', SHAPE)=1 '.$andwheredesa)
             ->get();
             $lid=1;
        }

        if($zoom > 10 && $zoom <= 12){
            $tingkat = "kecamatan";
            $lid=2;

            $g1="ST_Envelope(ST_GeomFromText(concat('LineString(',".$dimension[0].",' ',".$dimension[1].",',',".$dimension[2].",' ',".$dimension[3].",')'),2))";
            // $sql = "SELECT
            //             a.kpu_idkec id
            //             , concat('',a.kecamatan) nama
            //             , $val val
            //             ,ST_AsWKB(a.SHAPE) geom
            //             ,kpu_idkec idwil
            //         FROM
            //             data_kec a
            //         WHERE intersects($g1, a.SHAPE)=1 $andwherekec ";

            $layer = DB::table('data_kec')
            ->select(DB::raw("data_kec.kpu_idkec id, concat('Kecamatan ',data_kec.kecamatan) nama, $val val,ST_AsGeoJson(data_kec.SHAPE) geom, kpu_idkec idwil"))
            ->whereRaw('intersects('.$g1.', SHAPE)=1 '.$andwherekec)
            ->get();

        }

        if($zoom > 8 && $zoom <= 10){// (select avg(kpu_persen) from dataspasial b where b.provinsi=a.provinsi and b.kabkot=a.kabkot group by b.provinsi,b.kabkot)
            $tingkat = "kabupaten";
            $lid=3;
            $g1="ST_Envelope(ST_GeomFromText(concat('LineString(',".$dimension[0].",' ',".$dimension[1].",',',".$dimension[2].",' ',".$dimension[3].",')'),2))";
            // $sql = "SELECT
            //             a.kpu_idkab id
            //             , concat('',a.kabkot) nama
            //             , $val val
            //             ,ST_AsWKB(a.SHAPE) geom
            //             ,kpu_idkab idwil
            //         FROM
            //             data_kab a
            //         WHERE intersects($g1, a.SHAPE)=1 $andwherekab ";


            $layer = DB::table('data_kab')
            ->select(DB::raw("data_kab.kpu_idkab id, concat('Kabupaten/Kota ',data_kab.kabkot) nama, $val val,ST_AsGeoJson(data_kab.SHAPE) geom, kpu_idkab idwil"))
            ->whereRaw('intersects('.$g1.', SHAPE)=1 '.$andwherekab)
            ->get();
        }

        if($zoom > 4 && $zoom <= 8){// (select avg(kpu_persen) from dataspasial b where b.provinsi=a.provinsi and b.kabkot=a.kabkot group by b.provinsi,b.kabkot)
            $tingkat = "provinsi";
            $lid=4;
            $g1="ST_Envelope(ST_GeomFromText(concat('LineString(',".$dimension[0].",' ',".$dimension[1].",',',".$dimension[2].",' ',".$dimension[3].",')'),2))";
            // $sql = "SELECT
            //             a.kpu_idprov id
            //             , concat('',a.provinsi) nama
            //             , $val val
            //             ,ST_AsWKB(a.SHAPE) geom
            //             ,kpu_idprov idwil
            //         FROM
            //             data_prov a
            //         WHERE intersects($g1, a.SHAPE)=1 $andwhereprov ";


            $layer = DB::table('data_prov')
            ->select(DB::raw("data_prov.kpu_idprov id, concat('Provinsi ',data_prov.provinsi) nama, $val val,ST_AsGeoJson(data_prov.SHAPE) geom, kpu_idprov idwil"))
            ->whereRaw('intersects('.$g1.', SHAPE)=1 '.$andwhereprov)
            ->get();

        }
        if($zoom <= 4){
            $tingkat = "pulau";
            $lid=5;
            $g1="ST_Envelope(ST_GeomFromText(concat('LineString(',".$dimension[0].",' ',".$dimension[1].",',',".$dimension[2].",' ',".$dimension[3].",')'),2))";
            // $sql = "SELECT
            //             a.kpu_idpul id
            //             , a.kpu_pulau nama
            //             , $val val
            //             ,ST_AsWKB(a.SHAPE) geom
            //             ,kpu_idpul idwil
            //         FROM
            //             data_pulau a
            //         WHERE intersects($g1, a.SHAPE)=1 $andwhereprov ";
            $layer = DB::table('data_pulau')
            ->select(DB::raw("data_pulau.kpu_idpul id, data_pulau.kpu_pulau nama, $val val,ST_AsGeoJson(data_pulau.SHAPE) geom, kpu_idpul idwil"))
            ->whereRaw('intersects('.$g1.', SHAPE)=1 '.$andwhereprov)
            ->get();

        }






        $json = array();
        $i=0;
        foreach ($layer as $row)
        {
            $attributes = array("NAME"=>$row->nama, "value"=>$row->val, "lid"=>$lid); // 
            $data = array(
                "type"=>"Feature", 
                "id"=>$row->id, 
                "idwil" => $row->idwil,
                "properties"=> $attributes, 
                "geometry"=> json_decode($row->geom)
            );        
            $json[$i]=$data;
            $i++;
        }  

    
        return response()->json(array(
                            "type"=>"FeatureCollection",
                            "features"=>$json
        ));
    }


    public function singleLayer($tingkat, $kodewil)
    {
        $val = "'30'";
        $lid = 4;
        $layer = DB::table('data_prov')
             ->select(DB::raw("data_prov.kpu_idprov id, concat('Prop. ',data_prov.provinsi) nama, $val val,ST_AsGeoJson(data_prov.SHAPE) geom"))
             ->where('kpu_idprov',$kodewil)
             ->get();

        $json_prov = array();
        $i=0;
        foreach ($layer as $row)
        {
            $attributes = array("NAME"=>$row->nama, "value"=>$row->val, "lid"=>$lid); // 
            $data = array(
                "type"=>"Feature", 
                "id"=>$row->id, 
                
                "properties"=> $attributes, 
                "geometry"=> json_decode($row->geom)
            );        
            $json_prov[$i]=$data;
            $i++;
        }  

    
        return response()->json(array(
                            "type"=>"FeatureCollection",
                            "features"=>$json_prov
        ));
    }

    public function getlayervaksinasi()
    {
        $layer = DB::table('vaksinasi_1')
             ->select(DB::raw("id, penyelenggara, penyelenggaraan_date, total, tahap, jenis_vaksin,alamat, ST_AsGeoJson(Lokasi) geom,kode_wilayah idwil"))
             ->get();

             $json = array();
             $i=0;
             foreach ($layer as $row)
             {
                 $attributes = array(
                     "penyelenggara" => $row->penyelenggara, 
                     "penyelenggaraan_date" => $row->penyelenggaraan_date, 
                     "total" => $row->total,
                     "tahap" => $row->tahap,
                     "jenis_vaksin" => $row->jenis_vaksin,
                     "alamat" => $row->alamat,
                     "idwil" => $row->idwil
                );
                 $data = array(
                     "type"=>"Feature", 
                     "id"=>$row->id, 
                     "idwil" => $row->idwil,
                     "properties"=> $attributes, 
                     "geometry"=> json_decode($row->geom)
                 );        
                 $json[$i]=$data;
                 $i++;
             }  
     
         
             return response()->json(array(
                                 "type"=>"FeatureCollection",
                                 "features"=>$json
             ));
    }

    public function getlayerrs()
    {
        $layer = DB::table('rumah_sakit')
             ->select(DB::raw("id, nama ,alamat, ST_AsGeoJson(Lokasi) geom"))
             ->get();

             $json = array();
             $i=0;
             foreach ($layer as $row)
             {
                 $attributes = array(
                     "nama" => $row->nama, 
                     "alamat" => $row->alamat, 
                );
                 $data = array(
                     "type"=>"Feature", 
                     "id"=>$row->id, 
                     "properties"=> $attributes, 
                     "geometry"=> json_decode($row->geom)
                 );        
                 $json[$i]=$data;
                 $i++;
             }  
     
         
             return response()->json(array(
                                 "type"=>"FeatureCollection",
                                 "features"=>$json
             ));
    }


    public function getLayerPemakaman()
    {
        $layer = DB::table('lokasi_meninggal')
             ->select(DB::raw("id, nama, note, alamat, ST_AsGeoJson(Lokasi) geom"))
             ->get();

             $json = array();
             $i=0;
             foreach ($layer as $row)
             {
                 $attributes = array(
                     "nama" => $row->nama, 
                     "note" => $row->note, 
                     "alamat" => $row->alamat, 
                );
                 $data = array(
                     "type"=>"Feature", 
                     "id"=>$row->id, 
                     "properties"=> $attributes, 
                     "geometry"=> json_decode($row->geom)
                 );        
                 $json[$i]=$data;
                 $i++;
             }  
     
         
             return response()->json(array(
                                 "type"=>"FeatureCollection",
                                 "features"=>$json
             ));
    }

}