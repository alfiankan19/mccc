import React from 'react'

import PageTitle from '../components/Typography/PageTitle'

function Blank() {
	return (
		<div>
			<PageTitle>Blank</PageTitle>
		</div>
	)
}

export default Blank
