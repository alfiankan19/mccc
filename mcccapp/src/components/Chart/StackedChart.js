import React, { useEffect, useRef, useState } from 'react'

import * as am4core from '@amcharts/amcharts4/core'
import * as am4charts from '@amcharts/amcharts4/charts'
import chart_dark from '@amcharts/amcharts4/themes/dark'
import chart_light from '@amcharts/amcharts4/themes/animated'
import PageTitle from '../Typography/PageTitle'
import Loader from 'react-loader-spinner'
import { Select, Input, Label } from '@windmill/react-ui'
import {
	getWilayahKasusProvinsi,
	getChartJsonTerkonfirmasi,
	getChartJsonStacked,
} from '../../data/repository'

// eslint-disable-next-line react/prop-types
function StackedChart({ idchart }) {
	const chartRef = useRef(null)
	const [isLoading, setIsLoading] = useState(false)
	const [chartData, setChartData] = useState([])

	const [provinsidata, setProvinsidata] = useState([])
	const [whereProv, setWhereProv] = useState(11)

	const getProvinsiData = () => {
		setProvinsidata([])

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
			})
			.catch((err) => {
				getProvinsiData()
				//console.log(err)
			})
	}

	const getChartData = (provid) => {
		setIsLoading(true)
		getChartJsonStacked(provid)
			.then((result) => {
				//console.log(result.data)
				setChartData(result.data.data)
				setIsLoading(false)
			})
			.catch((err) => {
				//console.log(err)
				setIsLoading(false)
			})
	}

	// Create series
	const createSeries = (chart, field, name, color) => {
		// Set up series
		let series = chart.series.push(new am4charts.ColumnSeries())
		series.name = name
		series.dataFields.valueY = field
		series.dataFields.categoryX = 'wilayah'
		series.sequencedInterpolation = true

		// Make it stacked
		series.stacked = true

		// Configure columns
		series.columns.template.width = am4core.percent(60)
		series.columns.template.tooltipText =
			'[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}'

		// Add label
		// let labelBullet = series.bullets.push(new am4charts.LabelBullet())
		// labelBullet.label.text = '{valueY}'
		// labelBullet.locationY = 0.5
		// labelBullet.label.hideOversized = true

		series.fill = am4core.color(color)

		return series
	}

	useEffect(() => {
		if (!chartRef.current) {
			if (localStorage.getItem('theme') === 'dark') {
				am4core.useTheme(chart_dark)
			} else {
				am4core.useTheme(chart_light)
			}

			let x = am4core.create(idchart, am4charts.XYChart)

			// Add data
			x.data = chartData

			// Create axes
			let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis())
			categoryAxis.dataFields.category = 'wilayah'
			categoryAxis.renderer.grid.template.location = 0
			categoryAxis.renderer.minGridDistance = 30
			categoryAxis.renderer.labels.template.horizontalCenter = 'right'
			categoryAxis.renderer.labels.template.verticalCenter = 'middle'
			categoryAxis.renderer.labels.template.rotation = 270
			categoryAxis.tooltip.disabled = true
			categoryAxis.renderer.minHeight = 110

			let valueAxis = x.yAxes.push(new am4charts.ValueAxis())
			valueAxis.renderer.inside = true
			valueAxis.renderer.labels.template.disabled = true
			valueAxis.min = 0

			let s1 = createSeries(x, 'terkonfirmasi', 'Terkonfirmasi', '#3479e0')
			let s2 = createSeries(x, 'aktif', 'Aktif', '#e03464')
			let s3 = createSeries(x, 'sembuh', 'Sembuh', '#057a55')
			let s4 = createSeries(x, 'meninggal', 'Meninggal', '#dde034')

			// Legend
			x.legend = new am4charts.Legend()

			let scrollbarX = new am4charts.XYChartScrollbar()
			scrollbarX.series.push(s1)
			scrollbarX.series.push(s2)
			scrollbarX.series.push(s3)
			scrollbarX.series.push(s4)
			x.scrollbarX = scrollbarX
			x.scrollbarX.parent = x.topAxesContainer
			let scrollAxis = x.scrollbarX.scrollbarChart.xAxes.getIndex(0)
			scrollAxis.renderer.labels.template.disabled = true

			chartRef.current = x
		}

		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		if (chartRef.current) {
			chartRef.current.data = chartData
		}
	}, [chartData])

	useEffect(() => {
		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		getProvinsiData()
		getChartData('')
	}, [0])

	return (
		<div className='pt-12'>
			<div className='grid grid-col-2 grid-flow-col gap-1'>
				<div>
					<PageTitle>Kasus Per Daerah</PageTitle>
				</div>
				<div>
					<div className='relative inline-block float-right text-gray-700 pt-5'>
						<Label className=' max-w-max'>
							<span>Pilih Provinsi</span>
							<Select
								onChange={(e) => {
									setWhereProv(e.target.value)
									getChartData(e.target.value)
								}}
								className='mt-1 border-solid border-2 rounded'
							>
								<option key={0} value=''>
									Semua
								</option>
								{provinsidata.map((v) => (
									<option key={v.idwil} value={v.idwil}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					</div>
				</div>
			</div>

			{isLoading ? (
				<center>
					<div className='pt-5'>
						<Loader type='Oval' color='#00BFFF' height={30} width={30} />
					</div>
				</center>
			) : null}
			<div
				id={idchart}
				style={{ width: '100%', height: '500px', borderRadius: 15 }}
			></div>
		</div>
	)
}

export default StackedChart
