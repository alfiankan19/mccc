import React, { useEffect, useRef, useState } from 'react'
import { useMap, GeoJSON } from 'react-leaflet'
import L from 'leaflet'
import MouseTooltip from 'react-sticky-mouse-tooltip'
import { useDispatch } from 'react-redux'
import { showLoadingBatasDesa, hideLoadingBatasDesa } from './mapSlice'
import { getLayerProvinsi, getBatasDesaLayer } from '../../data/repository'
const maptype = 1

function BatasDesa() {
	const [dataGeoJson, setdataGeoJson] = useState([])
	const [infolokasi, setinfolokasi] = useState('*')
	const [ishovering, setHovering] = useState(false)
	const batasDesaRef = useRef(null)
	const parentMap = useMap()

	const dispatch = useDispatch()

	useEffect(() => {
		if (localStorage.getItem('LAYER_PROVINSI') == null) {
			pindahkan()
		}
		setEvent(parentMap)
		refreshmapLocal(parentMap)
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [0])

	const pindahkan = () => {
		getLayerProvinsi().then((result) => {
			localStorage.setItem('LAYER_PROVINSI', JSON.stringify(result.data))
		})
	}

	function highlightFeature(e) {
		var layer = e.target

		setinfolokasi(layer.feature.properties.NAME)
		setHovering(true)
		layer.setStyle({
			weight: 0.5,
			color: '#666',
			dashArray: '',
			fillOpacity: 0.7,
		})
		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			layer.bringToFront()
		}
	}
	function resetHighlight(e) {
		batasDesaRef.current.resetStyle(e.target)
		setHovering(false)
	}
	function zoomToFeature(e) {
		if (e.sourceTarget.feature.properties.lid === 1) {
			parentMap.setZoom(13)
		} else if (e.sourceTarget.feature.properties.lid === 2) {
			parentMap.setZoom(12)
		} else if (e.sourceTarget.feature.properties.lid === 3) {
			parentMap.setZoom(9)
		} else if (e.sourceTarget.feature.properties.lid === 4) {
			parentMap.setZoom(7)
		}
		parentMap.flyToBounds(e.sourceTarget._bounds)

		if (e.sourceTarget.feature.properties.lid === 1) {
			var layer = e.target

			layer.setStyle({
				weight: 0.5,
				color: '#00FFFF',
				dashArray: '',
				fillOpacity: 0.7,
			})
			if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
				layer.bringToFront(layer)
			}

			goto()
		} else {
			goto(layer)
		}
	}

	function goto() {}

	function refreshmap(map) {
		dispatch(showLoadingBatasDesa())
		getBatasDesaLayer(map, maptype)
			.then((response) => {
				if (response !== null) {
					batasDesaRef.current.clearLayers().addData(response.data)
					setdataGeoJson(response.data)
					dispatch(hideLoadingBatasDesa())
				}
			})
			.catch((err) => {
				//console.log(err)
				dispatch(hideLoadingBatasDesa())
			})
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight,
			click: zoomToFeature,
		})
	}

	const refreshmapLocal = (map) => {
		let layer
		if (map.getZoom() <= 8) {
			if (localStorage.getItem('LAYER_PROVINSI') != null) {
				layer = localStorage.getItem('LAYER_PROVINSI')
				batasDesaRef.current.clearLayers().addData(JSON.parse(layer))
				setdataGeoJson(JSON.parse(layer))
			} else {
				refreshmap(map)
			}
		} else if (map.getZoom() > 8 && map.getZoom() <= 10) {
			refreshmap(map)
		} else if (map.getZoom() > 10 && map.getZoom() <= 12) {
			refreshmap(map)
		} else if (map.getZoom() > 12) {
			refreshmap(map)
		}
	}

	const setEvent = (map) => {
		map.on('moveend', () => {
			//refreshmap(map)
			refreshmapLocal(map)
		})
	}

	return (
		<div>
			<GeoJSON
				data={dataGeoJson}
				ref={batasDesaRef}
				onEachFeature={onEachFeature}
			/>
			<MouseTooltip
				visible={ishovering}
				offsetX={15}
				offsetY={10}
				style={{ zIndex: 10000 }}
			>
				<span
					style={{ color: 'white', backgroundColor: 'black', padding: '5px' }}
				>
					{infolokasi}
				</span>
			</MouseTooltip>
		</div>
	)
}

export default BatasDesa
