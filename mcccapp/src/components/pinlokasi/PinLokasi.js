import React, { useState } from 'react'
import {
	MapContainer,
	TileLayer,
	LayersControl,
	Marker,
	Popup,
	FeatureGroup,
} from 'react-leaflet'
import GeocodingWrapper from './GeocodingWrapper'

function PinLokasi({ onLocationPinned, lokasiVal }) {
	const [center, setCenter] = useState()

	const setPinCenter = (map) => {
		setCenter(map.getCenter())
		onLocationPinned([map.getCenter().lat, map.getCenter().lng])
		map.on('dragend', () => {
			setCenter(map.getCenter())
			//console.log(map.getCenter())
			onLocationPinned([map.getCenter().lat, map.getCenter().lng])
		})
	}

	return (
		<div className='mt-3'>
			<MapContainer
				center={lokasiVal}
				style={{ height: '500px', borderRadius: '15px' }}
				zoom={15}
				scrollWheelZoom={false}
				whenCreated={(map) => {
					setPinCenter(map)
				}}
			>
				<TileLayer
					attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					url='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
					subdomains={['mt0', 'mt1', 'mt2', 'mt3']}
				/>
				{center ? (
					<Marker position={center}>
						<Popup>
							<span>Pin Lokasi</span>
						</Popup>
					</Marker>
				) : null}
				<GeocodingWrapper
					position='topright'
					isFlying={(data) => {
						setCenter(data)
						onLocationPinned(data)
					}}
				/>
			</MapContainer>
		</div>
	)
}

export default PinLokasi
