<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class DataVaksinModel
{
    public static function vaksin1($kodewil, $date_start, $date_end, $page)
    {
        if($kodewil == ''){
            $page--;
            //perpage 10 row
            $limit = 10;
            $offset = $page * 10;
            $data = DB::select(
                "SELECT 
                a.id, a.kode_wilayah, a.penyelenggara, a.penyelenggaraan_date, a.alamat, a.total, a.tahap, a.jenis_vaksin, 
                (SELECT username FROM users WHERE id = a.created_by) AS editor,
                (SELECT nama FROM wilayah_provinsi WHERE id=SUBSTRING(a.kode_wilayah,1,2) LIMIT 1) AS provinsi, 
                (SELECT nama FROM wilayah_kabupaten WHERE id=SUBSTRING(a.kode_wilayah,1,4) LIMIT 1) AS kabkot, 
                (SELECT nama FROM wilayah_kecamatan WHERE id=SUBSTRING(a.kode_wilayah,1,7) LIMIT 1) AS kecamatan,
                ST_X(Lokasi) AS Lat,
                ST_Y(Lokasi) AS Lng
                FROM vaksinasi_1 a 
                WHERE a.penyelenggaraan_date BETWEEN ? AND ? ORDER BY a.penyelenggaraan_date DESC LIMIT ?,?", 
                [$date_start, $date_end, $offset, $limit]);
        }else{
            if ($page == '') {
                $data = DB::select(
                    "SELECT 
                    a.id, a.kode_wilayah, a.penyelenggara, a.penyelenggaraan_date, a.alamat, a.total, a.tahap, a.jenis_vaksin, 
                    (SELECT username FROM users WHERE id = a.created_by) AS editor,
                    (SELECT nama FROM wilayah_provinsi WHERE id=SUBSTRING(a.kode_wilayah,1,2) LIMIT 1) AS provinsi, 
                    (SELECT nama FROM wilayah_kabupaten WHERE id=SUBSTRING(a.kode_wilayah,1,4) LIMIT 1) AS kabkot, 
                    (SELECT nama FROM wilayah_kecamatan WHERE id=SUBSTRING(a.kode_wilayah,1,7) LIMIT 1) AS kecamatan,
                    ST_X(Lokasi) AS Lat,
                    ST_Y(Lokasi) AS Lng
                    FROM vaksinasi_1 a 
                    WHERE a.kode_wilayah LIKE ? AND a.penyelenggaraan_date BETWEEN ? AND ? ORDER BY a.penyelenggaraan_date DESC", 
                    [$kodewil. "%", $date_start, $date_end, $offset, $limit]);
            } else {
                $page--;
                //perpage 10 row
                $limit = 10;
                $offset = $page * 10;
                $data = DB::select(
                    "SELECT 
                    a.id, a.kode_wilayah, a.penyelenggara, a.penyelenggaraan_date, a.alamat, a.total, a.tahap, a.jenis_vaksin, a.created_by,
                    (SELECT nama FROM wilayah_provinsi WHERE id=SUBSTRING(a.kode_wilayah,1,2) LIMIT 1) AS provinsi, 
                    (SELECT nama FROM wilayah_kabupaten WHERE id=SUBSTRING(a.kode_wilayah,1,4) LIMIT 1) AS kabkot, 
                    (SELECT nama FROM wilayah_kecamatan WHERE id=SUBSTRING(a.kode_wilayah,1,7) LIMIT 1) AS kecamatan,
                    ST_X(Lokasi) AS Lat,
                    ST_Y(Lokasi) AS Lng
                    FROM vaksinasi_1 a 
                    WHERE a.kode_wilayah LIKE ? AND a.penyelenggaraan_date BETWEEN ? AND ? ORDER BY a.penyelenggaraan_date DESC LIMIT ?,?", 
                    [$kodewil. "%", $date_start, $date_end, $offset, $limit]);
            }
        }


        return $data;
    }

    public static function addNew($data, $user)
    {

        return DB::table('vaksinasi_1')->insertGetId([
            'kode_wilayah' => $data->kodewil, 
            'penyelenggara' => $data->penyelenggara, 
            'penyelenggaraan_date' => date('Y-m-d', strtotime($data->penyelenggaraan_date)), 
            'alamat' => $data->alamat, 
            'total' => $data->total, 
            'tahap' => $data->tahap,
            'jenis_vaksin' =>  $data->jenis_vaksin,
            'created_by' => $user->id,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT($data->lng $data->lat)',1)")
        ]);
    }

    public static function editData($data, $user)
    {

        return DB::table('vaksinasi_1')->where('id', $data->id)->update([
            'kode_wilayah' => $data->kodewil, 
            'penyelenggara' => $data->penyelenggara, 
            'penyelenggaraan_date' => date('Y-m-d', strtotime($data->penyelenggaraan_date)), 
            'alamat' => $data->alamat, 
            'total' => $data->total, 
            'tahap' => $data->tahap,
            'jenis_vaksin' =>  $data->jenis_vaksin,
            'created_by' => $user->id,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT($data->lng $data->lat)',1)")
        ]);
    }
}