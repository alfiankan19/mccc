import React, { Suspense, useEffect, lazy, useState } from 'react'
import {
	Switch,
	Route,
	Redirect,
	useLocation,
	Link,
	useRouteMatch,
} from 'react-router-dom'

import Swal from 'sweetalert2'

import {
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	Input,
	Label,
	Textarea,
	ModalFooter,
} from '@windmill/react-ui'
import ThemedSuspense from '../components/ThemedSuspense'
import moment from 'moment'
import {
	addRS,
	addVaksinRecord,
	deleteRS,
	deleteVaksinRecord,
	editRS,
	editVaksinRecord,
	getLastUpdated,
} from '../data/repository'
import WilayahOption from '../components/wilayahoption/WilayahOption'
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/dark.css'
import PinLokasi from '../components/pinlokasi/PinLokasi'

const TableSiranap = lazy(() => import('../components/siranap/TableSiranap'))
const TableKamar = lazy(() => import('../components/siranap/TableKamar'))
const TableRiwayatKamar = lazy(() =>
	import('../components/siranap/TableRiwayatKamar')
)

function PortalDataSiranap() {
	let { path, url } = useRouteMatch()
	const [refreshtable, setRefreshtable] = useState(0)

	return (
		<div className='px-10 py-5'>
			<Suspense fallback={<ThemedSuspense />}>
				<Switch>
					<Route exact path={path}>
						<Suspense fallback={<ThemedSuspense />}>
							<TableSiranap refreshTable={refreshtable} />
						</Suspense>
					</Route>
					<Route path={`${path}/kamar/:rsid`}>
						<Suspense fallback={<ThemedSuspense />}>
							<TableKamar />
						</Suspense>
					</Route>
					<Route path={`${path}/riwayat/:rsid/:jnskmr`}>
						<Suspense fallback={<ThemedSuspense />}>
							<TableRiwayatKamar />
						</Suspense>
					</Route>
				</Switch>
			</Suspense>
		</div>
	)
}

export default PortalDataSiranap
