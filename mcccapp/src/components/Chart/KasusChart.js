import React, { useEffect, useRef, useState } from 'react'

import * as am4core from '@amcharts/amcharts4/core'
import * as am4charts from '@amcharts/amcharts4/charts'
import chart_dark from '@amcharts/amcharts4/themes/dark'
import chart_light from '@amcharts/amcharts4/themes/animated'
import PageTitle from '../../components/Typography/PageTitle'
import Loader from 'react-loader-spinner'
import { Select, Input, Label } from '@windmill/react-ui'
import {
	getWilayahKasusProvinsi,
	getChartJsonTerkonfirmasi,
} from '../../data/repository'

// eslint-disable-next-line react/prop-types
function KasusChart({ idchart }) {
	const chartRef = useRef(null)
	const [isLoading, setIsLoading] = useState(false)
	const [chartData, setChartData] = useState([])

	const [provinsidata, setProvinsidata] = useState([])
	const [whereProv, setWhereProv] = useState(11)

	const getProvinsiData = () => {
		setProvinsidata([])

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
			})
			.catch((err) => {
				getProvinsiData()
				//console.log(err)
			})
	}

	const getChartData = (provid) => {
		setIsLoading(true)
		getChartJsonTerkonfirmasi(provid)
			.then((result) => {
				//console.log('refresh', provid)
				setChartData(result.data.data)
				setIsLoading(false)
			})
			.catch((err) => {
				//console.log(err)
				setIsLoading(false)
			})
	}

	useEffect(() => {
		if (!chartRef.current) {
			if (localStorage.getItem('theme') === 'dark') {
				am4core.useTheme(chart_dark)
			} else {
				am4core.useTheme(chart_light)
			}

			let x = am4core.create(idchart, am4charts.XYChart)

			x.paddingRight = 20

			let data = chartData
			let visits = 10

			// for (let i = 1; i < 366; i++) {
			// 	visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10)
			// 	data.push({ date: new Date(2018, 0, i), name: 'name' + i, value: visits })
			// }

			x.data = data

			let dateAxis = x.xAxes.push(new am4charts.DateAxis())
			dateAxis.renderer.grid.template.location = 0

			let valueAxis = x.yAxes.push(new am4charts.ValueAxis())
			valueAxis.tooltip.disabled = true
			valueAxis.renderer.minWidth = 35

			let series = x.series.push(new am4charts.LineSeries())
			series.dataFields.dateX = 'date'
			series.dataFields.valueY = 'value'
			series.tooltipText = '{valueY.value}'
			x.cursor = new am4charts.XYCursor()

			let scrollbarX = new am4charts.XYChartScrollbar()
			scrollbarX.series.push(series)
			x.scrollbarX = scrollbarX

			chartRef.current = x
		}

		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		if (chartRef.current) {
			chartRef.current.data = chartData
		}
	}, [chartData])

	useEffect(() => {
		return () => {
			chartRef.current && chartRef.current.dispose()
		}
	}, [])

	useEffect(() => {
		getProvinsiData()
		getChartData('')
	}, [0])

	return (
		<div className='pt-12'>
			<div className='grid grid-col-2 grid-flow-col gap-1'>
				<div>
					<PageTitle>Kasus Terkonfirmasi</PageTitle>
				</div>
				<div>
					<div className='relative inline-block float-right text-gray-700 pt-5'>
						<Label className=' max-w-max'>
							<span>Pilih Provinsi</span>
							<Select
								onChange={(e) => {
									setWhereProv(e.target.value)
									getChartData(e.target.value)
								}}
								className='mt-1 border-solid border-2 rounded'
							>
								<option key={0} value=''>
									Semua
								</option>
								{provinsidata.map((v) => (
									<option key={v.idwil} value={v.idwil}>
										{v.nama}
									</option>
								))}
							</Select>
						</Label>
					</div>
				</div>
			</div>

			{isLoading ? (
				<center>
					<div className='pt-5'>
						<Loader type='Oval' color='#00BFFF' height={30} width={30} />
					</div>
				</center>
			) : null}
			<div
				id={idchart}
				style={{ width: '100%', height: '500px', borderRadius: 15 }}
			></div>
		</div>
	)
}

export default KasusChart
