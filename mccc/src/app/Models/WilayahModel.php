<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class WilayahModel
{
    public static function kabupaten($provid, $search = '')
    {
        if($search == ''){
            return DB::table('wilayah_kabupaten')->where('provinsi_id', $provid)->select(DB::raw('nama, id AS kpu_idkab'))->get();

        }else{
            return DB::table('tblwilayah')->where('provinsi_id', $provid)->where('nama', 'like', '%' . $search . '%')->select(DB::raw('nama, id AS kpu_idkab'))->get();

        }
    }

    public static function kabupatenAll()
    {
        return DB::select(DB::raw("SELECT kpu_idkab, kpu_idprov, kabkot nama, ST_X(CENTROID(SHAPE)) AS lng,ST_Y(CENTROID(SHAPE)) AS lat FROM data_kab GROUP BY kpu_idkab"));
    }

    public static function getProvOne($provid)
    {
        return DB::table('wilayah_provinsi')->where('id', $provid)->select(DB::raw('nama, id AS kpu_idprov'))->first();
    }
}