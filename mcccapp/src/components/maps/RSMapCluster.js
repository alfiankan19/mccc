import React, { useEffect, useState, useRef } from 'react'
import { getGeojsonRS, getGeojsonVaksinasi } from '../../data/repository'
import L from 'leaflet'
import { Marker, Popup, Tooltip, GeoJSON, useMap } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import { Button } from '@windmill/react-ui'
import './cluster.css'
import { Link, useRouteMatch } from 'react-router-dom'
function RSMapCluster() {
	const [layerVaksin, setLayerVaksin] = useState([])
	const layerRef = useRef(null)
	const parentMap = useMap()
	const { url } = useRouteMatch()
	const getLayer = () => {
		getGeojsonRS()
			.then((result) => {
				console.log(result.data.features[1].geometry.coordinates)
				setLayerVaksin(result.data.features)
			})
			.catch((err) => {
				console.log(err)
			})
	}

	useEffect(() => {
		getLayer()
	}, [0])

	function pointToLayer(feature, latlng) {
		let marker = L.circleMarker(latlng, {
			radius: 8,
			weight: 1,
			fillColor: 'orange',
			color: '#000',
			opacity: 1,
			fillOpacity: 0.8,
		})
		var markers = L.markerClusterGroup()
		markers.addLayer(marker)
		parentMap.addLayer(markers)
	}

	return (
		<MarkerClusterGroup chunkedLoading={true} maxClusterRadius={90}>
			{layerVaksin.map((val) => (
				<Marker
					position={[val.geometry.coordinates[1], val.geometry.coordinates[0]]}
					icon={L.divIcon({
						html: `<div class="bg-transparent marker-cluster-small leaflet-zoom-animated leaflet-interactive"><svg id="Capa_1" enable-background="new 0 0 512 512" height="30" viewBox="0 0 512 512" width="30" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m496.419 106.496h-480.838c-4.463 0-8.081-3.618-8.081-8.081v-16.163c0-4.463 3.618-8.081 8.081-8.081h480.837c4.463 0 8.081 3.618 8.081 8.081v16.163c.001 4.463-3.617 8.081-8.08 8.081z" fill="#62a4fb"/></g><g><path d="m496.419 74.171h-32.325c4.463 0 8.081 3.618 8.081 8.081v16.163c0 4.463-3.618 8.081-8.081 8.081h32.325c4.463 0 8.081-3.618 8.081-8.081v-16.163c0-4.463-3.618-8.081-8.081-8.081z" fill="#5392f9"/></g><g><path d="m23.663 106.496h464.675v365.679h-464.675z" fill="#e6f7fe"/></g><g><path d="m23.663 106.496v26.264h113.138c5.579 0 10.102 4.523 10.102 10.102v10.102c0 25.663 20.804 46.467 46.467 46.467h125.26c25.663 0 46.467-20.804 46.467-46.467v-10.102c0-5.579 4.523-10.102 10.102-10.102h76.772c5.579 0 10.102 4.523 10.102 10.102v329.313h26.264v-365.679z" fill="#d3effd"/></g><g><path d="m78.211 203.472h181.829v226.276h-181.829z" fill="#91dafa" transform="matrix(0 1 -1 0 485.736 147.484)"/></g><g><path d="m178.217 303.478h181.829v26.264h-181.829z" fill="#75cff9" transform="matrix(0 1 -1 0 585.742 47.478)"/></g><g><path d="m262.061 278.224h246.48v141.423h-246.48z" fill="#91dafa" transform="matrix(0 1 -1 0 734.236 -36.366)"/></g><g><path d="m319.64 335.803h246.48v26.264h-246.48z" fill="#75cff9" transform="matrix(0 1 -1 0 791.815 -93.945)"/></g><g><path d="m496.419 504.5h-480.838c-4.463 0-8.081-3.618-8.081-8.081v-16.163c0-4.463 3.618-8.081 8.081-8.081h480.837c4.463 0 8.081 3.618 8.081 8.081v16.163c.001 4.463-3.617 8.081-8.08 8.081z" fill="#62a4fb"/></g><g><path d="m496.419 472.175h-32.325c4.463 0 8.081 3.618 8.081 8.081v16.163c0 4.463-3.618 8.081-8.081 8.081h32.325c4.463 0 8.081-3.618 8.081-8.081v-16.163c0-4.463-3.618-8.081-8.081-8.081z" fill="#5392f9"/></g><g><path d="m173.167 152.963v-125.26c0-11.158 9.045-20.203 20.203-20.203h125.26c11.158 0 20.203 9.045 20.203 20.203v125.26c0 11.158-9.045 20.203-20.203 20.203h-125.26c-11.158.001-20.203-9.045-20.203-20.203z" fill="#e175a5"/></g><g><path d="m328.631 10.164c-11.345 29.436-30.78 59.479-57.456 86.154-30.09 30.09-64.464 50.971-97.419 61.413 2.146 8.853 10.101 15.436 19.614 15.436h125.26c11.158 0 20.203-9.045 20.203-20.203v-125.261c0-7.516-4.113-14.059-10.202-17.539z" fill="#de5791"/></g><g><path d="m306.508 74.171h-34.345v-34.346h-32.326v34.346h-34.345v32.325h34.345v34.345h32.326v-34.345h34.345z" fill="#e6f7fe"/></g><g><path d="m282.26 218.2h-226.27c-4.143 0-7.5 3.357-7.5 7.5v181.82c0 4.143 3.357 7.5 7.5 7.5h28.11c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-20.61v-166.82h211.27v166.82h-155.66c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5h163.16c4.143 0 7.5-3.357 7.5-7.5v-181.82c0-4.142-3.358-7.5-7.5-7.5z"/><path d="m346.915 366.536c4.143 0 7.5-3.357 7.5-7.5v-20.203c0-4.143-3.357-7.5-7.5-7.5s-7.5 3.357-7.5 7.5v20.203c0 4.143 3.357 7.5 7.5 7.5z"/><path d="m496.42 114c8.591 0 15.58-6.993 15.58-15.59v-16.16c0-8.591-6.989-15.58-15.58-15.58h-150.087v-38.967c0-15.275-12.428-27.703-27.703-27.703h-125.26c-15.275 0-27.703 12.428-27.703 27.703v38.968h-150.086c-8.592 0-15.581 6.989-15.581 15.581v16.163c0 8.592 6.989 15.581 15.581 15.581h.582v350.674h-.583c-8.591 0-15.58 6.993-15.58 15.59v16.16c0 8.591 6.989 15.58 15.58 15.58h480.84c8.591 0 15.58-6.989 15.58-15.58v-16.16c0-8.597-6.989-15.59-15.58-15.59h-.583v-350.67zm-315.753-86.297c0-7.005 5.698-12.703 12.703-12.703h125.26c7.005 0 12.703 5.698 12.703 12.703v125.261c0 7.005-5.698 12.703-12.703 12.703h-125.26c-7.005 0-12.703-5.698-12.703-12.703zm-165.667 70.712v-16.163c0-.32.261-.581.581-.581h150.086v17.325h-150.086c-.32 0-.581-.261-.581-.581zm482 381.845v16.16c0 .32-.26.58-.58.58h-480.84c-.32 0-.58-.26-.58-.58v-16.16c0-.325.26-.59.58-.59h480.84c.32 0 .58.265.58.59zm-48.488-15.59h-126.422v-231.475h126.422zm32.325 0h-17.325v-238.975c0-4.143-3.357-7.5-7.5-7.5h-141.422c-4.143 0-7.5 3.357-7.5 7.5v238.975h-275.927v-350.674h134.504v38.968c0 15.275 12.428 27.703 27.703 27.703h125.26c15.275 0 27.703-12.428 27.703-27.703v-38.964h71.637c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-71.637v-17.33h150.087c.32 0 .58.26.58.58v16.16c0 .325-.26.59-.58.59h-43.45c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5h27.867z"/><path d="m205.492 113.996h26.845v26.846c0 4.143 3.357 7.5 7.5 7.5h32.326c4.143 0 7.5-3.357 7.5-7.5v-26.846h26.845c4.143 0 7.5-3.357 7.5-7.5v-32.325c0-4.143-3.357-7.5-7.5-7.5h-26.845v-26.846c0-4.143-3.357-7.5-7.5-7.5h-32.326c-4.143 0-7.5 3.357-7.5 7.5v26.846h-26.845c-4.143 0-7.5 3.357-7.5 7.5v32.325c0 4.143 3.358 7.5 7.5 7.5zm7.5-32.325h26.845c4.143 0 7.5-3.357 7.5-7.5v-26.846h17.326v26.846c0 4.143 3.357 7.5 7.5 7.5h26.845v17.325h-26.845c-4.143 0-7.5 3.357-7.5 7.5v26.846h-17.326v-26.846c0-4.143-3.357-7.5-7.5-7.5h-26.845z"/><path d="m418.383 274.753c1.465 1.464 3.385 2.196 5.304 2.196s3.839-.732 5.304-2.196c2.929-2.93 2.929-7.678 0-10.607l-11.429-11.429c-2.93-2.928-7.678-2.928-10.607 0-2.929 2.93-2.929 7.678 0 10.607z"/><path d="m249.939 276.949c1.919 0 3.839-.732 5.304-2.196 2.929-2.93 2.929-7.678 0-10.607l-11.429-11.429c-2.93-2.928-7.678-2.928-10.607 0-2.929 2.93-2.929 7.678 0 10.607l11.429 11.429c1.465 1.464 3.385 2.196 5.303 2.196z"/><path d="m204.635 269.859c-2.929 2.93-2.929 7.678 0 10.607l22.857 22.857c1.465 1.464 3.385 2.196 5.304 2.196s3.839-.732 5.304-2.196c2.929-2.93 2.929-7.678 0-10.607l-22.857-22.857c-2.931-2.927-7.679-2.927-10.608 0z"/></g></g></svg></div>`,
						className: 'marker-cluster bg-transparent',
						iconSize: L.point(40, 40, true),
					})}
				>
					<Popup>
						<table className='table-auto'>
							<tbody>
								<tr>
									<td className='font-bold pt-2'>Nama RS&nbsp;&nbsp;</td>
									<td className='pt-2'>{val.properties.nama}</td>
								</tr>
								<tr>
									<td className='font-bold pt-2'>Alamat&nbsp;&nbsp;</td>
									<td className='pt-2'>{val.properties.alamat}</td>
								</tr>
								<tr>
									<td className='font-bold pt-3'>
										<Link to={`/app/siranap/kamar/${val.id}`}>
											<button className='bg-red-700 hover:bg-red-900 text-white text-sm py-1 px-2 rounded'>
												Cek Siranap
											</button>
										</Link>
									</td>
								</tr>
							</tbody>
						</table>
					</Popup>
				</Marker>
			))}
		</MarkerClusterGroup>
	)
}

export default RSMapCluster
