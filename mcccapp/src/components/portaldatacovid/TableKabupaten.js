import React, { useState, useEffect } from 'react'
import {
	Table,
	TableHeader,
	TableCell,
	TableBody,
	TableRow,
	TableFooter,
	TableContainer,
	Button,
	Pagination,
} from '@windmill/react-ui'
import { getDataCovidKabupaten } from '../../data/repository'
import { EditIcon } from '../../icons'
import { Link } from 'react-router-dom'

function TableKabupaten({ whereProv, searchKey }) {
	const [pageTable2, setPageTable2] = useState(1)

	const [dataTable2, setDataTable2] = useState([])
	const [totalResults, settotalResults] = useState(0)

	const resultsPerPage = 10

	function onPageChangeTable2(p) {
		setPageTable2(p)
	}

	const getDataCovid = (provid = 0) => {
		getDataCovidKabupaten(provid === 0 ? whereProv : provid, searchKey)
			.then((result) => {
				settotalResults(result.data.length)
				setDataTable2(
					result.data.slice(
						(pageTable2 - 1) * resultsPerPage,
						pageTable2 * resultsPerPage
					)
				)
			})
			.catch((err) => console.log(err))

		//console.log(dataTable2)
	}

	useEffect(() => {
		getDataCovid()
	}, [pageTable2, searchKey, whereProv])

	return (
		<>
			<TableContainer className='mb-8'>
				<Table>
					<TableHeader>
						<tr>
							<TableCell>Kabupaten</TableCell>
							<TableCell>Terkonfirmasi</TableCell>
							<TableCell>Aktif</TableCell>
							<TableCell>Sembuh</TableCell>
							<TableCell>Meninggal</TableCell>
							<TableCell>Actions</TableCell>
						</tr>
					</TableHeader>
					<TableBody>
						{dataTable2.map((kasus, i) => (
							<TableRow key={i}>
								<TableCell>
									<span className='text-sm'>{kasus.nama}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.terkonfirmasi}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>
										{Number(kasus.terkonfirmasi) -
											(Number(kasus.sembuh) + Number(kasus.meninggal)) <
										0
											? 0
											: Number(kasus.terkonfirmasi) -
											  (Number(kasus.sembuh) + Number(kasus.meninggal))}
									</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.sembuh}</span>
								</TableCell>
								<TableCell>
									<span className='text-sm'>{kasus.meninggal}</span>
								</TableCell>
								<TableCell>
									<div className='flex items-center space-x-4'>
										<Button
											tag={Link}
											to={`covid19/detail/${kasus.kpu_idkab}`}
											size='small'
											aria-label='Detail'
										>
											Detail
										</Button>
									</div>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<TableFooter>
					<Pagination
						totalResults={totalResults}
						resultsPerPage={resultsPerPage}
						onChange={onPageChangeTable2}
						label='Table navigation'
					/>
				</TableFooter>
			</TableContainer>
		</>
	)
}

export default TableKabupaten
