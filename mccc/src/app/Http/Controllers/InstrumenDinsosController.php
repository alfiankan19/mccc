<?php

namespace App\Http\Controllers;

use Validator;

use App\Models\User;
use App\Models\InstrumenDinsos;
use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;

class InstrumenDinsosController extends ApiResponse
{

    public function collectSurveyPenerimaBantuan(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'data' => 'required|string',
            ]);
    
            if ($validator->fails()) {
    
                return response()->json(array(
                    "success" => FALSE,
                    "message" => "Error",
                    "data" => $validator->errors()
                ),422);
            }

            $user = Auth::user();
            //cek id exist
            $is_exist = InstrumenDinsos::ifExist($user->id);

            if($is_exist == NULL){
                //create empty record
                InstrumenDinsos::newRecord($user->id);
            }

            $dinsos = InstrumenDinsos::updateRecord(json_decode($request->data), $user->id);


            return response()->json(array(
                "success" => TRUE,
                "message" => "add data dinsos",
                "data" => $dinsos
            ),200);



        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }

    public function getInstrumenDinsos(Request $request)
    {
        try {

            $user = Auth::user();
            $dinsos = InstrumenDinsos::getRecord($user->id);


            return response()->json(array(
                "success" => TRUE,
                "message" => "add data dinsos",
                "data" => $dinsos
            ),200);



        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }

}