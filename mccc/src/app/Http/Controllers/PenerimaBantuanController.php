<?php

namespace App\Http\Controllers;

use Validator;

use App\Models\User;
use App\Models\PenerimaBantuan;
use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;

class PenerimaBantuanController extends ApiResponse
{

    public function getAll(Request $request)
    {
        $user = Auth::user();
        try {
            $responden = PenerimaBantuan::getAll(
                $user->id, 
                $request->query('page', '0'),
                $request->query('search', '')
            );
            return response()->json(array(
                "success" => TRUE,
                "message" => "get survey list",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }

    public function getOne($id)
    {
        $user = Auth::user();
        try {
            $responden = PenerimaBantuan::getOne($id);
            return response()->json(array(
                "success" => TRUE,
                "message" => "get survey single",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }


    public function deleteOne($id)
    {
        $user = Auth::user();
        try {
            $responden = PenerimaBantuan::deleteOne($id);
            return response()->json(array(
                "success" => TRUE,
                "message" => "Berhasil menghapus",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }

    public function collectSurveyPenerimaBantuan(Request $request)
    {
        try{
            $tipe = "penerimabantuan";
            $file1 = $request->file('foto1');
            $subname1 = md5(time()."_".$file1->getClientOriginalName()).".".$file1->getClientOriginalExtension();
            $file1->move('./upload/foto',$subname1);
        } catch (\Throwable $th) {
            $subname1 = "";
        }

        try{
            $file2 = $request->file('foto2');
            $subname2 = md5(time()."_".$file2->getClientOriginalName()).".".$file2->getClientOriginalExtension();
            $file2->move('./upload/foto',$subname2);
        } catch (\Throwable $th) {
            $subname2 = "";
        }

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'data' => 'required|string',
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                "success" => FALSE,
                "message" => "Error",
                "data" => $validator->errors()
            ),422);
        }
      

        //create survey
        try {
            $responden = PenerimaBantuan::newSurveyPenerimaBantuan(json_decode($request->data), $user->id, $subname1, $subname2);
            return response()->json(array(
                "success" => TRUE,
                "message" => "get Instrumen Form",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }

    }


    public function updateSurveyPenerimaBantuan(Request $request)
    {

        try{
            $tipe = "penerimabantuan";
            $file1 = $request->file('foto1');
            $subname1 = md5(time()."_".$file1->getClientOriginalName()).".".$file1->getClientOriginalExtension();
            $file1->move('./upload/foto',$subname1);
        } catch (\Throwable $th) {
            $subname1 = "";
        }

        try{
            $file2 = $request->file('foto2');
            $subname2 = md5(time()."_".$file2->getClientOriginalName()).".".$file2->getClientOriginalExtension();
            $file2->move('./upload/foto',$subname2);
        } catch (\Throwable $th) {
            $subname2 = "";
        }


        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'data' => 'required|string',
            'id' => 'required|string'
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                "success" => FALSE,
                "message" => "Error",
                "data" => $validator->errors()
            ),422);
        }
      

        //create survey
        try {
            $responden = PenerimaBantuan::editSurveyPenerimaBantuan(json_decode($request->data), $request->id, $subname1, $subname2);
            return response()->json(array(
                "success" => TRUE,
                "message" => "update Instrumen Form",
                "data" => $responden
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "update execution err",
                "data" => $th
            ),500);
        }

    }

}