<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ApisController;
use App\Http\Controllers\PenerimaBantuanController;
use App\Http\Controllers\GeojsonLayer;
use App\Http\Controllers\InstrumenDinsosController;
use App\Http\Controllers\DataKasus;
use App\Http\Controllers\DataVaksin;
use App\Http\Controllers\Siranap;
use App\Http\Controllers\WilayahController;
use App\Http\Controllers\PemakamanData;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/warning', function () {
    return response()->json(array(
        "success" => FALSE,
        "message" => "Forbidden",
        "data" => array("Unauthorized" => TRUE)
      ),401);
})->name('login');

Route::middleware(['api'])->group(function () {
    Route::post('/user/login', [AuthController::class, 'login']);
    Route::post('/user/register', [AuthController::class, 'register']);
});

//WITH AUTH
Route::middleware(['auth:api'])->group(function () {
    Route::post('/user/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user', [AuthController::class, 'userProfile']);
    Route::post('/user/password', [AuthController::class, 'changePassword']);
    Route::post('/user/foto', [AuthController::class, 'uploadFotoUser']);
    Route::put('/user', [AuthController::class, 'updateProfileUser']);

    Route::post('/series/covid', [DataKasus::class, 'uploadDataSeries']);
    Route::post('/series/covid/updatesingle', [DataKasus::class, 'updateSingleData']);


    // VAKSIn
    Route::post('/data/vaksin', [DataVaksin::class, 'addData']);
    Route::post('/data/vaksin/edit', [DataVaksin::class, 'editData']);
    Route::post('/data/vaksin/delete', [DataVaksin::class, 'deleteData']);

    // Rumah Sakit
    Route::post('/data/siranap/rs/add', [Siranap::class, 'addRS']);
    Route::post('/data/siranap/rs/edit', [Siranap::class, 'editRS']);
    Route::post('/data/siranap/rs/delete', [Siranap::class, 'deleteRS']);
    Route::post('/data/siranap/rs/addkamar', [Siranap::class, 'addKamarRS']);
    Route::post('/data/siranap/rs/updatekamar', [Siranap::class, 'updateKamarRS']);

    

    Route::post('/data/siranap/jeniskamar/add', [Siranap::class, 'addJenisKamar']);
    Route::post('/data/siranap/jeniskamar/edit', [Siranap::class, 'editJenisKamar']);
    
});





// NO AUTH
Route::middleware(['api'])->group(function () {
    Route::get('/geojson/provinsi', [GeojsonLayer::class, 'getlayerprov']);

    //$dimension,/$zoom,/$type=1,/$crop="0x0"
    Route::get('/geojson/layer', [GeojsonLayer::class, 'getBatasDesa']);
    Route::get('/geojson/coba', [GeojsonLayer::class, 'coba']);


    Route::get('/wilayahkasus/provinsi', [DataKasus::class, 'kasusprovinsi']);


    Route::get('/wilayah/kabupaten/{idprov}', [WilayahController::class, 'kabupaten']);
    Route::get('/wilayah/kecamatan/{idkab}', [WilayahController::class, 'kecamatan']);
    Route::get('/wilayah/desa/{idkec}', [WilayahController::class, 'desa']);

    Route::get('/geojson/{tingkat}/{kodewil}', [GeojsonLayer::class, 'singleLayer']);


    Route::get('/covid/kabupaten/{provid}/excell', [DataKasus::class, 'datakabupatenExcell']);
    Route::get('/covid/kabupaten/{provid}/excelljson', [DataKasus::class, 'datakabupatenExcellJson']);

    Route::get('/covid/kabupaten/{provid}/json', [DataKasus::class, 'datakabupaten']);

    Route::get('/covid/kabupaten/{provid}/{search}/json', [DataKasus::class, 'datakabupaten']);

    Route::get('/covid/series/kabupaten/{kabid}/{dateStart}/{dateEnd}/json', [DataKasus::class, 'getDataSeriesKabupaten']);
    Route::get('/log/lastupdate', [DataKasus::class, 'getLastUpdate']);

    Route::get('/covid/kabupaten/json', [DataKasus::class, 'datakabupatenAll']);

    Route::get('/covid/summary/json', [DataKasus::class, 'dataSummary']);

    Route::get('/covid/provinsi/series/json', [DataKasus::class, 'seriesProvinsi']);

    Route::get('/covid/terkonfirmasi/series/{provid}/json', [DataKasus::class, 'seriesProvinsiTerkonfirmasi']);
    Route::get('/covid/sembuh/series/{provid}/json', [DataKasus::class, 'seriesProvinsiSembuh']);
    Route::get('/covid/meninggal/series/{provid}/json', [DataKasus::class, 'seriesProvinsiMeninggal']);

    Route::get('/covid/terkonfirmasi/provinsi/series/json', [DataKasus::class, 'seriesProvinsiTerkonfirmasi']);
    Route::get('/covid/sembuh/provinsi/series/json', [DataKasus::class, 'seriesProvinsiSembuh']);
    Route::get('/covid/meninggal/provinsi/series/json', [DataKasus::class, 'seriesProvinsiMeninggal']);

    Route::get('/covid/stacked/provinsi/series/json', [DataKasus::class, 'stackedCovidSeries']);
    Route::get('/covid/stacked/series/{provid}/json', [DataKasus::class, 'stackedCovidSeries']);

    // DATA VAKSIN
    Route::get('/data/vaksin/{kodewil}/json', [DataVaksin::class, 'datavaksin1']);
    Route::get('/data/vaksin/json', [DataVaksin::class, 'datavaksin1All']);
    Route::get('/data/vaksin/series', [DataVaksin::class, 'seriesPertumbuhanVaksinasi']);

    // TOTAL AKMULASI VAKSIN
   // Route:get('/data/vaksin/total/{tahap}', [DataVaksin::class, 'akumulasiData']);


    //Route::get('/covid/kabupaten/{provid}/{search}/json', [DataKasus::class, 'datakabupaten']);

    // KODE WILAYAH TO STRING
    Route::get('/extractor/wilayah/{kode}', [WilayahController::class, 'wilayahResolver']);

    // GET GEOJSON LOKASI VAKSIN
    Route::get('/geojson/vaksinasi', [GeojsonLayer::class, 'getlayervaksinasi']);
    Route::get('/geojson/rs', [GeojsonLayer::class, 'getlayerrs']);

    //SIRANAP
    Route::get('/data/siranap/rs', [Siranap::class, 'getRS']);
    
    Route::get('/data/siranap/jeniskamar', [Siranap::class, 'getJenisKamar']);
    Route::get('/data/siranap/jeniskamar/{rsid}', [Siranap::class, 'getJenisKamarByRS']);

    Route::get('/data/siranap/rs/info/{id}', [Siranap::class, 'infoRS']);
    Route::get('/data/siranap/kamar/info/{rsid}/{jsnkmr}', [Siranap::class, 'infoKamarRS']);
    Route::get('/data/siranap/kamar/riwayat/{rsid}/{jsnkmr}', [Siranap::class, 'riwayatKamarRS']);

    // KAMAR SIRANAP
    Route::get('/data/siranap/kamar/{rsid}', [Siranap::class, 'getKamarByRS']);


    //SIRANAP FRONT
    Route::get('/data/siranap/rsfront/{kodewil}', [Siranap::class, 'getRSbyKodewil']);




    // pemakaman
    Route::post('/data/pemakaman', [PemakamanData::class, 'addPemakamn']);
    Route::post('/data/pemakaman/edit', [PemakamanData::class, 'editPemakamn']);
    Route::post('/data/pemakaman/delete', [PemakamanData::class, 'deletePemakamn']);
    Route::get('/data/pemakaman/table', [PemakamanData::class, 'getPemakamanData']);

    
});
Route::get('/geojson/pemakaman', [GeojsonLayer::class, 'getLayerPemakaman']);