import React, { useState, useEffect } from 'react'
import TableDataVaksin from '../../../components/portaldatavaksin/TableDataVaksin'
import SectionTitle from '../../../components/Typography/SectionTitle'
import { Select, Input, Label } from '@windmill/react-ui'
import {
	TableContainer,
	Button,
	Modal,
	ModalBody,
	ModalHeader,
	ModalFooter,
} from '@windmill/react-ui'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
	faEye,
	faEyeSlash,
	faSync,
	faFilter,
} from '@fortawesome/free-solid-svg-icons'
import DataVaksinFilter from '../../../components/wilayahoption/DataVaksinFilter'
import moment from 'moment'

function TableVaksinHead({ onEditClicked, onDeleteClicked, refreshTable }) {
	const [whereProv, setWhereProv] = useState(11)
	const [searchKey, setSearchKey] = useState('')
	const [isModalOpen, setIsModalOpen] = useState(false)
	const [kodewil, setKodewil] = useState('')
	const [tanggalStart, setTanggalStart] = useState('')
	const [tanggalEnd, setTanggalEnd] = useState('')

	const closeModal = () => {
		setIsModalOpen(false)
	}
	const openModal = () => {
		setIsModalOpen(true)
	}

	const notZero = (value, index, self) => {
		return value !== '00'
	}

	return (
		<>
			{/* <SectionTitle>
				Data Vaksin {kodewil.slice(0, 4).filter(notZero).reverse()[0]}
				{' | '}
				{kodewil[4] ? kodewil[4] : '-'}
			</SectionTitle> */}
			<div className='grid grid-cols-2'>
				<div>
					<div className='py-3'>
						<div className='grid grid-rows-2'>
							<div className='text-white'>
								{kodewil}
								{' | '}
								{tanggalStart} | {tanggalEnd}
							</div>
							<div>
								<Button onClick={openModal} size='small' aria-label='Detail'>
									<FontAwesomeIcon color='white' size='sm' icon={faFilter} />
									&nbsp; Filter
								</Button>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div className='py-3 float-right'>
						{/* <Label className=' max-w-max'>
							<span>Cari penyelenggara</span>
							<Input
								placeholder='ketik lalu enter'
								onKeyDown={(e) =>
									e.key === 'Enter' ? setSearchKey(e.target.value) : null
								}
								className='mt-1 border-solid border-2 rounded'
							/>
						</Label> */}
					</div>
				</div>
			</div>
			<TableDataVaksin
				refreshTable={refreshTable}
				whereProv={whereProv}
				searchKey={searchKey}
				kodewil={kodewil}
				dateStarted={tanggalStart}
				dateEnded={tanggalEnd}
				onEditClicked={(v) => {
					onEditClicked(v)
				}}
				onDeleteClicked={(v) => onDeleteClicked(v)}
			/>
			<DataVaksinFilter
				onFilterSaved={(kodewil) => {
					setKodewil(kodewil.slice(0, 4).filter(notZero).reverse()[0])
					const dateRange = kodewil[4].split(' to ')
					////console.log(kodewil[4], dateRange)
					setTanggalStart(dateRange[0])
					setTanggalEnd(dateRange[1])
				}}
				isModalOpen={isModalOpen}
				onCloseModal={() => setIsModalOpen(false)}
			/>
		</>
	)
}

export default TableVaksinHead
