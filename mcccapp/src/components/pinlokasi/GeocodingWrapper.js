import React, { useEffect } from 'react'
import Loader from 'react-loader-spinner'
import GeocodingAutoComplete from './GeocodingAutoComplete'
import { useMap, GeoJSON } from 'react-leaflet'
import L from 'leaflet'
const POSITION_CLASSES = {
	bottomleft: 'leaflet-bottom leaflet-left',
	bottomright: 'leaflet-bottom leaflet-right',
	topleft: 'leaflet-top leaflet-left',
	topright: 'leaflet-top leaflet-right',
}
function GeocodingWrapper({ position, zoom, isFlying }) {
	const positionClass =
		(position && POSITION_CLASSES[position]) || POSITION_CLASSES.topright
	const parentMap = useMap()

	const goToLocation = (coordinate) => {
		parentMap.flyTo([coordinate[1], coordinate[0]], 18)
		isFlying([coordinate[1], coordinate[0]])
	}

	return (
		<div className={positionClass}>
			<div className='leaflet-control leaflet-bar bg-black'>
				<GeocodingAutoComplete
					onSelect={(place) => goToLocation(place.geometry.coordinates)}
				/>
			</div>
		</div>
	)
}

export default GeocodingWrapper
