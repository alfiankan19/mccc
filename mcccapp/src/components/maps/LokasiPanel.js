import React, { useEffect, useState } from 'react'
import Loader from 'react-loader-spinner'
import { useMap } from 'react-leaflet'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash, faSync } from '@fortawesome/free-solid-svg-icons'
import { Button } from '@windmill/react-ui'
import axios from 'axios'
import L from 'leaflet'
import { base_url } from '../../data/service'
import './hidescroll.css'
import { getWilayahKasusProvinsi } from '../../data/repository'

const POSITION_CLASSES = {
	bottomleft: 'leaflet-bottom leaflet-left',
	bottomright: 'leaflet-bottom leaflet-right',
	topleft: 'leaflet-top leaflet-left',
	topright: 'leaflet-top leaflet-right',
}
function LokasiPanel({ position, zoom, setMapScrollable }) {
	const parentMap = useMap()
	const [showPanel, setShowPanel] = useState(false)
	const [dataWilayah, setDataWilayah] = useState([])
	const [selected, setSelected] = useState(0.2)
	const [isError, setIsError] = useState(false)

	const getData = () => {
		setIsError(false)
		setDataWilayah([])

		getWilayahKasusProvinsi()
			.then((result) => {
				setDataWilayah(result.data)
				setIsError(false)
			})
			.catch((err) => {
				setIsError(true)
				//console.log(err)
			})
	}

	useEffect(() => {
		setTimeout(function () {
			getData()
		}, 3000)
	}, [0])

	function zoomToFeature(idwil, i) {
		var config = {
			method: 'get',
			url: base_url + `geojson/4/${idwil}`,
		}

		axios(config)
			.then(function (response) {
				parentMap.flyToBounds(L.geoJSON(response.data).getBounds())
				setSelected(i)

				// parentMap.eachLayer(function (layer) {

				//   if(layer.feature != undefined){
				//     //console.log('oke')
				//     //console.log(layer.feature.properties)
				//     if(layer.feature.properties.idwil == idwil) {
				//       layer.setStyle({fillColor :'green'})
				//     }
				//   }

				// });
			})
			.catch(function (error) {
				//console.log(error)
			})

		// //console.log(e.sourceTarget._bounds);
		// parentMap.setZoom(9)
		// parentMap.fitBounds(e.sourceTarget._bounds)
	}

	const positionClass =
		(position && POSITION_CLASSES[position]) || POSITION_CLASSES.topright
	return (
		<div className={positionClass}>
			<div className='leaflet-control leaflet-bar bg-black bg-opacity-75'>
				<div className='grid grid-col-2 grid-flow-col gap-1'>
					<div>
						<button
							onClick={() => {
								showPanel ? setShowPanel(false) : setShowPanel(true)
							}}
							className='w-full bg-red-500 hover:bg-blue-700 text-white font-bold px-4 py-2 rounded'
						>
							{showPanel ? (
								<span>
									<FontAwesomeIcon color='white' size='lg' icon={faEyeSlash} />{' '}
									Hide
								</span>
							) : (
								<span>
									<FontAwesomeIcon color='white' size='lg' icon={faEye} /> Panel
									Wilayah
								</span>
							)}{' '}
						</button>
					</div>
					<div>
						<button
							onClick={() => {
								parentMap.flyTo([-2.548926, 118.0148634], 5)
							}}
							className='w-full bg-red-500 hover:bg-blue-700 text-white font-bold px-4 py-2 rounded'
						>
							{showPanel ? (
								<span>
									<FontAwesomeIcon color='white' size='lg' icon={faSync} />{' '}
									Reset
								</span>
							) : (
								<span>
									<FontAwesomeIcon color='white' size='lg' icon={faSync} />{' '}
									Reset
								</span>
							)}{' '}
						</button>
					</div>
				</div>

				<div
					className='panelwilayah'
					style={showPanel ? { display: 'block' } : { display: 'none' }}
				>
					<div className='p-2 relative mx-auto text-gray-600'>
						{/* <input
							className='border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none'
							type='search'
							name='search'
							placeholder='Search'
						/> */}
					</div>
					<div
						onMouseOver={() => {
							parentMap.scrollWheelZoom.disable()
						}}
						onMouseOut={() => {
							parentMap.scrollWheelZoom.enable()
						}}
						className='col-md-6 bar3 text-white'
						style={{ height: '300px', maxHeight: '100%', overflowY: 'auto' }}
					>
						{dataWilayah.length === 0 && isError === false ? (
							<center>
								<div className='pt-5'>
									<Loader type='Oval' color='#00BFFF' height={30} width={30} />
								</div>
							</center>
						) : null}
						{isError === true ? (
							<center>
								<Button onClick={getData} layout='link' size='small'>
									<FontAwesomeIcon color='white' size='lg' icon={faSync} />
									&nbsp;Retry
								</Button>
							</center>
						) : null}
						<ul className='p-5'>
							{dataWilayah.map((v, i) => (
								<li
									onClick={() => {
										zoomToFeature(v.idwil, i)
									}}
									key={i}
									style={{ marginTop: '5px' }}
									className={`p-2 cursor-pointer hover:bg-blue-700 ${
										selected === i ? 'bg-blue-700' : ''
									}`}
								>
									<div className='grid grid-rows-2 grid-flow-col gap-1'>
										<div>
											<span className='font-bold'>{v.nama} </span>
										</div>
										{/* <div>
											<span>Kasus : {v.kasus}</span>
										</div> */}
									</div>
								</li>
							))}
						</ul>
					</div>
				</div>
			</div>
		</div>
	)
}

export default LokasiPanel
