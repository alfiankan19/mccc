import React, { useEffect, useState } from 'react'
import { Label, Select } from '@windmill/react-ui'
import { Button } from '@windmill/react-ui'
import { base_url } from '../../data/service'
import { Modal, ModalHeader, ModalBody, ModalFooter } from '@windmill/react-ui'
import { getWilayahKasusProvinsi } from '../../data/repository'

function DownloadTemplate() {
	const [provinsidata, setProvinsidata] = useState([])
	const [selectedProvinsi, setSelectedProvinsi] = useState(0)
	const [isModalOpen, setIsModalOpen] = useState(false)

	function openModal() {
		setIsModalOpen(true)
	}

	function closeModal() {
		setIsModalOpen(false)
	}

	const downloadTemplate = () => {
		if (selectedProvinsi !== 0) {
			window.open(
				base_url + `covid/kabupaten/${selectedProvinsi}/excell`,
				'_blank'
			)
		} else {
			openModal()
		}
	}

	const getProvinsiData = () => {
		setProvinsidata([])

		getWilayahKasusProvinsi()
			.then((result) => {
				setProvinsidata(result.data)
			})
			.catch((err) => {
				getProvinsiData()
				//console.log(err)
			})
	}

	useEffect(() => {
		getProvinsiData()
	}, [0])

	return (
		<div className='px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800 text-gray-700 dark:text-gray-200'>
			<div className='p-5'>
				<Label className=' max-w-max'>
					<span>Pilih Provinsi</span>
					<Select
						className='mt-1 border-solid border-2 rounded'
						onChange={(e) => setSelectedProvinsi(e.target.value)}
					>
						<option key={0} value={0}>
							{' '}
							- Pilih -{' '}
						</option>
						{provinsidata.map((v) => (
							<option key={v.idwil} value={v.idwil}>
								{v.nama}
							</option>
						))}
					</Select>
				</Label>
			</div>
			<div className='px-5 mb-2'>
				<Button onClick={downloadTemplate} size='small'>
					Download
				</Button>
			</div>
			<Modal isOpen={isModalOpen} onClose={closeModal}>
				<ModalHeader>Gagal</ModalHeader>
				<ModalBody>Pilih Provinsi Terlebih Dahulu</ModalBody>
				<ModalFooter>
					<div className='hidden sm:block'>
						<Button onClick={closeModal}>Oke</Button>
					</div>

					<div className='block w-full sm:hidden'>
						<Button block size='large' onClick={closeModal}>
							Oke
						</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	)
}

export default DownloadTemplate
