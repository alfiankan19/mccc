import React, { useState } from 'react'
import { Button } from '@windmill/react-ui'
import PageTitle from '../components/Typography/PageTitle'
import SectionTitle from '../components/Typography/SectionTitle'
import StepWizard from 'react-step-wizard'

import response from '../utils/demo/tableData'
import DownloadTemplate from '../components/uploader/DownloadTemplate'
import UploadExcell from '../components/uploader/UploadExcell'
import { Link } from 'react-router-dom'

function PortalDataCovidUploader() {
	return (
		<div className='px-10 py-5'>
			<PageTitle>Portal Upload Data Covid</PageTitle>

			<a
				className='flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple'
				href='/#'
			>
				<div className='flex items-center'>
					<span>Terakhir data di update : {new Date().toDateString()}</span>
				</div>
			</a>

			<SectionTitle>Download Excell Template</SectionTitle>
			<div className='p-1s'>
				<DownloadTemplate />
			</div>
			<SectionTitle>Upload Excell</SectionTitle>
			<UploadExcell />
		</div>
	)
}

export default PortalDataCovidUploader
