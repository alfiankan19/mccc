<?php
namespace App\Http\Controllers;

class ApiResponse extends Controller {


  public function json_success($data=array(),$message="",$code=200)
  {
    return response()->json(array(
      "success" => TRUE,
      "message" => $message,
      "data" => $data
    ),$code);

  }

  public function json_fail($data=array(),$message="",$code=401)
  {
    return response()->json(array(
      "success" => FALSE,
      "message" => $message,
      "data" => $data
    ),$code);  
  }

  public function json_error($data=array(),$message="",$code=500)
  {
    return response()->json(array(
      "success" => FALSE,
      "message" => $message,
      "data" => $data
    ),$code);  
  }

}