import { createSlice } from '@reduxjs/toolkit'

export const slice = createSlice({
	name: 'covid',
	initialState: {
		kodewil: 3213,
		dateRange: '',
	},
	reducers: {
		setKodeWil: (state, action) => {
			state.kodewil = action.payload
		},
		setDateRange: (state, action) => {
			state.dateRange = action.payload
		},
	},
})

export const { setKodeWil, setDateRange } = slice.actions

export const kodewil = (state) => state.covid.kodewil
export const dateRange = (state) => state.covid.dateRange

export default slice.reducer
