<?php

namespace App\Http\Controllers;

use Validator;


use App\Models\Respondent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiResponse;


use App\Models\User;


use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DataKabupaten;
use Illuminate\Support\Facades\DB;


class PemakamanData extends Controller
{

    public function addPemakamn(Request $request)
    {


        DB::table('lokasi_meninggal')->insertGetId([
            'nama' => $request->nama,
            'note'=> $request->note,
            'kodewil'=> $request->kodewil,
            'alamat'=> $request->alamat,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT(".$request->lat." ".$request->lng.")')"),
        ]);
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil ",
            "data" => "Berhasil"
        ),200); 
    }

    public function deletePemakamn(Request $request)
    {


        DB::table('lokasi_meninggal')->where('id', $request->id)->delete();
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil ",
            "data" => "Berhasil"
        ),200); 
    }
    
    public function editPemakamn(Request $request)
    {


        DB::table('lokasi_meninggal')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'note'=> $request->note,
            'kodewil'=> $request->kodewil,
            'alamat'=> $request->alamat,
            'Lokasi' => DB::raw("ST_GeomFromText('POINT(".$request->lat." ".$request->lng.")')"),
        ]);
        return response()->json(array(
            "success" => TRUE,
            "message" => "Berhasil UPDATE",
            "data" => $request->id
        ),200); 
    }
    public function getPemakamanData(Request $request)
    {
        // https://reqres.in/api/users?page=${page}&per_page=${newPerPage}&delay=1
        $page = $request->query('page', '0') - 1;
        $perpage = $request->query('per_page', '5');
        $search = $request->query('search', '');

        $page = $page * $perpage;
        try {
            $table = DB::table('lokasi_meninggal');
            $select = $table->select(
                'id','nama', 'note', 'kodewil', 'alamat','updated_at',
                DB::raw('SUBSTRING(kodewil,1,2) AS kode_prov'),
                DB::raw('SUBSTRING(kodewil,1,4) AS kode_kab'),
                DB::raw('SUBSTRING(kodewil,1,7) AS kode_kec'),
                DB::raw('SUBSTRING(kodewil,1,10) AS kode_des'),
                DB::raw('(SELECT nama FROM wilayah_provinsi WHERE id = kode_prov LIMIT 1) AS prov'),
                DB::raw('(SELECT nama FROM wilayah_kabupaten WHERE id = kode_kab LIMIT 1) AS kab'),
                DB::raw('(SELECT nama FROM wilayah_kecamatan WHERE id = kode_kec LIMIT 1) AS kec'),
                DB::raw('(SELECT nama FROM wilayah_desa WHERE id = kode_des LIMIT 1) AS des'),
                DB::raw('ST_X(Lokasi) AS lat'), 
                DB::raw('ST_Y(Lokasi) AS lng'),
            );
            
            if($search != ""){
                //orWhere
                $select->where('nama', 'like', '%'.$search.'%');
                $select->orWhere('alamat', 'like', '%'.$search.'%');
            }
            $select->limit($perpage);
            $select->offset($page);
            $pemakaman = $select->get();
            // "page": 1,
            // "per_page": 6,
            // "total": 12,
            // "total_pages": 2,
            // "data": [
            return response()->json(array(
                "success" => TRUE,
                "message" => "get survey list",
                "data" => array(
                    "page" => $page,
                    "per_page" => $perpage,
                    "total" => count(DB::table('lokasi_meninggal')->get()),
                    "data" => $pemakaman
                )
            ),200);
        } catch (\Throwable $th) {
            return response()->json(array(
                "success" => FALSE,
                "message" => "Error execution",
                "data" => $th
            ),500);
        }
    }
    
    
}