import React, { useState } from 'react'
import 'flatpickr/dist/themes/dark.css'
import moment from 'moment'
import Flatpickr from 'react-flatpickr'
import { Input, HelperText, Label, Select, Textarea } from '@windmill/react-ui'
import Uploader from './Uploader'
import { useDispatch } from 'react-redux'
import { setDate } from './uploaderSlice'
const CustomInput = ({ value, defaultValue, inputRef, ...props }) => {
	return (
		<Input
			{...props}
			value={moment().format('D-MM-Y')}
			ref={inputRef}
			className='mt-1 border-solid border-2 rounded'
		/>
	)
}

function UploadExcell() {
	const [dateValue, setDateValue] = useState(moment().format('DD-MM-Y'))
	const dispatch = useDispatch()
	return (
		<div className='px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800'>
			<div className='p-5'>
				<Label className=' max-w-max'>
					<span>Tanggal Data</span>
					<Flatpickr
						options={{ dateFormat: 'd-m-Y' }}
						onChange={([date]) =>
							dispatch(setDate(moment(date).format('DD-MM-Y')))
						}
						render={({ defaultValue, value, ...props }, ref) => {
							return (
								<CustomInput
									onChange={(e) => {
										setDateValue(e.target.value)
									}}
									defaultValue={defaultValue}
									inputRef={ref}
								/>
							)
						}}
					/>
				</Label>
			</div>
			<div className='p-5'>
				<Label className=' max-w-max'>
					<span>Pilih File</span>
				</Label>
				<Uploader />
			</div>
		</div>
	)
}

export default UploadExcell
