const base_url = 'https://dev2.robohox.com/mccc/mccc/api/v1/'
const MAP_BOX_PUBLIC_KEY =
	'pk.eyJ1Ijoia3ViZWdpcyIsImEiOiJjazd1bHp1ZWMxMnhxM2xucWwyY2psbTJvIn0.xKBnJu7oqoAWuB_Ynv42GA'
export { base_url, MAP_BOX_PUBLIC_KEY }
